<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $departement_id
 * @property string $nom
 * @property string $logo
 * @property string $banniere
 * @property string $email
 * @property string $siteweb
 * @property string $maire
 * @property string $adresse
 * @property string $telephone
 * @property integer $nombre_conseiller
 * @property integer $population
 * @property string $description
 * @property string $coordonnees
 * @property string $observations
 * @property string $slug
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $deleted_by
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property Departement $departement
 * @property Gymnase[] $gymnases
 */
class Commune extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['departement_id', 'nom', 'logo', 'banniere', 'email', 'siteweb', 'maire', 'adresse', 'telephone', 'nombre_conseiller', 'population', 'description', 'coordonnees', 'observations', 'slug', 'created_by', 'updated_by', 'deleted_by', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function departement()
    {
        return $this->belongsTo('App\Models\Departement');
    }

    public function gymnases()
    {
        return $this->hasMany('App\Models\Gymnase');
    }
}
