<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer $abonnement_id
 * @property integer $mode_paiement_id
 * @property float $montant
 * @property string $statut
 * @property string $date
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 * @property ModePaiement $modePaiement
 * @property Abonnement $abonnement
 */
class Transaction extends Model
{
    use SoftDeletes;
    /**
     * @var array
     */
    protected $fillable = ['abonnement_id', 'mode_paiement_id', 'montant', 'statut', 'date', 'deleted_at', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function modePaiement()
    {
        return $this->belongsTo('App\Models\ModePaiement');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function abonnement()
    {
        return $this->belongsTo('App\Models\Abonnement');
    }
}
