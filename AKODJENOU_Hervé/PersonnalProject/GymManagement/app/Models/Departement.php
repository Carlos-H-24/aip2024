<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $nom
 * @property Commune[] $communes
 * @property Salle[] $salles
 */
class Departement extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['nom'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function communes()
    {
        return $this->hasMany('App\Models\Commune');
    }

    public function salles()
    {
        return $this->hasMany('App\Models\Salle');
    }
}
