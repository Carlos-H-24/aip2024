<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer $id
 * @property integer $user_id
 * @property string $nom
 * @property string $description
 * @property string $date
 * @property string $heure
 * @property integer $duree
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 * @property User $user
 * @property Gymnase $gymnase
 * @property Seance[] $seances
 */
class Cour extends Model
{
    use SoftDeletes;
    /**
     * @var array
     */
    protected $fillable = ['user_id', 'gymnase_id', 'nom', 'description', 'deleted_at', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

     /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function gymnase()
    {
        return $this->belongsTo('App\Models\Gymnase');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function seances()
    {
        return $this->hasMany('App\Models\Seance', 'cours_id');
    }
}
