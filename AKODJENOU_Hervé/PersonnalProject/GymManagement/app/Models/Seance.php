<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer $id
 * @property integer $cours_id
 * @property string $nom
 * @property string $date
 * @property string $heure_debut
 * @property string $heure_fin
 * @property string $statut
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 * @property Cour $cour
 */
class Seance extends Model
{
    use SoftDeletes;
    /**
     * @var array
     */
    protected $fillable = ['cours_id', 'nom', 'date', 'heure_debut', 'statut', 'heure_fin', 'deleted_at', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cour()
    {
        return $this->belongsTo('App\Models\Cour', 'cours_id');
    }
}
