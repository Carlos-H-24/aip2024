<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer $id
 * @property string $nom
 * @property double $tarif
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 * @property Abonnement[] $abonnements
 */
class TypeAbonnement extends Model
{
    use SoftDeletes;
    /**
     * @var array
     */
    protected $fillable = ['nom', 'tarif', 'deleted_at', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function abonnements()
    {
        return $this->hasMany('App\Models\Abonnement');
    }
}
