<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer $equipement_id
 * @property integer $salle_id
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 * @property integer $quantite
 * @property Salle $salle
 * @property Equipement $equipement
 */
class Contenir extends Model
{
    use SoftDeletes;
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'contenir';

    /**
     * @var array
     */
    protected $fillable = ['deleted_at', 'created_at', 'updated_at', 'quantite'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function salle()
    {
        return $this->belongsTo('App\Models\Salle');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function equipement()
    {
        return $this->belongsTo('App\Models\Equipement');
    }
}
