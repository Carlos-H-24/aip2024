<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer $id
 * @property integer $commune_id
 * @property string $nom
 * @property string $email
 * @property string $adresse
 * @property string $description
 * @property integer $longitude
 * @property integer $latitude
 * @property integer $telephone
 * @property string $photo
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 * @property Appartenir[] $appartenirs
 * @property Cour[] $cours
 * @property Commune $commune
 */
class Gymnase extends Model
{
    use SoftDeletes;
    /**
     * @var array
     */
    protected $fillable = ['commune_id', 'nom', 'email', 'adresse', 'description', 'longitude', 'latitude', 'telephone', 'photo', 'deleted_at', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function appartenirs()
    {
        return $this->hasMany('App\Models\Appartenir');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cours()
    {
        return $this->hasMany('App\Models\Cour');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function commune()
    {
        return $this->belongsTo('App\Models\Commune');
    }
}
