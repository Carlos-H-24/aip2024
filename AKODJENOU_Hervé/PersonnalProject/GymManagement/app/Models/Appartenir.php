<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer $user_id
 * @property integer $gymnase_id
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 * @property Gymnase $gymnase
 * @property User $user
 */
class Appartenir extends Model
{
    use SoftDeletes;
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'appartenir';

    /**
     * @var array
     */
    protected $fillable = ['user_id', 'gymnase_id', 'deleted_at', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function gymnase()
    {
        return $this->belongsTo('App\Models\Gymnase');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
