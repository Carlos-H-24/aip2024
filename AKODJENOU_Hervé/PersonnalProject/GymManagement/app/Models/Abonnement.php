<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer $id
 * @property integer $user_id
 * @property integer $type_abonnement_id
 * @property int $mois
 * @property string $date_debut
 * @property string $date_fin
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 * @property User $user
 * @property Gymnase $gymnase
 * @property TypeAbonnement $typeAbonnement
 * @property Transaction[] $transactions
 */
class Abonnement extends Model
{
    use SoftDeletes;
    /**
     * @var array
     */
    protected $fillable = ['user_id', 'gymnase_id', 'type_abonnement_id', 'mois', 'date_debut', 'date_fin', 'deleted_at', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function gymnase()
    {
        return $this->belongsTo('App\Models\Gymnase');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function typeAbonnement()
    {
        return $this->belongsTo('App\Models\TypeAbonnement');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transactions()
    {
        return $this->hasMany('App\Models\Transaction');
    }

    
}
