<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer $id
 * @property string $nom
 * @property string $superficie
 * @property integer $capacite
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 * @property integer $gymnase_id
 * @property Gymnase $gymnase
 * @property Contenir[] $contenirs
 * @property Cour[] $cours
 */
class Salle extends Model
{
    use SoftDeletes;
    /**
     * @var array
     */
    protected $fillable = ['nom', 'superficie', 'capacite', 'deleted_at', 'created_at', 'updated_at','gymnase_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function contenirs()
    {
        return $this->hasMany('App\Models\Contenir');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cours()
    {
        return $this->hasMany('App\Models\Cour');
    }

    public function gymnase()
    {
        return $this->belongsTo('App\Models\Gymnase');
    }
}
