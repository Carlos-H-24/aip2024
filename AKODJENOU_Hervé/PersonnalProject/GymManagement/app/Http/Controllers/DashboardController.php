<?php

namespace App\Http\Controllers;

use App\Models\Appartenir;
use App\Models\Cour;
use App\Models\Gymnase;
use App\Models\Role;
use App\Models\Salle;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function index()
    {
        $user =  Auth::user();
        if ($user->role->nom == 'Gestionnaire') {
            $appartenance = Appartenir::where('user_id', $user->id)->first();
            $gymnase =  $appartenance->gymnase;

            //Adhérent
            $otherAdherents = Appartenir::where('gymnase_id', $gymnase->id)
                ->whereHas('user', function ($query) {
                    $query->where('role_id', '3');
                })
                ->where('user_id', '!=', $user->id)
                ->get();
            $adherents = User::whereIn('id', $otherAdherents->pluck('user_id')->toArray())->get();
            $nbreAdherent = count($adherents->all());
            $nbreAdherent = str_pad($nbreAdherent, 2, '0', STR_PAD_LEFT);

            //Coachs
            $coachs = Appartenir::where('gymnase_id', $gymnase->id)
                ->whereHas('user', function ($query) {
                    $query->where('role_id', '4');
                })
                ->with('user')
                ->get();
            $nbreCoach = count($coachs->all());
            $nbreCoach = str_pad($nbreCoach, 2, '0', STR_PAD_LEFT);

            //Cours
            $cours = Cour::where('gymnase_id', $gymnase->id)->get();
            $nbreCours = count($cours->all());
            $nbreCours = str_pad($nbreCours, 2, '0', STR_PAD_LEFT);


            //Salles
            $salles = Salle::where('gymnase_id', $gymnase->id)->get();
            $nbreSalles = count($salles->all());
            $nbreSalles = str_pad($nbreSalles, 2, '0', STR_PAD_LEFT);

            //Séances en attente
            $nbreSeancesEnAttente = 0;
            foreach ($cours as $cour) {
                $nbreSeancesEnAttente += $cour->seances()->where('statut', 'en attente')->count();
            }
            $nbreSeancesEnAttente = str_pad($nbreSeancesEnAttente, 2, '0', STR_PAD_LEFT);


            //Séances terminées
            $nbreSeanceTerminees = 0;
            foreach ($cours as $cour) {
                $nbreSeanceTerminees += $cour->seances()->where('statut', 'terminéé')->count();
            }
            $nbreSeanceTerminees = str_pad($nbreSeanceTerminees, 2, '0', STR_PAD_LEFT);


            // Récupérer tous les cours avec le nombre total de séances
            $coursAvecNbSeances = Cour::where('gymnase_id', $gymnase->id)->with(['seances', 'user'])->withCount('seances')->get();
            $coursTries = $coursAvecNbSeances->sortByDesc('seances_count');
            $topCours = $coursTries->take(5);

            return view("pages.manager.dashboard", compact('nbreAdherent', 'nbreCoach', 'nbreCours', 'nbreSalles', 'nbreSeancesEnAttente', 'nbreSeanceTerminees', 'topCours'));
        }
        if (Auth::user()->role->nom == 'Administrateur') {
            $manageRoleIds = Role::where('nom', 'gestionnaire')->pluck('id');
            $gestionnaires = User::whereIn('role_id', $manageRoleIds)->count();
            $gymnases = Gymnase::count();
            $gymnases = str_pad($gymnases, 2, '0', STR_PAD_LEFT);


            $adherentsTotal = User::whereHas('appartenirs', function ($query) {
                $query->where('user_id', '!=', auth()->user()->id)->where('role_id', '3');
            })->count();
            $adherentsTotal = str_pad($adherentsTotal, 2, '0', STR_PAD_LEFT);

            $coachsTotal = User::whereHas('appartenirs', function ($query) {
                $query->where('user_id', '!=', auth()->user()->id)->where('role_id', '4');
            })->count();
            $coachsTotal = str_pad($coachsTotal, 2, '0', STR_PAD_LEFT);


            return view("pages.admin.dashboard", compact('gymnases', 'gestionnaires', 'adherentsTotal', 'coachsTotal'));
        }
        if ($user->role->nom == 'Adhérent') {
            $abonnements = $user->abonnements->count();
            $abonnements = str_pad($abonnements, 2, '0', STR_PAD_LEFT);
            $nombreGymnases = $user->appartenirs->count();
            $nombreGymnases = str_pad($nombreGymnases, 2, '0', STR_PAD_LEFT);

            $montantTotalAbonnements = (int)$user->abonnements->sum(function ($abonnement) {
                return $abonnement->typeAbonnement->tarif * $abonnement->mois;
            });

            $abonnementsParGymnase = $user->abonnements->map(function ($abonnement) {
                return [
                    'gymnase' => $abonnement->gymnase,
                    'type_abonnement'  => $abonnement->typeAbonnement->nom,
                    'mois' => str_pad($abonnement->mois, 2, '0', STR_PAD_LEFT),
                    'montant' => (int)$abonnement->typeAbonnement->tarif * $abonnement->mois,
                    'date_debut' => $abonnement->date_debut,
                    'date_fin' => $abonnement->date_fin,
                ];
            });




            return view('pages.adherent.dashboard', compact('abonnements', 'nombreGymnases', 'montantTotalAbonnements', 'abonnementsParGymnase'));
        }
        if ($user->role->nom == 'Coach') {
            $nombreCours = Cour::where('user_id', $user->id)->count();
            $nombreCours = str_pad($nombreCours, 2, '0', STR_PAD_LEFT);

            $coursAvecSeances = $user->cours()->with(['seances' => function ($query) {
                $query->where('statut', 'en attente');
            }])->get();
            $coursAvecSeances2 = $user->cours()->with('seances')->get();

            $nombreTotalSeances = 0;
            foreach ($coursAvecSeances2 as $cours) {
                $nombreTotalSeances += $cours->seances->count();
            }
            $nombreTotalSeances = str_pad($nombreTotalSeances, 2, '0', STR_PAD_LEFT);

            return view('pages.coach.dashboard', compact('nombreCours', 'nombreTotalSeances', 'coursAvecSeances'));
        }
    }
}
