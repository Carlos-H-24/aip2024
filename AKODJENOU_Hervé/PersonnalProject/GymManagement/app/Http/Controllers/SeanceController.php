<?php

namespace App\Http\Controllers;

use App\Http\Requests\SeanceStoreRequest;
use App\Models\Cour;
use App\Models\Seance;
use DateTime;
use Illuminate\Http\Request;

class SeanceController extends Controller
{
    function formaterDate($date)
    {
        setlocale(LC_TIME, 'fr_FR.utf8', 'fr_FR', 'fr');
        $dateObj = DateTime::createFromFormat('Y-m-d', $date);
        if ($dateObj) {
            return strftime('%e %B %Y', $dateObj->getTimestamp());
        } else {
            return null;
        }
    }

    public function index(string $id)
    {
        $cours = Cour::findOrFail($id);
        return view("pages.manager.seances.create", compact('cours'));
    }

    public function store(SeanceStoreRequest $request, string $id)
    {
        $cours = Cour::findOrFail($id);
        $data = $request->validated();
        $data['date'] = $this->formaterDate($request->validated('date'));
        $data['cours_id'] = $cours->id;
        Seance::create($data);
        return redirect()->route('manager.cours.show',$cours);
    }


    public function delete(string $id1,string $id2)
    {
        $cours = Cour::findOrFail($id1);
        Seance::findOrFail($id2)->delete();
        return redirect()->route('manager.cours.show',$cours);
    }

    public function update(string $id1,string $id2)
    {
        $cours = Cour::findOrFail($id1);
        $seance = Seance::findOrFail($id2);
        $seance->statut = "terminée";
        $seance->save();
        return redirect()->route('manager.cours.show',$cours);
    }
}
