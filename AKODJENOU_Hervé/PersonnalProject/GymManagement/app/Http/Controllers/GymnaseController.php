<?php

namespace App\Http\Controllers;

use App\Http\Requests\GymnaseRequest;
use App\Models\Appartenir;
use App\Models\Commune;
use App\Models\Departement;
use App\Models\Gymnase;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\DataTables;

class GymnaseController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function loadCommunes($departementId)
    {
        $communes = Commune::where('departement_id', $departementId)->get();
        // dd($communes);
        return response()->json($communes);
    }

    public function index()
    {
        $gymnases = Gymnase::paginate(10);
        return view("pages.admin.gymnases.index", compact("gymnases"));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $departements = Departement::get();
        $manageRoleIds = Role::where('nom', 'gestionnaire')->pluck('id');
        $gestionnaires = User::whereIn('role_id', $manageRoleIds)->get();
        return view('pages.admin.gymnases.create', compact('gestionnaires', 'departements'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(GymnaseRequest $request)
    {
        // dd($request->validated());
        $gym = Gymnase::create($request->except('nom_ges', 'prenom_ges', 'email_ges', 'adresse_ges', 'telephone_ges', 'date_naissance_ges', 'sexe_ges'));

        $user = User::create([
            'role_id' => 2,
            'nom' => $request->validated('nom_ges'),
            'prenom' => $request->validated('prenom_ges'),
            'email' => $request->validated('email_ges'),
            'password' => Hash::make('password'),
            'adresse' => $request->validated('adresse_ges'),
            'telephone' => $request->validated('telephone_ges'),
            'sexe' => $request->validated('sexe_ges'),
            'date_naissance' => $request->validated('date_naissance_ges'),
        ]);

        Appartenir::create([
            'user_id' => $user->id,
            'gymnase_id' => $gym->id,
        ]);
        return redirect()->route('admin.gymnases.index')->with('successCreate', 'Gymnase crée avec succès');
    }


    /**
     * Display the specified resource.
     */
    public function show(String $id)
    {
        $gymnase = Gymnase::findOrFail($id);
        $commune = $gymnase->commune;
        $dep = $commune->departement;
        $appartenirs = $gymnase->appartenirs;
        $gestionnaires = [];
        foreach ($appartenirs as $appartenir) {
            $user = $appartenir->user;
            if ($user->role && $user->role->nom === 'Gestionnaire') {
                $gestionnaires[] = $user;
            }
        }
        $g = $gestionnaires[0];
        return view('pages.admin.gymnases.show', compact('gymnase', 'g', 'commune', 'dep'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $departements = Departement::get();
        $manageRoleIds = Role::where('nom', 'gestionnaire')->pluck('id');
        $gestionnaires = User::whereIn('role_id', $manageRoleIds)
            ->whereDoesntHave('appartenirs.gymnase')
            ->get();
        $gymnase = Gymnase::findOrFail($id);
        return view('pages.admin.gymnases.edit', compact('gymnase', 'gestionnaires', 'departements'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(GymnaseRequest $request, string $id)
    {
        $a = Appartenir::where('gymnase_id', $id)->firstOrFail();
        $a->user_id = (int)$request->validated('idgest', 'departement ');
        $d = [];
        $d['user_id'] = $a->user_id;
        $d['gymnase_id'] = $a->gymnase_id;
        Appartenir::where('gymnase_id', $id)->update($d);
        $photoGym = $request->validated('photo')->store('uploads/gym', 'public');
        $data  = $request->except('idgest');
        $data['photo'] = $photoGym;
        Gymnase::findOrFail($id)->update($data);
        return redirect()->route('admin.gymnases.index')->with('successUpdate', 'Gymnase modifié avec succès');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $gymnase = Gymnase::findOrFail($id);

        $gymnase->appartenirs()->delete();
        $gymnase->delete();

        return redirect()->route('admin.gymnases.index')->with('successDelete', 'Gymnase supprimée avec succès');
    }
}
