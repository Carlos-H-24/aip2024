<?php

namespace App\Http\Controllers;

use App\Http\Requests\SalleRequest;
use App\Models\Appartenir;
use App\Models\Salle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SalleController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $user =  Auth::user();
        if ($user->role->nom == 'Gestionnaire') {
            $appartenance = Appartenir::where('user_id', $user->id)->first();
            $gymnase =  $appartenance->gymnase;

            $salles = Salle::where('gymnase_id', $gymnase->id)->paginate(10);
            return view('pages.manager.salles.index', compact('salles'));
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('pages.manager.salles.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(SalleRequest $request)
    {
        $user =  Auth::user();
        $appartenance = Appartenir::where('user_id', $user->id)->first();
        $gymnase =  $appartenance->gymnase;

        $data = $request->validated();
        $data['gymnase_id'] =  $gymnase->id;
        Salle::create($data);
        return redirect()->route('manager.salles.index')->with('successCreate', 'Salle créee avec succès');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
    }


    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $salle = Salle::find($id);
        return view('pages.manager.salles.edit', compact('salle'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(SalleRequest $request, string $id)
    {
        Salle::find($id)->update($request->validated());
        return redirect()->route('manager.salles.index')->with('successUpdate', 'Salle modifiée avec succès');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        Salle::find($id)->delete();
        return redirect()->route('manager.salles.index')->with('successDelete', 'Salle supprimée avec succès');
    }
}
