<?php

namespace App\Http\Controllers;

use App\Http\Requests\ManagerRequest;
use App\Models\Appartenir;
use App\Models\Gymnase;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ManagerController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $manageRoleId = Role::where('nom', 'gestionnaire')->value('id');
        $gestionnaires = User::where('role_id', $manageRoleId)
            ->with('appartenirs.gymnase')
            ->paginate(10);
        // dd($gestionnaires);
        return view('pages.admin.managers.index', compact('gestionnaires'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $manager = User::where('id', $id)
            ->with('appartenirs.gymnase')
            ->get();
        return view('pages.admin.managers.show',compact('manager'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $gymnases = Gymnase::whereDoesntHave('appartenirs.user', function ($query) {
            $query->where('role_id', '2');
        })->get();
        $manager = User::find($id);
        return view('pages.admin.managers.edit', compact('gymnases', 'manager'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(ManagerRequest $request, string $id)
    {
        // dd($request->validated());
        User::find($id)->update($request->except('gymnase_id'));

        if ($request->validated('gymnase_id') != null) {
            Appartenir::where('user_id', $id)->update([
                'user_id' => $id,
                'gymnase_id' => $request->validated('gymnase_id'),
            ]);
        }
        return redirect()->route('admin.managers.index')->with('successUpdate', 'Gestionnaire crée avec succès');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        Appartenir::where('user_id', $id)->delete();
        User::find($id)->delete();
        return redirect()->route('admin.managers.index')->with('successDelete', 'Gestionnaire suprimé avec succès');
    }
}
