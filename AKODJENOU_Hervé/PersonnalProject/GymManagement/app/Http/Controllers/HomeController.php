<?php

namespace App\Http\Controllers;

use App\Http\Requests\AbonneeRequest;
use App\Http\Requests\AbonnementRequest;
use App\Mail\NouvelAbonnementMail;
use App\Models\Abonnement;
use App\Models\Appartenir;
use App\Models\Cour;
use App\Models\Gymnase;
use App\Models\TypeAbonnement;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    public function index()
    {
        $gymnases = Gymnase::get();
        return view("layouts.home", compact("gymnases"));
    }

    public function show(string $id)
    {
        $gym = Gymnase::find($id);
        $appartenirs = $gym->appartenirs;
        $gestionnaires = [];
        foreach ($appartenirs as $appartenir) {
            $user = $appartenir->user;
            if ($user->role && $user->role->nom === 'Gestionnaire') {
                $gestionnaires[] = $user;
            }
        }
        $gest = $gestionnaires[0];

        $coachs = Appartenir::where('gymnase_id', $gym->id)
            ->whereHas('user', function ($query) {
                $query->where('role_id', '4');
            })
            ->with('user')
            ->get();
        $nbreCoach = count($coachs->all());
        $nbreCoach = str_pad($nbreCoach, 2, '0', STR_PAD_LEFT);

        $cours = Cour::where('gymnase_id', $gym->id)->get();
        $nbreCours = count($cours->all());
        $nbreCours = str_pad($nbreCours, 2, '0', STR_PAD_LEFT);

        $typeAbonnements = TypeAbonnement::all();


        return view("pages.others.salle", compact("gym", "gest", "nbreCoach", "nbreCours", "typeAbonnements"));
    }

    private function calculateDates($nombreDeMois)
    {
        $aujourdhui = Carbon::now();
        $dateDebut = $aujourdhui->copy()->addMonths($nombreDeMois);
        $dateFin = $aujourdhui;
        $format = 'd/m/Y';
        $dateDebutFormatee = $dateDebut->format($format);
        $dateFinFormatee = $dateFin->format($format);
        return [
            'date_fin' => $dateDebutFormatee,
            'date_debut' => $dateFinFormatee,
        ];
    }

    public function store(AbonnementRequest $request)
    {
        $data = $request->except('gymnase', 'mois', 'typeAbn');
        $data['role_id'] = 3;
        $data['password'] = Hash::make('password');
        $user = User::create($data);

        $date = $this->calculateDates($request->validated('mois'));
        $gym = Gymnase::find($request->validated('gymnase'));
        $typeAbn = TypeAbonnement::find($request->validated('typeAbn'));

        $abn = Abonnement::create([
            'user_id' => $user->id,
            'gymnase_id' => $request->validated('gymnase'),
            'type_abonnement_id' => $request->validated('typeAbn'),
            'mois' => $request->validated('mois'),
            'date_debut' => $date['date_debut'],
            'date_fin' => $date['date_fin'],
        ]);

        Appartenir::create([
            'user_id' => $user->id,
            'gymnase_id' => $request->validated('gymnase'),
        ]);

        Mail::to($user->email)->send(new NouvelAbonnementMail($user,$gym,$abn,$typeAbn));
        

        return redirect()->route('home')->with('successCreate', 'Abonnement crée avec succès');
    }

    public function other(AbonneeRequest $request){
        $date = $this->calculateDates($request->validated('mois'));
        $user = Auth::user();
        Appartenir::firstOrCreate([
            'user_id' => $user->id,
            'gymnase_id' => $request->validated('gymnase'),
        ]);
        $abn = Abonnement::create([
            'user_id' => $user->id,
            'gymnase_id' => $request->validated('gymnase'),
            'type_abonnement_id' => $request->validated('typeAbn'),
            'mois' => $request->validated('mois'),
            'date_debut' => $date['date_debut'],
            'date_fin' => $date['date_fin'],
        ]);
        $gym = Gymnase::find($request->validated('gymnase'));
        $typeAbn = TypeAbonnement::find($request->validated('typeAbn'));
        Mail::to($user->email)->send(new NouvelAbonnementMail($user,$gym,$abn,$typeAbn));
        return redirect()->route('home')->with('successCreate', 'Abonnement crée avec succès');
    }
}
