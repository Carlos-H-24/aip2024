<?php

namespace App\Http\Controllers;

use App\Http\Requests\AuthRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    protected $redirectTo = '/';
    public function login()
    {
        return view("layouts.login");
    }

    public function doLogin(AuthRequest $request)
    {
        
        $credentials = $request->validated();
        $remember = $request->has('remember');
        if (Auth::attempt($credentials, $remember)) {
            $request->session()->regenerate();
            return redirect()->intended(route('home'));
        } else {
            return redirect()->route('login')->with("error", "Email ou mot de passe incorrecte")->onlyInput('email');
        }
    }

    public function logout()
    {
        Auth::logout();
        return to_route('login');
    }
}
