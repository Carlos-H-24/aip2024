<?php

namespace App\Http\Controllers;

use App\Http\Requests\CoursRequest;
use App\Http\Requests\CoursUpdateRequest;
use App\Models\Appartenir;
use App\Models\Cour;
use App\Models\Gymnase;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class CoursController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $user =  Auth::user();
        $appartenance = Appartenir::where('user_id', $user->id)->first();
        $gymnase =  $appartenance->gymnase;

        
        $cours = Cour::where('gymnase_id' , $gymnase->id)->paginate(10);
    
        return view('pages.manager.cours.index', compact('cours'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('pages.manager.cours.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CoursRequest $request)
    {
        $user =  Auth::user();
        $appartenance = Appartenir::where('user_id', $user->id)->first();
        $gymnase =  $appartenance->gymnase;


        $user = User::create([
            'role_id' => 4,
            'nom' => $request->validated('nom_coach'),
            'prenom' => $request->validated('prenom_coach'),
            'email' => $request->validated('email_coach'),
            'password' => Hash::make('password'),
            'adresse' => $request->validated('adresse_coach'),
            'telephone' => $request->validated('telephone_coach'),
            'sexe' => $request->validated('sexe_coach'),
            'date_naissance' => $request->validated('date_naissance_coach'),
        ]);

        Appartenir::create([
            'user_id' => $user->id,
            'gymnase_id' => $gymnase->id,
        ]);

        $cour = Cour::create(
            [
                'nom'=> $request->validated('nom'),
                'user_id' => $user->id,
                'gymnase_id' => $gymnase->id,
                'description'=> $request->validated('description'),
            ]
        );

        return redirect()->route('manager.cours.index')->with('successCreate', 'Cours créee avec succès');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $cours = Cour::findOrFail($id);
        $seances = $cours->seances()->where('statut', 'en attente')->get();
        return view('pages.manager.cours.show', compact('cours', 'seances'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $cours = Cour::find($id);
        $gymnase = Gymnase::findOrFail($cours->gymnase_id);
        $appartenirs = $gymnase->appartenirs;
        $coachs = [];
        foreach ($appartenirs as $appartenir) {
            $user = $appartenir->user;
            if ($user->role_id == 4) {
                $coachs[] = $user;
            }
        }
        
        

        
        return view('pages.manager.cours.edit', compact('cours','coachs'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(CoursUpdateRequest $request, string $id)
    {
        Cour::find($id)->update([
            'nom'=> $request->validated('nom'),
            'user_id'=> (int) $request->validated('coach'),
            'description'=> $request->validated('description'),
        ]);
        return redirect()->route('manager.cours.index')->with('successUpdate', 'Cours modifié avec succès');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        Cour::find($id)->delete();
        return redirect()->route('manager.cours.index')->with('successDelete', 'Cours supprimé avec succès');
    }
}
