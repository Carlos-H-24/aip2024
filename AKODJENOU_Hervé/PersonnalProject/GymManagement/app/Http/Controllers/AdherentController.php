<?php

namespace App\Http\Controllers;

use App\Http\Requests\AdherentRequest;
use App\Http\Requests\AdherentUpdateRequest;
use App\Models\Abonnement;
use App\Models\Appartenir;
use App\Models\Gymnase;
use App\Models\Role;
use App\Models\TypeAbonnement;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use PhpParser\Node\Expr\Cast\Object_;
use Yajra\DataTables\DataTables;

class AdherentController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {

        $user =  Auth::user();
        $appartenance = Appartenir::where('user_id', $user->id)->first();
        $gymnase =  $appartenance->gymnase;

        $adherents = Appartenir::where('gymnase_id', $gymnase->id)
            ->whereHas('user', function ($query) {
                $query->where('role_id', '3');
            })
            ->with('user')
            ->paginate(10);

        return view("pages.manager.adherent.index", compact("adherents"));
    }

    private function calculateDates($nombreDeMois)
    {
        $aujourdhui = Carbon::now();
        $dateDebut = $aujourdhui->copy()->addMonths($nombreDeMois);
        $dateFin = $aujourdhui;
        $format = 'd/m/Y';
        $dateDebutFormatee = $dateDebut->format($format);
        $dateFinFormatee = $dateFin->format($format);
        return [
            'date_fin' => $dateDebutFormatee,
            'date_debut' => $dateFinFormatee,
        ];
    }


    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $id = Auth::user()->appartenirs[0]->gymnase_id;
        $gymnase = Gymnase::findOrFail($id);
        $typeAbonnements = TypeAbonnement::all();
        return view("pages.manager.adherent.create", compact('typeAbonnements', 'gymnase'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(AdherentRequest $request)
    {
        $date = $this->calculateDates($request->validated('mois'));
        $data = $request->except('typeAbn', 'mois');
        $data['role_id'] = 3;
        $data['password'] = Hash::make('password');
        $user = User::create($data);

        $userAuth =  Auth::user();
        $appartenance = Appartenir::where('user_id', $userAuth->id)->first();
        $gymnase =  $appartenance->gymnase;

        Abonnement::create([
            'user_id' => $user->id,
            'gymnase_id' => $gymnase->id,
            'type_abonnement_id' => $request->validated('typeAbn'),
            'mois' => $request->validated('mois'),
            'date_debut' => $date['date_debut'],
            'date_fin' => $date['date_fin'],
        ]);

        Appartenir::create([
            'user_id' => $user->id,
            'gymnase_id' => $gymnase->id,
        ]);

        return redirect()->route('manager.adherents.index')->with('successCreate', 'Adhérent crée avec succès');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $userAuth =  Auth::user();
        $appartenance = Appartenir::where('user_id', $userAuth->id)->first();
        $gymnase =  $appartenance->gymnase;
        $ab = Abonnement::where('user_id',$id)->first();
        $user = User::find($id);
        $type = TypeAbonnement::find($ab->type_abonnement_id);
        return view('pages.manager.adherent.show',compact('gymnase','ab','user','type'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $userAb = Abonnement::where('user_id',$id)->first();
        $user = User::find($id);
        $typeAbonnements = TypeAbonnement::all();
        return view('pages.manager.adherent.edit',compact('userAb','user','typeAbonnements'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(AdherentUpdateRequest $request, string $id)
    {
        $date = $this->calculateDates($request->validated('mois'));
        User::find( $id )->update($request->except('typeAbn', 'mois'));
        Abonnement::where('user_id',$id)->update([
            'type_abonnement_id' => $request->validated('typeAbn'),
            'mois' => $request->validated('mois'),
            'date_debut' => $date['date_debut'],
            'date_fin' => $date['date_fin'],
        ]);
        return redirect()->route('manager.adherents.index')->with('successUpdate', 'Adhérent modifié avec succès');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        Appartenir::where('user_id', $id)->delete();
        Abonnement::where('user_id',$id)->delete();
        User::find($id)->delete();
        return redirect()->route('manager.adherents.index')->with('successDelete', 'Adhérent suprimé avec succès');
    }
}
