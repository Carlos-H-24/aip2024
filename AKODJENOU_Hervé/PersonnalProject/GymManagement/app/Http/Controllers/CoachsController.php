<?php

namespace App\Http\Controllers;

use App\Http\Requests\CoachsRequest;
use App\Models\Appartenir;
use App\Models\Cour;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CoachsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $user =  Auth::user();
        $appartenance = Appartenir::where('user_id', $user->id)->first();
        $gymnase =  $appartenance->gymnase;

        $coachs = Appartenir::where('gymnase_id', $gymnase->id)
            ->whereHas('user', function ($query) {
                $query->where('role_id', '4');
            })
            ->with('user')
            ->paginate(10);
            
        return view('pages.manager.coachs.index', compact('coachs'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $user =  User::find($id);
        return view('pages.manager.coachs.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $user =  User::find($id);
        return view('pages.manager.coachs.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(CoachsRequest $request, string $id)
    {
        User::find( $id )->update($request->validated());
        return redirect()->route('manager.coachs.index')->with('successUpdate', 'Coach modifié avec succès');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        Cour::where('user_id', $id)->delete();
        User::find($id)->delete();
        return redirect()->route('manager.coachs.index')->with('successDelete', 'Coach suprimé avec succès');
    }
}
