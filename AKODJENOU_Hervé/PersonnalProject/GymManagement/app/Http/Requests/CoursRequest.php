<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CoursRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'nom' => 'required|string|max:255',
            'description' => 'required|string',
            'nom_coach' => 'required|string|max:255',
            'prenom_coach' => 'required|string|max:255',
            'email_coach' => 'required|email',
            'adresse_coach' => 'required|string|max:255',
            'telephone_coach' => 'required|string|max:20',
            'date_naissance_coach' => 'required|date',
            'sexe_coach' => 'required',
        ];
    }
}
