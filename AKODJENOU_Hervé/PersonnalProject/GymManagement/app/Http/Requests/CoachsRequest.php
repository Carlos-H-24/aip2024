<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CoachsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'nom' => 'required|string|max:255',
            'prenom' => 'required|string|max:255',
            'adresse' => 'required|string|max:255',
            'telephone' => 'required|string|max:20',
            'sexe' => 'required|in:Masculin,Féminin,Autre',
            'date_naissance' => 'required|date',
        ];
    }
}
