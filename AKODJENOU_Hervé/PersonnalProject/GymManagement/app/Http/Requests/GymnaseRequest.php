<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GymnaseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'nom' => 'required|string|max:255',
            'email' => 'required|email',
            'adresse' => 'required|string|max:255',
            'description' => 'required|string',
            'longitude' => 'required|numeric',
            'latitude' => 'required|numeric',
            'telephone' => 'required|string|max:20',
            'nom_ges' => 'required|string|max:255',
            'prenom_ges' => 'required|string|max:255',
            'email_ges' => 'required|email',
            'adresse_ges' => 'required|string|max:255',
            'telephone_ges' => 'required|string|max:20',
            'date_naissance_ges' => 'required|date',
            'sexe_ges' => 'required',
        ];
    }
}
