<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('abonnements', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('type_abonnement_id');
            $table->integer('mois');
            $table->dateTime('date_debut');
            $table->dateTime('date_fin');
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users') ;
            $table->foreign('type_abonnement_id')->references('id')->on('type_abonnements') ;
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('abonnements');
    }
};
