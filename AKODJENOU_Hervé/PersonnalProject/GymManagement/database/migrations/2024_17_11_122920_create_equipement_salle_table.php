<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('contenir', function (Blueprint $table) {
            $table->unsignedBigInteger('equipement_id');
            $table->unsignedBigInteger('salle_id');
            $table->softDeletes();
            $table->timestamps();
            $table->integer('quantite');
            $table->foreign('equipement_id')->references('id')->on('equipements') ;
            $table->foreign('salle_id')->references('id')->on('salles') ;
            $table->primary(['equipement_id','salle_id']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('contenir');
    }
};
