<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->unsignedBigInteger('abonnement_id');
            $table->unsignedBigInteger('mode_paiement_id');
            $table->double('montant');
            $table->string('statut');
            $table->dateTime('date');
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('abonnement_id')->references('id')->on('abonnements');
            $table->foreign('mode_paiement_id')->references('id')->on('mode_paiements');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('transactions');
    }
};
