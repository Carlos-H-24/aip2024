<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('gymnases', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('commune_id');
            $table->string('nom');
            $table->string('email');
            $table->string('adresse');
            $table->longText('description');
            $table->integer('longitude');
            $table->integer('latitude');
            $table->integer('telephone');
            $table->string('photo');
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('commune_id')->references('id')->on('communes') ;
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('gymnases');
    }
};
