<?php

use App\Http\Controllers\AdherentController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CoachsController;
use App\Http\Controllers\CoursController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\GymnaseController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ManagerController;
use App\Http\Controllers\SalleController;
use App\Http\Controllers\SeanceController;
use App\Http\Controllers\SeancesController;
use App\Http\Controllers\SearchController;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;
use Laravel\Fortify\Http\Controllers\AuthenticatedSessionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

//Principale
Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('/salles/{salle}', [HomeController::class, 'show'])->name('salle.show');
Route::post('/abonnement', [HomeController::class, 'store'])->name('abonnement.store');
Route::post('/abonnement/adherent', [HomeController::class, 'other'])->name('abonnement.other');



//Connexion
Route::get('/connexion', [AuthenticatedSessionController::class, 'create'])->middleware('guest')->name('login');
Route::post('/connexion', [AuthenticatedSessionController::class, 'store']);

//Déconnexion
Route::post('/logout', [AuthenticatedSessionController::class, 'destroy'])->middleware('auth')->name('logout');

//Admin
Route::prefix('admin')->middleware('auth')->name('admin.')->group(function () {
    Route::resource('gymnases', GymnaseController::class);
    Route::resource('managers', ManagerController::class);
});

//Coach
Route::prefix('coach')->middleware('auth')->name('coach.')->group(function () {
});

//Gestionnaire
Route::prefix('manager')->middleware('auth')->name('manager.')->group(function () {
    Route::resource('adherents', AdherentController::class);
    Route::resource('salles', SalleController::class);
    Route::resource('cours', CoursController::class);
    Route::resource('coachs', CoachsController::class);
    Route::get('cours/{cours}/seances/add',[ SeanceController::class,'index'])->name('seance.add');
    Route::post('cours/{cours}/seances/add',[ SeanceController::class,'store'])->name('seance.store');
    Route::delete('cours/{cours}/seances/{seance}',[ SeanceController::class,'delete'])->name('seance.delete');
    Route::post('cours/{cours}/seances/{seance}',[ SeanceController::class,'update'])->name('seance.update');


});

Route::get('/dashboard', [DashboardController::class, 'index'])->middleware('auth')->name('dashboard');


Route::get('/get-communes/{departementId}', [GymnaseController::class, 'loadCommunes']);

// Route::get('/insert', function () {
//     User::create([
//         'role_id' => 1,
//         'nom' => 'AKODJENOU',
//         'prenom' => 'Hervé',
//         'adresse' => 'Abomey-Calavi,Houèto Fifonsi',
//         'telephone' => 66748936,
//         'sexe' => 'Masculin',
//         'date_naissance' => Date::now(),
//         'email' => 'akodjenouherve13@gmail.com',
//         'password' => Hash::make('password'),
//         'photo' => 'photo'
//     ]);
// });
