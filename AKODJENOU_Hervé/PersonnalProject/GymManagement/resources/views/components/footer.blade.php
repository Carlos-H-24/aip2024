<footer class="footer">
    <div class="container-fluid mt-md-2">
            <div class="copyright ml-auto">
                2024, made with <i class="fa fa-eye text-danger"></i> by <a href="https://therichpost.com">Hervé Carlos</a>
            </div>				
    </div>
</footer>