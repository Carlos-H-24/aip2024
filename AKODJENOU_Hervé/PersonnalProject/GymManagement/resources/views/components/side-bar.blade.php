@php
    $routeName = request()->route()->getName();
@endphp
<div class="sidebar sidebar-style-2" data-background-color="dark2">
    <div class="sidebar-wrapper scrollbar scrollbar-inner">
        <div class="sidebar-content">
            <div class="user">
                <div class="avatar-sm float-left mr-2">
                    <img src="/assets/img/profile.jpg" alt="..." class="avatar-img rounded-circle">
                </div>
                <div class="info">
                    <a data-toggle="collapse" href="#collapseExample" aria-expanded="true">
                        <span>
                            {{ Auth::user()->nom }} {{ Auth::user()->prenom }}
                            <span class="user-level">{{ Auth::user()->role->nom }}</span>
                            <span class="caret"></span>
                        </span>
                    </a>
                    <div class="clearfix"></div>

                </div>
            </div>
            <ul class="nav nav-primary">
                @if (Auth::user()->role->nom == 'Adhérent')
                    <li class="nav-item">
                        <a href="{{ route('home') }}">
                            <i class="fas fa-home"></i>
                            <p>Accueil</p>
                        </a>
                    </li>
                @endif
                <li @class(['nav-item', 'active' => $routeName == 'dashboard'])>
                    <a href="{{ route('dashboard') }}">
                        <i class="fas fa-qrcode"></i>
                        <p>Tableau de bord</p>
                    </a>
                </li>
                @if (Auth::user()->role->nom == 'Gestionnaire')
                    <li @class([
                        'nav-item',
                        'active' =>
                            $routeName == 'manager.adherents.index' ||
                            $routeName == 'manager.adherents.create' ||
                            $routeName == 'manager.adherents.edit' ||
                            $routeName == 'manager.adherents.show',
                    ])>
                        <a href="{{ route('manager.adherents.index') }}">
                            <i class="fas fa-address-card"></i>
                            <p>Adhérents</p>
                        </a>
                    </li>
                @endif
                @if (Auth::user()->role->nom == 'Gestionnaire')
                    <li @class([
                        'nav-item',
                        'active' =>
                            $routeName == 'manager.salles.index' ||
                            $routeName == 'manager.salles.create' ||
                            $routeName == 'manager.salles.edit' ||
                            $routeName == 'manager.salles.show',
                    ])>
                        <a href="{{ route('manager.salles.index') }}">
                            <i class="fas fa-address-card"></i>
                            <p>Salles</p>
                        </a>
                    </li>
                @endif
                @if (Auth::user()->role->nom == 'Gestionnaire')
                    <li @class([
                        'nav-item',
                        'active' =>
                            $routeName == 'manager.cours.index' ||
                            $routeName == 'manager.cours.create' ||
                            $routeName == 'manager.cours.edit' ||
                            $routeName == 'manager.cours.show',
                    ])>
                        <a href="{{ route('manager.cours.index') }}">
                            <i class="fas fa-address-card"></i>
                            <p>Cours</p>
                        </a>
                    </li>
                @endif
                
                @if (Auth::user()->role->nom == 'Gestionnaire')
                    <li @class([
                        'nav-item',
                        'active' =>
                            $routeName == 'manager.coachs.index' ||
                            $routeName == 'manager.coachs.create' ||
                            $routeName == 'manager.coachs.edit' ||
                            $routeName == 'manager.coachs.show',
                    ])>
                        <a href="{{ route('manager.coachs.index') }}">
                            <i class="fas fa-address-card"></i>
                            <p>Coachs</p>
                        </a>
                    </li>
                @endif
                @if (Auth::user()->role->nom == 'Administrateur')
                    <li @class([
                        'nav-item',
                        'active' =>
                            $routeName == 'admin.gymnases.index' ||
                            $routeName == 'admin.gymnases.create' ||
                            $routeName == 'admin.gymnases.edit' ||
                            $routeName == 'admin.gymnases.show',
                    ])>
                        <a href="{{ route('admin.gymnases.index') }}">
                            <i class="fas fa-address-book"></i>
                            <p>Gymnases</p>
                        </a>
                    </li>
                @endif
                @if (Auth::user()->role->nom == 'Administrateur')
                    <li @class([
                        'nav-item',
                        'active' =>
                            $routeName == 'admin.managers.index' ||
                            $routeName == 'admin.managers.create' ||
                            $routeName == 'admin.managers.edit' ||
                            $routeName == 'admin.managers.show',
                    ])>
                        <a href="{{ route('admin.managers.index') }}">
                            <i class="fas fa-address-book"></i>
                            <p>Gestionnaires</p>
                        </a>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</div>
