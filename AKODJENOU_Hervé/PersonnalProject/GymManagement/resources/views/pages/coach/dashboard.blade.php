@extends('layouts.app')
@section('title', 'Tableau de bord')
@section('content')
    <div class="main-panel">
        <div class="content">
            <div class="page-inner">
                <div class="mt-2 mb-4">
                    <h2 class="text-white pb-2">Content de vous, {{ Auth::user()->prenom }}</h2>
                    <h5 class="text-white op-7 mb-4">Hier, j'étais intelligent, alors je voulais changer le monde.
                        Aujourd'hui je suis
                        sage, alors je me change.</h5>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="card card-dark bg-primary-gradient">
                            <div class="card-body pb-0">
                                <h2 class="mb-2" style="font-size: 50px !important">{{ $nombreCours }}</h2>
                                <p>Nombre de cours</p>
                                <div class="pull-in sparkline-fix chart-as-background">
                                    <div id="lineChart"><canvas width="327" height="70"
                                            style="display: inline-block; width: 327px; height: 70px; vertical-align: top;"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card card-dark bg-secondary-gradient">
                            <div class="card-body pb-0">
                                <h2 class="mb-2" style="font-size: 50px !important">{{ $nombreTotalSeances }}</h2>
                                <p>Nombre de séances</p>
                                <div class="pull-in sparkline-fix chart-as-background">
                                    <div id="lineChart2"><canvas width="327" height="70"
                                            style="display: inline-block; width: 327px; height: 70px; vertical-align: top;"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-5">
                    <h2 class="text-center"
                        style="font-size: 25px !important; color: white !important; font-weight: bold !important">Liste des
                        séances</h2>
                </div>
                <div class="row mt-5">
                    <div class="col-12">
                        <table class="table table-striped text-white">
                            <thead>
                                <tr>
                                    <th>Cours</th>
                                    <th>Libellé de la séance</th>
                                    <th>Date</th>
                                    <th>Date de début</th>
                                    <th>Date de fin</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($coursAvecSeances as $cours)
                                    @foreach ($cours->seances as $seance)
                                        <tr>
                                            <td>{{ $cours->nom }}</td>
                                            <td>{{ $seance->nom }}</td>
                                            <td>{{ $seance->date }}</td>
                                            <td>{{ $seance->heure_debut }}</td>
                                            <td>{{ $seance->heure_fin }}</td>
                                        </tr>
                                    @endforeach
                                @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
