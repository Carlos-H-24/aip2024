@extends('layouts.app')
@section('title', 'Tableau de bord')
@section('content')
    <div class="main-panel">
        <div class="content">
            <div class="page-inner">
                <div class="mt-2 mb-4">
                    <h2 class="text-white pb-2">Content de vous, {{ Auth::user()->prenom }}</h2>
                    <h5 class="text-white op-7 mb-4">Hier, j'étais intelligent, alors je voulais changer le monde.
                        Aujourd'hui je suis
                        sage, alors je me change.</h5>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="card card-dark bg-primary-gradient">
                            <div class="card-body pb-0">
                                <h2 class="mb-2" style="font-size: 50px !important">{{$gymnases}}</h2>
                                <p>Nombre de gymnases</p>
                                <div class="pull-in sparkline-fix chart-as-background">
                                    <div id="lineChart"><canvas width="327" height="70"
                                            style="display: inline-block; width: 327px; height: 70px; vertical-align: top;"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card card-dark bg-secondary-gradient">
                            <div class="card-body pb-0">
                                <h2 class="mb-2" style="font-size: 50px !important">{{$adherentsTotal}}</h2>
                                <p>Nombre de total d'adhérent</p>
                                <div class="pull-in sparkline-fix chart-as-background">
                                    <div id="lineChart2"><canvas width="327" height="70"
                                            style="display: inline-block; width: 327px; height: 70px; vertical-align: top;"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card card-dark bg-success2">
                            <div class="card-body pb-0">
                                <h2 class="mb-2" style="font-size: 50px !important">{{$coachsTotal}}</h2>
                                <p>Nombre de coachs</p>
                                <div class="pull-in sparkline-fix chart-as-background">
                                    <div id="lineChart3"><canvas width="327" height="100"
                                            style="display: inline-block; width: 327px; height: 70px; vertical-align: top;"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="card card-dark bg-primary-gradient">
                            <div class="card-body pb-0">
                                <h2 class="mb-2" style="font-size: 50px !important">{{$gymnases}}</h2>
                                <p>Nombre de gymnases</p>
                                <div class="pull-in sparkline-fix chart-as-background">
                                    <div id="lineChart6"><canvas width="327" height="70"
                                            style="display: inline-block; width: 327px; height: 70px; vertical-align: top;"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card card-dark bg-secondary-gradient">
                            <div class="card-body pb-0">
                                <h2 class="mb-2" style="font-size: 50px !important">{{$adherentsTotal}}</h2>
                                <p>Nombre de total d'adhérent</p>
                                <div class="pull-in sparkline-fix chart-as-background">
                                    <div id="lineChart5"><canvas width="327" height="70"
                                            style="display: inline-block; width: 327px; height: 70px; vertical-align: top;"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card card-dark bg-success2">
                            <div class="card-body pb-0">
                                <h2 class="mb-2" style="font-size: 50px !important">{{$coachsTotal}}</h2>
                                <p>Nombre de coachs</p>
                                <div class="pull-in sparkline-fix chart-as-background">
                                    <div id="lineChart4"><canvas width="327" height="100"
                                            style="display: inline-block; width: 327px; height: 70px; vertical-align: top;"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
