@extends('layouts.app')
@section('title', 'Admin | Modification d\'un gestionnaire')
@section('content')
    <div class="main-panel">
        <div class="content">
            <div class="page-inner">
                <div class="mt-2 mb-4 d-flex justify-content-between">
                    <h4 class="page-title">Modification</h4>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-stats card-round">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <form action="{{ route('admin.managers.update', $manager) }}" method="post"
                                            class="form" enctype="multipart/form-data">
                                            @method('PUT')
                                            @csrf

                                            <div class="row">
                                                <div
                                                    class="form-group col-md-6 @error('nom') has-error has-feedback @enderror mt-3">
                                                    <label for="nom" class="form-label">Nom </label>
                                                    <input type="text" class="form-control" id="nom" name="nom"
                                                        value="{{ old('nom', $manager->nom) }}">
                                                    @error('nom')
                                                        <span
                                                            class="form-text text-muted text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>

                                                <div
                                                    class="form-group col-md-6 @error('prenom') has-error has-feedback @enderror mt-3">
                                                    <label for="prenom" class="form-label">Prenom </label>
                                                    <input type="text" class="form-control" id="prenom" name="prenom"
                                                        value="{{ old('nom', $manager->prenom) }}">
                                                    @error('prenom')
                                                        <span
                                                            class="form-text text-muted text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="row">
                                
                                                <div
                                                    class="form-group col-12 @error('adresse') has-error has-feedback @enderror mt-3">
                                                    <label for="adresse" class="form-label">Adresse </label>
                                                    <input type="text" class="form-control" id="adresse" name="adresse"
                                                        value="{{ old('adresse', $manager->adresse) }}">
                                                    @error('adresse')
                                                        <span
                                                            class="form-text text-muted text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="row">

                                                <div
                                                    class="form-group col-md-6 @error('telephone') has-error has-feedback @enderror mt-3">
                                                    <label for="telephone" class="form-label">Télephone </label>
                                                    <input type="tel" class="form-control" id="telephone"
                                                        name="telephone" value="{{ old('telephone', $manager->telephone) }}">
                                                    @error('telephone')
                                                        <span
                                                            class="form-text text-muted text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>


                                                <div class="form-group col-md-6 mt-3">
                                                    <label for="sexe" class="form-label">Sexe</label>
                                                    <select class="form-control" id="sexe" name="sexe">
                                                        <option value="Masculin">Masculin</option>
                                                        <option value="Féminin">Féminin</option>
                                                        <option value="Autre">Autre</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div
                                                    class="form-group col-md-6 @error('date_naissance') has-error has-feedback @enderror ">
                                                    <label for="date_naissance" class="form-label">Date de naissance
                                                    </label>
                                                    <input type="date" class="form-control" id="date_naissance"
                                                        name="date_naissance" value="{{ old('date_naissance',$manager->date_naissance) }}">
                                                    @error('date_naissance')
                                                        <span
                                                            class="form-text text-muted text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>

                                                <div class="form-group col-md-6">
                                                    <label for="departement" class="form-label">Gymnase</label>
                                                    <select id="departement" name="gymnase_id" class="form-control">
                                                        @foreach ($gymnases as $gymnase)
                                                            <option value="{{ $gymnase->id }}">{{ $gymnase->nom }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <button type="submit" class="btn btn-primary mt-4">Modifier</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            $('#basic-datatables').DataTable({});

            $('#multi-filter-select').DataTable({
                "pageLength": 5,
                initComplete: function() {
                    this.api().columns().every(function() {
                        var column = this;
                        var select = $(
                                '<select class="form-control"><option value=""></option></select>'
                            )
                            .appendTo($(column.footer()).empty())
                            .on('change', function() {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );

                                column
                                    .search(val ? '^' + val + '$' : '', true, false)
                                    .draw();
                            });

                        column.data().unique().sort().each(function(d, j) {
                            select.append('<option value="' + d + '">' + d +
                                '</option>')
                        });
                    });
                }
            });

            // Add Row
            $('#add-row').DataTable({
                "pageLength": 5,
            });

            var action =
                '<td> <div class="form-button-action"> <button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task"> <i class="fa fa-edit"></i> </button> <button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove"> <i class="fa fa-times"></i> </button> </div> </td>';

            $('#addRowButton').click(function() {
                $('#add-row').dataTable().fnAddData([
                    $("#addName").val(),
                    $("#addPosition").val(),
                    $("#addOffice").val(),
                    action
                ]);
                $('#addRowModal').modal('hide');

            });



        });
    </script>
@endsection
