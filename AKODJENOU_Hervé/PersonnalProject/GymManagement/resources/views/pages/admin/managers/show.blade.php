@extends('layouts.app')
@section('title', 'Admin | Détail d\'un gestionnaire')
@section('content')
    <style>
        .detail * {
            color: white !important;
            font-size: 19px !important
        }

        .titre {
            font-size: 30px !important
        }
    </style>
    <div class="main-panel">
        <div class="content">
            <div class="page-inner">
                <div class="mt-2 mb-4 d-flex justify-content-between">
                    <h4 class="page-title">Détail</h4>
                </div>
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="card card-post card-round">

                            <div class="card-body detail">
                                @foreach ($manager as $data)
                                    <h2 class="text-center titre">{{ $data->nom }} {{ $data->prenom }}</h2>
                                    <div class="d-flex mt-5 justify-content-between">
                                        <div class="info-post ml-2">
                                            <p class="username">Email : </p>
                                            <p class="date text-muted">{{ $data->email }}</p>
                                        </div>
                                        <div class="info-post ml-2">
                                            <p class="username">Gymnase : </p>
                                            <p class="date text-muted">
                                                @foreach ($data->appartenirs as $appartenir)
                                                    {{ $appartenir->gymnase->nom }}
                                                @endforeach
                                            </p>
                                        </div>
                                    </div>
                                    <div class="separator-solid"></div>
                                    <p>Adresse : {{ $data->adresse }} </p>
                                    <p>Telephone : {{ $data->telephone }} </p>
                                    <p>Sexe : {{ $data->sexe }} </p>
                                    <p>Date de naissance : {{ $data->date_naissance }}</p>
                                    
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            $('#basic-datatables').DataTable({});

            $('#multi-filter-select').DataTable({
                "pageLength": 5,
                initComplete: function() {
                    this.api().columns().every(function() {
                        var column = this;
                        var select = $(
                                '<select class="form-control"><option value=""></option></select>'
                            )
                            .appendTo($(column.footer()).empty())
                            .on('change', function() {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );

                                column
                                    .search(val ? '^' + val + '$' : '', true, false)
                                    .draw();
                            });

                        column.data().unique().sort().each(function(d, j) {
                            select.append('<option value="' + d + '">' + d +
                                '</option>')
                        });
                    });
                }
            });

            // Add Row
            $('#add-row').DataTable({
                "pageLength": 5,
            });

            var action =
                '<td> <div class="form-button-action"> <button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task"> <i class="fa fa-edit"></i> </button> <button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove"> <i class="fa fa-times"></i> </button> </div> </td>';

            $('#addRowButton').click(function() {
                $('#add-row').dataTable().fnAddData([
                    $("#addName").val(),
                    $("#addPosition").val(),
                    $("#addOffice").val(),
                    action
                ]);
                $('#addRowModal').modal('hide');

            });
        });
    </script>
@endsection
