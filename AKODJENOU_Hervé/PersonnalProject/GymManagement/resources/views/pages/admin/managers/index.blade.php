@extends('layouts.app')
@section('title', 'Admin | Liste des gestionnaires')
@section('content')
    <style>
        .empty {
            text-align: center;
            font-weight: bold;
            font-size: 20px;
        }

        .page-title {
            font-size: 21px !important
        }
    </style>
    <div class="main-panel">

        <div class="content">
            <div class="page-inner">
                <div class="mt-2 mb-4 d-flex justify-content-between">
                    <h4 class="page-title">Liste des gestionnaires</h4>
                    
                </div>
                @if (session('successCreate'))
                    <div id="alertS7"></div>
                @endif
                @if (session('successUpdate'))
                    <div id="alertS8"></div>
                @endif
                @if (session('successDelete'))
                    <div id="alertS9"></div>
                @endif
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="gymnases-table" class="display table table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th>#ID</th>
                                                <th>Nom</th>
                                                <th>Prenom</th>
                                                <th>Email</th>
                                                <th>Gymnase</th>
                                                <th class="d-flex justify-content-center">Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($gestionnaires as $data)
                                                <tr>
                                                    <td>{{ $data->id }}</td>
                                                    <td>{{ $data->nom }}</td>
                                                    <td>{{ $data->prenom }}</td>
                                                    <td>{{ $data->email }}</td>
                                                    <td>
                                                        @foreach ($data->appartenirs as $appartenir)
                                                            {{ $appartenir->gymnase->nom }}
                                                        @endforeach
                                                    </td>
                                                    <td>
                                                        <div class="d-flex gap-2  justify-content-center">
                                                            <a href="{{ route('admin.managers.show', $data) }}"
                                                                class="btn btn-success mx-2">
                                                                <i class="far fa-eye" style="font-size: 1.3rem"></i></a>
                                                            <a href="{{ route('admin.managers.edit', $data) }}"
                                                                class="btn btn-primary mx-4"><i class="fas fa-edit"
                                                                    style="font-size: 1.3rem"></i></a>
                                                            <form action="{{ route('admin.managers.destroy', $data) }}"
                                                                method="post" id="form">
                                                                @csrf
                                                                @method('delete')
                                                                <button class="btn btn-danger show_confirm_3">
                                                                    <i class="fas fa-trash-alt"
                                                                        style="font-size: 1.3rem"></i>
                                                                </button>
                                                            </form>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    @if ($gestionnaires->total() == 0)
                                        <div class="empty text-danger text-center">
                                            Aucune données trouvées
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-end mx-5">
                            {{ $gestionnaires->links('shared.paginator') }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection
    @section('script')
        <script>
            function showAlertSuccess(alertId) {
                var alertElement = document.getElementById(alertId);
                if (alertElement) {
                    swal("Succès!", "Le gestionnaire a été bien crée!", {
                        icon: "success",
                        buttons: {
                            confirm: {
                                className: 'btn btn-success'
                            }
                        },
                    });

                }
            }
            showAlertSuccess('alertS7')

            function showAlertUpdate(alertId) {
                var alertElement = document.getElementById(alertId);
                if (alertElement) {
                    swal("Succès!", "Le gestionnaire a modifié bien crée!", {
                        icon: "success",
                        buttons: {
                            confirm: {
                                className: 'btn btn-success'
                            }
                        },
                    });

                }
            }
            showAlertUpdate("alertS8");

            function showAlertDelete(alertId) {
                var alertElement = document.getElementById(alertId);
                if (alertElement) {
                    swal("Succès!", "Le gestionnaire a supprimé bien crée!", {
                        icon: "success",
                        buttons: {
                            confirm: {
                                className: 'btn btn-success'
                            }
                        },
                    });

                }
            }
            showAlertDelete("alertS9");
        </script>
        <script type="text/javascript">
            $('.show_confirm_3').click(function(event) {
                var form = $(this).closest("form");
                var name = $(this).data("name");
                event.preventDefault();
                swal({
                        title: `Etes-vous sûr de vouloir supprimer ce gestionnaire?`,
                        icon: "warning",
                        buttons: {
                            cancel: "Annuler",
                            confirm: "Supprimer"
                        },
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                        if (willDelete) {
                            form.submit();
                        }
                    });
            });
        </script>
    @endsection
