@extends('layouts.app')
@section('title', 'Admin | Ajout d\'un gymnase')
@section('content')
    <div class="main-panel">
        <div class="content">
            <div class="page-inner">
                <div class="mt-2 mb-4 d-flex justify-content-between">
                    <h4 class="page-title">Ajouter un noueau Gymnase</h4>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-stats card-round">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <form action="{{ route('admin.gymnases.store') }}" method="post" class="form"
                                            enctype="multipart/form-data">
                                            @csrf
                                            <div class="row">
                                                <div
                                                    class="form-group col-md-6 @error('nom') has-error has-feedback @enderror mt-3">
                                                    <label for="nom" class="form-label">Nom </label>
                                                    <input type="text" class="form-control" id="nom" name="nom"
                                                        value="{{ old('nom') }}">
                                                    @error('nom')
                                                        <span
                                                            class="form-text text-muted text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>

                                                <div
                                                    class="form-group col-md-6 @error('email') has-error has-feedback @enderror mt-3">
                                                    <label for="email" class="form-label">Email </label>
                                                    <input type="text" class="form-control" id="email" name="email"
                                                        value="{{ old('email') }}">
                                                    @error('email')
                                                        <span
                                                            class="form-text text-muted text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div
                                                    class="form-group col-md-6 @error('adresse') has-error has-feedback @enderror mt-3">
                                                    <label for="adresse" class="form-label">Adresse </label>
                                                    <input type="text" class="form-control" id="adresse" name="adresse"
                                                        value="{{ old('adresse') }}">
                                                    @error('adresse')
                                                        <span
                                                            class="form-text text-muted text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                                <div
                                                    class="form-group col-md-6 @error('description') has-error has-feedback @enderror mt-3">
                                                    <label for="description" class="form-label">Description </label>
                                                    <textarea class="form-control" id="description" name="description" cols="30">{{ old('description') }}</textarea>
                                                    @error('description')
                                                        <span
                                                            class="form-text text-muted text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div
                                                    class="form-group col-md-6 @error('longitude') has-error has-feedback @enderror mt-3">
                                                    <label for="longitude" class="form-label">Longitude</label>
                                                    <input type="number" class="form-control" id="longitude"
                                                        name="longitude" value="{{ old('longitude') }}">
                                                    @error('longitude')
                                                        <span
                                                            class="form-text text-muted text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                                <div
                                                    class="form-group col-md-6 @error('latitude') has-error has-feedback @enderror mt-3">
                                                    <label for="latitude" class="form-label">Latitude</label>
                                                    <input type="number" class="form-control" id="latitude"
                                                        name="latitude" value="{{ old('latitude') }}">
                                                    @error('latitude')
                                                        <span
                                                            class="form-text text-muted text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>



                                            <div class="row mt-3">
                                                <div
                                                    class="form-group col-md-4 @error('telephone') has-error has-feedback @enderror ">
                                                    <label for="telephone" class="form-label">Téléphone </label>
                                                    <input type="tel" class="form-control" id="telephone"
                                                        name="telephone" value="{{ old('telephone') }}">
                                                    @error('telephone')
                                                        <span
                                                            class="form-text text-muted text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>

                                                <div class="form-group col-md-4">
                                                    <label for="departement" class="form-label">Département</label>
                                                    <select id="departement" name="departement" class="form-control">
                                                        @foreach ($departements as $departement)
                                                            <option value="{{ $departement->id }}">{{ $departement->nom }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="commune" class="form-label">Commune</label>
                                                    <select id="commune" name="commune_id" class="form-control"></select>
                                                </div>
                                            </div>



                                            <h1 class="text-center text-white m-5" style="font-size: 25px !important; margin-top: 100px !important">
                                                Gestionnaire</h1>
                                            
                                            <div class="row">
                                                <div class="form-group col-md-6 @error('nom_ges') has-error has-feedback @enderror mt-3">
                                                    <label for="nom_ges" class="form-label">Nom</label>
                                                    <input type="text" class="form-control" id="nom_ges" name="nom_ges"
                                                        value="{{ old('nom_ges') }}">
                                                    @error('nom_ges')
                                                        <span class="form-text text-muted text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                                <div class="form-group col-md-6 @error('prenom_ges') has-error has-feedback @enderror mt-3">
                                                    <label for="prenom_ges" class="form-label">Prénom </label>
                                                    <input type="text" class="form-control" id="prenom_ges" name="prenom_ges"
                                                        value="{{ old('prenom_ges') }}">
                                                    @error('prenom_ges')
                                                        <span class="form-text text-muted text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>

                                           <div class="row">
                                            <div
                                            class="form-group col-md-6 @error('email_ges') has-error has-feedback @enderror mt-3">
                                            <label for="email_ges" class="form-label">Email </label>
                                            <input type="text" class="form-control" id="email_hges"
                                                name="email_ges" value="{{ old('email_ges') }}">
                                            @error('email_ges')
                                                <span class="form-text text-muted text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div
                                            class="form-group  col-md-6 @error('adresse_ges') has-error has-feedback @enderror mt-3">
                                            <label for="adresse_ges" class="form-label">Adresse</label>
                                            <input type="text" class="form-control" id="adresse_ges"
                                                name="adresse_ges" value="{{ old('adresse_ges') }}">
                                            @error('adresse_ges')
                                                <span class="form-text text-muted text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                           </div>

                                            <div class="row">   
                                                <div
                                                class="form-group col-md-4  @error('telephone_ges') has-error has-feedback @enderror mt-3">
                                                <label for="telephone_ges" class="form-label">Télephone </label>
                                                <input type="tel" class="form-control" id="telephone_ges"
                                                    name="telephone_ges" value="{{ old('telephone_ges') }}">
                                                @error('telephone_ges')
                                                    <span class="form-text text-muted text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                            <div class="form-group col-md-4 mt-3">
                                                <label for="sexe" class="form-label">Sexe</label>
                                                <select class="form-control" id="sexe" name="sexe_ges">
                                                    <option value="Masculin">Masculin</option>
                                                    <option value="Féminin">Féminin</option>
                                                    <option value="Autre">Autre</option>
                                                </select>
                                            </div>
                                            <div
                                                class="form-group col-md-4 @error('date_naissance_ges') has-error has-feedback @enderror mt-3">
                                                <label for="date_naissance_ges" class="form-label">Date de naissance </label>
                                                <input type="date" class="form-control" id="date_naissance_ges"
                                                    name="date_naissance_ges" value="{{ old('date_naissance_ges') }}">
                                                @error('date_naissance_ges')
                                                    <span class="form-text text-muted text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                            </div>
                                            

                                            <button type="submit" class="btn btn-primary mt-4">Enrégistrer</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            $('#basic-datatables').DataTable({});

            $('#multi-filter-select').DataTable({
                "pageLength": 5,
                initComplete: function() {
                    this.api().columns().every(function() {
                        var column = this;
                        var select = $(
                                '<select class="form-control"><option value=""></option></select>'
                            )
                            .appendTo($(column.footer()).empty())
                            .on('change', function() {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );

                                column
                                    .search(val ? '^' + val + '$' : '', true, false)
                                    .draw();
                            });

                        column.data().unique().sort().each(function(d, j) {
                            select.append('<option value="' + d + '">' + d +
                                '</option>')
                        });
                    });
                }
            });

            // Add Row
            $('#add-row').DataTable({
                "pageLength": 5,
            });

            var action =
                '<td> <div class="form-button-action"> <button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task"> <i class="fa fa-edit"></i> </button> <button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove"> <i class="fa fa-times"></i> </button> </div> </td>';

            $('#addRowButton').click(function() {
                $('#add-row').dataTable().fnAddData([
                    $("#addName").val(),
                    $("#addPosition").val(),
                    $("#addOffice").val(),
                    action
                ]);
                $('#addRowModal').modal('hide');

            });

            loadCommunes($('#departement').val());

            $('#departement').change(function() {
                // Appel la fonction pour charger les communes à chaque changement de département
                loadCommunes($(this).val());
            });

            function loadCommunes(departementId) {
                // Appel AJAX pour récupérer les communes en fonction du département
                $.ajax({
                    url: '/get-communes/' + departementId,
                    type: 'GET',
                    success: function(data) {
                        // Supprimez les options actuelles du champ Commune
                        $('#commune').empty();

                        // Ajoutez les nouvelles options en fonction des données reçues
                        data.forEach(function(commune) {
                            $('#commune').append('<option value="' + commune.id + '">' + commune
                                .nom + '</option>');
                        });
                    },
                    error: function(error) {
                        console.log(error);
                    }
                });
            }
        });
    </script>
@endsection
