@extends('layouts.app')
@section('title', 'Admin | Liste des gymnases')
@section('content')
    <style>
        .empty {
            text-align: center;
            font-weight: bold;
            font-size: 20px;
        }

        .page-title {
            font-size: 21px !important
        }
    </style>
    <div class="main-panel">

        <div class="content">
            <div class="page-inner">
                <div class="mt-2 mb-4 d-flex justify-content-between">
                    <h4 class="page-title">Liste des gymnases</h4>
                    <a href="{{ route('admin.gymnases.create') }}" class="btn btn-primary fw-bold add"
                        style="font-size: 18px !important">
                        <i class="fas fa-plus-circle"></i>
                        Ajouter
                    </a>
                </div>
                @if (session('successCreate'))
                    <div id="alertS1"></div>
                @endif
                @if (session('successUpdate'))
                    <div id="alertS2"></div>
                @endif
                @if (session('successDelete'))
                    <div id="alertS3"></div>
                @endif
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="gymnases-table" class="display table table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th>#ID</th>
                                                <th>Nom</th>
                                                <th>Adresse</th>
                                                <th>Email</th>
                                                <th class="d-flex justify-content-center">Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($gymnases as $data)
                                                <tr>
                                                    <td>{{ $data->id }}</td>
                                                    <td>{{ $data->nom }}</td>
                                                    <td>{{ $data->adresse }}</td>
                                                    <td>{{ $data->email }}</td>
                                                    <td>
                                                        <div class="d-flex gap-2  justify-content-center">
                                                            <a href="{{ route('admin.gymnases.show', $data) }}"
                                                                class="btn btn-success mx-2">
                                                                <i class="far fa-eye" style="font-size: 1.3rem"></i></a>
                                                            <a href="{{ route('admin.gymnases.edit', $data) }}"
                                                                class="btn btn-primary mx-4"><i class="fas fa-edit"
                                                                    style="font-size: 1.3rem"></i></a>
                                                            <form action="{{ route('admin.gymnases.destroy', $data) }}"
                                                                method="post" id="form">
                                                                @csrf
                                                                @method('delete')
                                                                <button class="btn btn-danger show_confirm">
                                                                    <i class="fas fa-trash-alt"
                                                                        style="font-size: 1.3rem"></i>
                                                                </button>
                                                            </form>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    @if ($gymnases->total() == 0)
                                        <div class="empty text-danger  text-center">
                                            Aucune données trouvées
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-end mx-5">
                            {{ $gymnases->links('shared.paginator') }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection
    @section('script')
        <script>
            function showAlertSuccess(alertId) {
                var alertElement = document.getElementById(alertId);
                if (alertElement) {
                    swal("Succès!", "Le gymnase a été bien crée!", {
                        icon: "success",
                        buttons: {
                            confirm: {
                                className: 'btn btn-success'
                            }
                        },
                    });

                }
            }
            showAlertSuccess('alertS1')

            function showAlertUpdate(alertId) {
                var alertElement = document.getElementById(alertId);
                if (alertElement) {
                    swal("Succès!", "Le gymnase a modifié bien crée!", {
                        icon: "success",
                        buttons: {
                            confirm: {
                                className: 'btn btn-success'
                            }
                        },
                    });

                }
            }
            showAlertUpdate("alertS2");

            function showAlertDelete(alertId) {
                var alertElement = document.getElementById(alertId);
                if (alertElement) {
                    swal("Succès!", "Le gymnase a supprimé bien crée!", {
                        icon: "success",
                        buttons: {
                            confirm: {
                                className: 'btn btn-success'
                            }
                        },
                    });

                }
            }
            showAlertDelete("alertS3");
        </script>
        <script type="text/javascript">
            $('.show_confirm').click(function(event) {
                var form = $(this).closest("form");
                var name = $(this).data("name");
                event.preventDefault();
                swal({
                        title: `Etes-vous sûr de vouloir supprimer ce gymnase?`,
                        icon: "warning",
                        buttons: {
                            cancel: "Annuler",
                            confirm: "Supprimer"
                        },
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                        if (willDelete) {
                            form.submit();
                        }
                    });
            });
        </script>
    @endsection
