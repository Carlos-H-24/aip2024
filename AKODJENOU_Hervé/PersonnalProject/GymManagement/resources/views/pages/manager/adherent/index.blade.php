@extends('layouts.app')
@section('title', 'Manager | Liste des adhérents')
@section('content')
    <div class="main-panel">
        <div class="content">
            <div class="page-inner">
                <div class="mt-2 mb-4 d-flex justify-content-between">
                    <h4 class="page-title">Liste des adhérents</h4>
                    <a href="{{ route('manager.adherents.create') }}" class="btn btn-primary fw-bold add"
                        style="font-size: 18px !important">
                        <i class="fas fa-plus-circle"></i>
                        Ajouter
                    </a>
                </div>
                @if (session('successCreate'))
                    <div id="alertS4"></div>
                @endif
                @if (session('successUpdate'))
                    <div id="alertS5"></div>
                @endif
                @if (session('successDelete'))
                    <div id="alertS6"></div>
                @endif
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="basic-datatables" class="display table table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th>#ID</th>
                                                <th>Nom</th>
                                                <th>Prénom</th>
                                                <th>Email</th>
                                                <th>Adresse</th>
                                                <th class="d-flex justify-content-center">Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($adherents as $data)
                                                <tr>
                                                    <td>{{ $data->user->id }}</td>
                                                    <td>{{ $data->user->nom }}</td>
                                                    <td>{{ $data->user->prenom }}</td>
                                                    <td>{{ $data->user->email }}</td>
                                                    <td>{{ $data->user->adresse }}</td>
                                                    <td>
                                                        <div class="d-flex gap-2  justify-content-center">
                                                            <a href="{{ route('manager.adherents.show', $data->user) }}"
                                                                class="btn btn-success mx-2">
                                                                <i class="far fa-eye" style="font-size: 1.3rem"></i></a>
                                                            <a href="{{ route('manager.adherents.edit', $data->user) }}"
                                                                class="btn btn-primary mx-4"><i class="fas fa-edit"
                                                                    style="font-size: 1.3rem"></i></a>
                                                            <form action="{{ route('manager.adherents.destroy', $data->user) }}"
                                                                method="post" id="form">
                                                                @csrf
                                                                @method('delete')
                                                                <button class="btn btn-danger show_confirm2">
                                                                    <i class="fas fa-trash-alt"
                                                                        style="font-size: 1.3rem"></i>
                                                                </button>
                                                            </form>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>

                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    @if ($adherents->total() == 0)
                                        <div class="empty text-danger  text-center">
                                            Aucune données trouvées
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-end mx-5">
                            {{ $adherents->links('shared.paginator') }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        function showAlertSuccess(alertId) {
            var alertElement = document.getElementById(alertId);
            if (alertElement) {
                swal("Succès!", "L'adhérent a été bien crée!", {
                    icon: "success",
                    buttons: {
                        confirm: {
                            className: 'btn btn-success'
                        }
                    },
                });

            }
        }
        showAlertSuccess('alertS4')

        function showAlertUpdate(alertId) {
            var alertElement = document.getElementById(alertId);
            if (alertElement) {
                swal("Succès!", "L'adhérent a modifié bien crée!", {
                    icon: "success",
                    buttons: {
                        confirm: {
                            className: 'btn btn-success'
                        }
                    },
                });

            }
        }
        showAlertUpdate("alertS5");

        function showAlertDelete(alertId) {
            var alertElement = document.getElementById(alertId);
            if (alertElement) {
                swal("Succès!", "L'adhérent a supprimé bien crée!", {
                    icon: "success",
                    buttons: {
                        confirm: {
                            className: 'btn btn-success'
                        }
                    },
                });

            }
        }
        showAlertDelete("alertS6");
    </script>
    <script type="text/javascript">
        $('.show_confirm2').click(function(event) {
            var form = $(this).closest("form");
            var name = $(this).data("name");
            event.preventDefault();
            swal({
                    title: `Etes-vous sûr de vouloir supprimer cet adhérent?`,
                    icon: "warning",
                    buttons: {
                        cancel: "Annuler",
                        confirm: "Supprimer"
                    },
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        form.submit();
                    }
                });
        });
    </script>
@endsection
