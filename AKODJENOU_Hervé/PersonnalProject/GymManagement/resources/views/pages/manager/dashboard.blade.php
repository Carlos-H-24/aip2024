@extends('layouts.app')
@section('title', 'Tableau de bord')
@section('content')
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="mt-2 mb-4">
                <h2 class="text-white pb-2">Content de vous, {{ Auth::user()->prenom }}</h2>
                <h5 class="text-white op-7 mb-4">Hier, j'étais intelligent, alors je voulais changer le monde.
                    Aujourd'hui je suis
                    sage, alors je me change.</h5>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="card card-dark bg-primary-gradient">
                        <div class="card-body pb-0">
                            <h2 class="mb-2" style="font-size: 50px !important">{{$nbreAdherent}}</h2>
                            <p>Nombre d'adhérents</p>
                            <div class="pull-in sparkline-fix chart-as-background">
                                <div id="lineChart"><canvas width="327" height="70"
                                        style="display: inline-block; width: 327px; height: 70px; vertical-align: top;"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-dark bg-secondary-gradient">
                        <div class="card-body pb-0">
                            <h2 class="mb-2" style="font-size: 50px !important">{{$nbreCoach}}</h2>
                            <p>Nombre de coachs</p>
                            <div class="pull-in sparkline-fix chart-as-background">
                                <div id="lineChart2"><canvas width="327" height="70"
                                        style="display: inline-block; width: 327px; height: 70px; vertical-align: top;"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-dark bg-success2">
                        <div class="card-body pb-0">
                            <h2 class="mb-2" style="font-size: 50px !important">{{$nbreCours}}</h2>
                            <p>Nombre de cours</p>
                            <div class="pull-in sparkline-fix chart-as-background">
                                <div id="lineChart3"><canvas width="327" height="100"
                                        style="display: inline-block; width: 327px; height: 70px; vertical-align: top;"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="card card-dark bg-primary-gradient">
                        <div class="card-body pb-0">
                            <h2 class="mb-2" style="font-size: 50px !important">{{$nbreSalles}}</h2>
                            <p>Nombre de salles</p>
                            <div class="pull-in sparkline-fix chart-as-background">
                                <div id="lineChart6"><canvas width="327" height="70"
                                        style="display: inline-block; width: 327px; height: 70px; vertical-align: top;"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-dark bg-secondary-gradient">
                        <div class="card-body pb-0">
                            <h2 class="mb-2" style="font-size: 50px !important">{{$nbreSeancesEnAttente}}</h2>
                            <p>Nombre de séance en attente</p>
                            <div class="pull-in sparkline-fix chart-as-background">
                                <div id="lineChart5"><canvas width="327" height="70"
                                        style="display: inline-block; width: 327px; height: 70px; vertical-align: top;"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-dark bg-success2">
                        <div class="card-body pb-0">
                            <h2 class="mb-2" style="font-size: 50px !important">{{$nbreSeanceTerminees}}</h2>
                            <p>Nombre de séance terminées</p>
                            <div class="pull-in sparkline-fix chart-as-background">
                                <div id="lineChart4"><canvas width="327" height="100"
                                        style="display: inline-block; width: 327px; height: 70px; vertical-align: top;"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">Top 5 des cours les plus survis</div>
                        </div>
                        <div class="card-body">
                            <ol class="activity-feed">
                                @foreach ($topCours as $tpc)
                                <li class="feed-item feed-item-success">
                                    <time class="date" datetime="9-24">{{$tpc->nom}}</time>
                                    <span class="text">Coah : <a href="{{route('manager.coachs.show',$tpc->user)}}">
                                        {{$tpc->user->nom}} {{$tpc->user->prenom}}</a></span>
                                </li>
                                @endforeach
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
