@extends('layouts.app')
@section('title', 'Manager | Liste des salles')
@section('content')
    <div class="main-panel">
        <div class="content">
            <div class="page-inner">
                <div class="mt-2 mb-4 d-flex justify-content-between">
                    <h4 class="page-title">Liste des salles</h4>
                    <a href="{{ route('manager.salles.create') }}" class="btn btn-primary fw-bold add"
                        style="font-size: 18px !important">
                        <i class="fas fa-plus-circle"></i>
                        Ajouter
                    </a>
                </div>
                @if (session('successCreate'))
                    <div id="alertS10"></div>
                @endif
                @if (session('successUpdate'))
                    <div id="alertS11"></div>
                @endif
                @if (session('successDelete'))
                    <div id="alertS12"></div>
                @endif
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="basic-datatables" class="display table table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th>#ID</th>
                                                <th>Nom</th>
                                                <th>Superficie</th>
                                                <th>Capacité</th>
                                                <th class="d-flex justify-content-center">Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($salles as $data)
                                                <tr>
                                                    <td>{{ $data->id }}</td>
                                                    <td>{{ $data->nom }}</td>
                                                    <td>{{ $data->superficie }}</td>
                                                    <td>{{ $data->capacite }}</td>
                                                    
                                                    <td>
                                                        <div class="d-flex gap-2  justify-content-center">
                                                            
                                                            <a href="{{ route('manager.salles.edit', $data) }}"
                                                                class="btn btn-primary mx-4"><i class="fas fa-edit"
                                                                    style="font-size: 1.3rem"></i></a>
                                                            <form action="{{ route('manager.salles.destroy', $data) }}"
                                                                method="post" id="form">
                                                                @csrf
                                                                @method('delete')
                                                                <button class="btn btn-danger show_confirm_4">
                                                                    <i class="fas fa-trash-alt"
                                                                        style="font-size: 1.3rem"></i>
                                                                </button>
                                                            </form>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>

                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    @if ($salles->total() == 0)
                                        <div class="empty text-danger text-center">
                                            Aucune données trouvées
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-end mx-5">
                            {{ $salles->links('shared.paginator') }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        function showAlertSuccess(alertId) {
            var alertElement = document.getElementById(alertId);
            if (alertElement) {
                swal("Succès!", "La salle a été bien crée!", {
                    icon: "success",
                    buttons: {
                        confirm: {
                            className: 'btn btn-success'
                        }
                    },
                });

            }
        }
        showAlertSuccess('alertS10')

        function showAlertUpdate(alertId) {
            var alertElement = document.getElementById(alertId);
            if (alertElement) {
                swal("Succès!", "La salle a modifié bien crée!", {
                    icon: "success",
                    buttons: {
                        confirm: {
                            className: 'btn btn-success'
                        }
                    },
                });

            }
        }
        showAlertUpdate("alertS11");

        function showAlertDelete(alertId) {
            var alertElement = document.getElementById(alertId);
            if (alertElement) {
                swal("Succès!", "La salle a supprimé bien crée!", {
                    icon: "success",
                    buttons: {
                        confirm: {
                            className: 'btn btn-success'
                        }
                    },
                });

            }
        }
        showAlertDelete("alertS12");
    </script>
    <script type="text/javascript">
        $('.show_confirm_4').click(function(event) {
            var form = $(this).closest("form");
            var name = $(this).data("name");
            event.preventDefault();
            swal({
                    title: `Etes-vous sûr de vouloir supprimer cette salle?`,
                    icon: "warning",
                    buttons: {
                        cancel: "Annuler",
                        confirm: "Supprimer"
                    },
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        form.submit();
                    }
                });
        });
    </script>
@endsection
