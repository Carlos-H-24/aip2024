@extends('layouts.app')
@section('title', 'Manager | Modification d\'un cours ')
@section('content')
    <div class="main-panel">
        <div class="content">
            <div class="page-inner">
                <div class="mt-2 mb-4 d-flex justify-content-between">
                    <h4 class="page-title">Modification d'un cours</h4>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-stats card-round">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <form action="{{ route('manager.cours.update', $cours) }}" method="post"
                                            class="form" enctype="multipart/form-data">
                                            @csrf
                                            @method('PUT')
                                            <div
                                                class="form-group  @error('nom') has-error has-feedback @enderror mt-3">
                                                <label for="nom" class="form-label">Nom </label>
                                                <input type="text" class="form-control" id="nom" name="nom"
                                                    value="{{ old('nom', $cours->nom) }}">
                                                @error('nom')
                                                    <span class="form-text text-muted text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                            <div
                                                class="form-group  @error('description') has-error has-feedback @enderror mt-3">
                                                <label for="description" class="form-label">Description </label>
                                                <textarea class="form-control" id="description" name="description" cols="30">{{ old('description', $cours->description) }}</textarea>
                                                @error('description')
                                                    <span class="form-text text-muted text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                            <div>
                                                <label for="coach" class="form-label">Coach</label>
                                                <select class="form-control" id="coach" name="coach">
                                                    @foreach ($coachs as $coach)
                                                        <option value="{{ $coach->id }}">{{ $coach->nom }}
                                                            {{ $coach->prenom }}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <button type="submit" class="btn btn-primary mt-4">Modifier</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            $('#basic-datatables').DataTable({});

            $('#multi-filter-select').DataTable({
                "pageLength": 5,
                initComplete: function() {
                    this.api().columns().every(function() {
                        var column = this;
                        var select = $(
                                '<select class="form-control"><option value=""></option></select>'
                            )
                            .appendTo($(column.footer()).empty())
                            .on('change', function() {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );

                                column
                                    .search(val ? '^' + val + '$' : '', true, false)
                                    .draw();
                            });

                        column.data().unique().sort().each(function(d, j) {
                            select.append('<option value="' + d + '">' + d +
                                '</option>')
                        });
                    });
                }
            });

            // Add Row
            $('#add-row').DataTable({
                "pageLength": 5,
            });

            var action =
                '<td> <div class="form-button-action"> <button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task"> <i class="fa fa-edit"></i> </button> <button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove"> <i class="fa fa-times"></i> </button> </div> </td>';

            $('#addRowButton').click(function() {
                $('#add-row').dataTable().fnAddData([
                    $("#addName").val(),
                    $("#addPosition").val(),
                    $("#addOffice").val(),
                    action
                ]);
                $('#addRowModal').modal('hide');

            });
        });
    </script>
@endsection
