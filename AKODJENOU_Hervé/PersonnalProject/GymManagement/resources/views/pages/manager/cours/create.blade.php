@extends('layouts.app')
@section('title', 'Manager | Ajout d\'un cours ')
@section('content')
    <div class="main-panel">
        <div class="content">
            <div class="page-inner">
                <div class="mt-2 mb-4 d-flex justify-content-between">
                    <h4 class="page-title">Ajouter un nouveau cours</h4>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-stats card-round">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <form action="{{ route('manager.cours.store') }}" method="post" class="form"
                                            enctype="multipart/form-data">
                                            @csrf


                                            <div class="form-group  @error('nom') has-error has-feedback @enderror mt-3">
                                                <label for="nom" class="form-label">Nom </label>
                                                <input type="text" class="form-control" id="nom" name="nom"
                                                    value="{{ old('nom') }}">
                                                @error('nom')
                                                    <span class="form-text text-muted text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>

                                            <div
                                                class="form-group  @error('description') has-error has-feedback @enderror mt-3">
                                                <label for="description" class="form-label">Description </label>
                                                <textarea class="form-control" id="description" name="description" cols="30">{{ old('description') }}</textarea>
                                                @error('description')
                                                    <span class="form-text text-muted text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>

                                            <h1 class="mt-5 text-center text-white" style="font-size: 25px !important">Coach
                                                <h1>
                                                    <div class="row">
                                                        <div
                                                            class="form-group col-md-6 @error('nom_coach') has-error has-feedback @enderror mt-3">
                                                            <label for="nom_coach" class="form-label">Nom</label>
                                                            <input type="text" class="form-control" id="nom_coach"
                                                                name="nom_coach" value="{{ old('nom_coach') }}">
                                                            @error('nom_coach')
                                                                <span
                                                                    class="form-text text-muted text-danger">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                                                        <div
                                                            class="form-group col-md-6 @error('prenom_coach') has-error has-feedback @enderror mt-3">
                                                            <label for="prenom_coach" class="form-label">Prénom </label>
                                                            <input type="text" class="form-control" id="prenom_coach"
                                                                name="prenom_coach" value="{{ old('prenom_coach') }}">
                                                            @error('prenom_coach')
                                                                <span
                                                                    class="form-text text-muted text-danger">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div
                                                            class="form-group col-md-6 @error('email_coach') has-error has-feedback @enderror mt-3">
                                                            <label for="email_coach" class="form-label">Email </label>
                                                            <input type="text" class="form-control" id="email_hcoach"
                                                                name="email_coach" value="{{ old('email_coach') }}">
                                                            @error('email_coach')
                                                                <span
                                                                    class="form-text text-muted text-danger">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                                                        <div
                                                            class="form-group  col-md-6 @error('adresse_coach') has-error has-feedback @enderror mt-3">
                                                            <label for="adresse_coach" class="form-label">Adresse</label>
                                                            <input type="text" class="form-control" id="adresse_coach"
                                                                name="adresse_coach" value="{{ old('adresse_coach') }}">
                                                            @error('adresse_coach')
                                                                <span
                                                                    class="form-text text-muted text-danger">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div
                                                            class="form-group col-md-4  @error('telephone_coach') has-error has-feedback @enderror mt-3">
                                                            <label for="telephone_coach" class="form-label">Télephone </label>
                                                            <input type="tel" class="form-control" id="telephone_coach"
                                                                name="telephone_coach" value="{{ old('telephone_coach') }}">
                                                            @error('telephone_coach')
                                                                <span
                                                                    class="form-text text-muted text-danger">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                                                        <div class="form-group col-md-4 mt-3">
                                                            <label for="sexe" class="form-label">Sexe</label>
                                                            <select class="form-control" id="sexe" name="sexe_coach">
                                                                <option value="Masculin">Masculin</option>
                                                                <option value="Féminin">Féminin</option>
                                                                <option value="Autre">Autre</option>
                                                            </select>
                                                        </div>
                                                        <div
                                                            class="form-group col-md-4 @error('date_naissance_coach') has-error has-feedback @enderror mt-3">
                                                            <label for="date_naissance_coach" class="form-label">Date de
                                                                naissance </label>
                                                            <input type="date" class="form-control"
                                                                id="date_naissance_coach" name="date_naissance_coach"
                                                                value="{{ old('date_naissance_coach') }}">
                                                            @error('date_naissance_coach')
                                                                <span
                                                                    class="form-text text-muted text-danger">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                                                    </div>

                                                    <button type="submit" class="btn btn-success mt-4">Ajouter</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            $('#basic-datatables').DataTable({});

            $('#multi-filter-select').DataTable({
                "pageLength": 5,
                initComplete: function() {
                    this.api().columns().every(function() {
                        var column = this;
                        var select = $(
                                '<select class="form-control"><option value=""></option></select>'
                            )
                            .appendTo($(column.footer()).empty())
                            .on('change', function() {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );

                                column
                                    .search(val ? '^' + val + '$' : '', true, false)
                                    .draw();
                            });

                        column.data().unique().sort().each(function(d, j) {
                            select.append('<option value="' + d + '">' + d +
                                '</option>')
                        });
                    });
                }
            });

            // Add Row
            $('#add-row').DataTable({
                "pageLength": 5,
            });

            var action =
                '<td> <div class="form-button-action"> <button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task"> <i class="fa fa-edit"></i> </button> <button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove"> <i class="fa fa-times"></i> </button> </div> </td>';

            $('#addRowButton').click(function() {
                $('#add-row').dataTable().fnAddData([
                    $("#addName").val(),
                    $("#addPosition").val(),
                    $("#addOffice").val(),
                    action
                ]);
                $('#addRowModal').modal('hide');

            });
        });
    </script>
@endsection
