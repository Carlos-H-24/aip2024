@extends('layouts.app')
@section('title', 'Manager | Détail d\'un cours')
@section('content')
    <style>
        .detail * {
            color: white !important;
            font-size: 19px !important
        }

        .titre {
            font-size: 30px !important
        }

        .avatar {
            margin-left: 40%
        }

        .aimg {
            border: gray dotted 3px !important;

        }
    </style>
    <div class="main-panel">
        <div class="content">
            <div class="page-inner">
                <div class="mt-2 mb-4 d-flex justify-content-between">
                    <h4 class="page-title">Détail - Séances</h4>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="card card-post card-round">
                            <div class="card-body detail">
                                <h2 class="mt-5 text-center titre">{{ $cours->nom }}</h2>
                                <div class="separator-solid"></div>
                                <p>Description du cours : {{ $cours->description }}</p>
                                <div>
                                    <div>
                                        <h5 class="mb-4">Séances du cours :</h5>
                                        <table id="basic-datatables" class="display table table-striped table-hover">
                                            <thead>
                                                <tr>
                                                    <th>#ID</th>
                                                    <th>Nom</th>
                                                    <th>Date</th>
                                                    <th>Heure</th>
                                                    <th class="d-flex justify-content-center">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($seances as $seance)
                                                    <tr>
                                                        <td>{{ $seance->id }}</td>
                                                        <td>{{ $seance->nom }}</td>
                                                        <td>
                                                            {{ $seance->date }}
                                                        </td>
                                                        <td>
                                                            {{ $seance->heure_debut }} à
                                                            {{ $seance->heure_fin }}

                                                        </td>
                                                        <td class="d-flex">
                                                            <form
                                                                action="{{ route('manager.seance.delete', [
                                                                    'cours' => $cours,
                                                                    'seance' => $seance,
                                                                ]) }}"
                                                                method="post" id="form">
                                                                @csrf
                                                                @method('delete')
                                                                <button class="btn  mx-3 btn-danger show_confirm_10">
                                                                    <i class="fas fa-trash-alt"></i>
                                                                </button>
                                                            </form>
                                                            <form
                                                                action="{{ route('manager.seance.update', [
                                                                    'cours' => $cours,
                                                                    'seance' => $seance,
                                                                ]) }}"
                                                                method="post" id="form2">
                                                                @csrf
                                                                <button class="btn  mx-3 btn-warning show_confirm_11">
                                                                    <i class="far fa-calendar-check"></i>
                                                                </button>
                                                            </form>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                        @if (count($seances) == 0)
                                        <div class="empty text-danger text-center" style="font-weight: bold !important; color: rgb(214, 2, 2) !important; font-size: 20px !important">
                                            Aucune données trouvées
                                        </div>
                                    @endif
                                    </div>
                                </div>
                                <div class="mt-5  d-flex justify-content-end">

                                    <a href="{{ route('manager.seance.add', ['cours' => $cours]) }}"
                                        class="btn btn-success fw-bold add" style="font-size: 16px !important">
                                        <i class="fas fa-plus-circle"></i>
                                        Ajouter une séance
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $('.show_confirm_10').click(function(event) {
            var form = $(this).closest("form");
            var name = $(this).data("name");
            event.preventDefault();
            swal({
                    title: `Etes-vous sûr de vouloir supprimer cette séance?`,
                    icon: "warning",
                    buttons: {
                        cancel: "Annuler",
                        confirm: "Supprimer"
                    },
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        form.submit();
                    }
                });
        });

        $('.show_confirm_11').click(function(event) {
            var form = $(this).closest("form");
            var name = $(this).data("name");
            event.preventDefault();
            swal({
                    title: `Etes-vous sûr de vouloir marquer terminée cette séance?`,
                    icon: "warning",
                    buttons: {
                        cancel: "Annuler",
                        confirm: "Terminer"
                    },
                })
                .then((willDelete) => {
                    if (willDelete) {
                        form.submit();
                    }
                });
        });
    </script>
@endsection
