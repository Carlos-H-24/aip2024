@extends('layouts.app')
@section('title', 'Manager | Liste des coachs')
@section('content')
    <style>
        .empty {
            text-align: center;
            font-weight: bold;
            font-size: 20px;
        }

        .page-title {
            font-size: 21px !important
        }
    </style>
    <div class="main-panel">

        <div class="content">
            <div class="page-inner">
                <div class="mt-2 mb-4 d-flex justify-content-between">
                    <h4 class="page-title">Liste des coachs</h4>
                </div>
                @if (session('successCreate'))
                    <div id="alertS15"></div>
                @endif
                @if (session('successUpdate'))
                    <div id="alertS16"></div>
                @endif
                @if (session('successDelete'))
                    <div id="alertS17"></div>
                @endif
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="gymnases-table" class="display table table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th>#ID</th>
                                                <th>Nom</th>
                                                <th>Prenom</th>
                                                <th>Email</th>
                                                <th>Cours</th>
                                                <th class="d-flex justify-content-center">Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($coachs as $data)
                                                <tr>
                                                    <td>{{ $data->user->id }}</td>
                                                    <td>{{ $data->user->nom }}</td>
                                                    <td>{{ $data->user->prenom }}</td>
                                                    <td>{{ $data->user->email }}</td>
                                                    <td>
                                                        @if ($data->user->cours->isNotEmpty())
                                                            @foreach ($data->user->cours as $cours)
                                                                {{ $cours->nom }}
                                                                @if (!$loop->last)
                                                                    -
                                                                @endif
                                                            @endforeach
                                                        @else
                                                            Aucun cours
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <div class="d-flex gap-2  justify-content-center">
                                                            <a href="{{ route('manager.coachs.show', $data->user) }}"
                                                                class="btn btn-success mx-2">
                                                                <i class="far fa-eye" style="font-size: 1.3rem"></i></a>
                                                            <a href="{{ route('manager.coachs.edit', $data->user) }}"
                                                                class="btn btn-primary mx-4"><i class="fas fa-edit"
                                                                    style="font-size: 1.3rem"></i></a>
                                                            <form
                                                                action="{{ route('manager.coachs.destroy', $data->user) }}"
                                                                method="post" id="form">
                                                                @csrf
                                                                @method('delete')
                                                                <button class="btn btn-danger show_confirm_6">
                                                                    <i class="fas fa-trash-alt"
                                                                        style="font-size: 1.3rem"></i>
                                                                </button>
                                                            </form>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>

                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    @if ($coachs->total() == 0)
                                        <div class="empty text-danger text-center">
                                            Aucune données trouvées
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-end mx-5">
                            {{ $coachs->links('shared.paginator') }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection
    @section('script')
        <script>
            function showAlertSuccess(alertId) {
                var alertElement = document.getElementById(alertId);
                if (alertElement) {
                    swal("Succès!", "Le coach a été bien crée!", {
                        icon: "success",
                        buttons: {
                            confirm: {
                                className: 'btn btn-success'
                            }
                        },
                    });

                }
            }
            showAlertSuccess('alertS15')

            function showAlertUpdate(alertId) {
                var alertElement = document.getElementById(alertId);
                if (alertElement) {
                    swal("Succès!", "Le coach a modifié bien crée!", {
                        icon: "success",
                        buttons: {
                            confirm: {
                                className: 'btn btn-success'
                            }
                        },
                    });

                }
            }
            showAlertUpdate("alertS16");

            function showAlertDelete(alertId) {
                var alertElement = document.getElementById(alertId);
                if (alertElement) {
                    swal("Succès!", "Le coach a supprimé bien crée!", {
                        icon: "success",
                        buttons: {
                            confirm: {
                                className: 'btn btn-success'
                            }
                        },
                    });

                }
            }
            showAlertDelete("alertS17");
        </script>
        <script type="text/javascript">
            $('.show_confirm_6').click(function(event) {
                var form = $(this).closest("form");
                var name = $(this).data("name");
                event.preventDefault();
                swal({
                        title: `Etes-vous sûr de vouloir supprimer ce coach?`,
                        icon: "warning",
                        buttons: {
                            cancel: "Annuler",
                            confirm: "Supprimer"
                        },
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                        if (willDelete) {
                            form.submit();
                        }
                    });
            });
        </script>
    @endsection
