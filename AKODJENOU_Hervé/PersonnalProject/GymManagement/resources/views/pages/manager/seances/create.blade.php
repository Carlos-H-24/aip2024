@extends('layouts.app')
@section('title','Manager | Ajout d\'une séance ')
@section('content')
    <div class="main-panel">
        <div class="content">
            <div class="page-inner">
                <div class="mt-2 mb-4 d-flex justify-content-between">
                    <h4 class="page-title">Ajouter une nouvelle séance</h4>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-stats card-round">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <form action="{{ route('manager.seance.store',$cours) }}" method="post" class="form" enctype="multipart/form-data">
                                            @csrf
                            
                                            <div class="row">
                                                <div class="form-group col-md-3 @error('nom') has-error has-feedback @enderror mt-3">
                                                    <label for="nom" class="form-label">Nom </label>
                                                    <input type="text" class="form-control" id="nom" name="nom"
                                                    value="{{ old('nom') }}">
                                                    @error('nom')
                                                        <span class="form-text text-muted text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                                <div class="form-group col-md-3 @error('date') has-error has-feedback @enderror mt-3">
                                                    <label for="date" class="form-label">Date </label>
                                                    <input type="date" class="form-control" id="date" name="date"
                                                    value="{{ old('date') }}">
                                                    @error('date')
                                                        <span class="form-text text-muted text-danger">{{ $message }}</span>
                                                    @enderror           
                                                </div>
                                                <div class="form-group col-md-3 @error('heure_debut') has-error has-feedback @enderror mt-3">
                                                    <label for="heure_debut" class="form-label">Heure de début </label>
                                                    <input type="time" class="form-control" id="heure_debut" name="heure_debut"
                                                    value="{{ old('heure_debut') }}">
                                                    @error('heure_debut')
                                                        <span class="form-text text-muted text-danger">{{ $message }}</span>
                                                    @enderror           
                                                </div>
                                                <div class="form-group col-md-3 @error('heure_fin') has-error has-feedback @enderror mt-3">
                                                    <label for="heure_fin" class="form-label">Heure de fin </label>
                                                    <input type="time" class="form-control" id="heure_fin" name="heure_fin"
                                                    value="{{ old('heure_fin') }}">
                                                    @error('heure_fin')
                                                        <span class="form-text text-muted text-danger">{{ $message }}</span>
                                                    @enderror           
                                                </div>
                                            </div>
                                            <button type="submit" class="btn btn-success mt-4">Ajouter</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            $('#basic-datatables').DataTable({});

            $('#multi-filter-select').DataTable({
                "pageLength": 5,
                initComplete: function() {
                    this.api().columns().every(function() {
                        var column = this;
                        var select = $(
                                '<select class="form-control"><option value=""></option></select>'
                            )
                            .appendTo($(column.footer()).empty())
                            .on('change', function() {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );

                                column
                                    .search(val ? '^' + val + '$' : '', true, false)
                                    .draw();
                            });

                        column.data().unique().sort().each(function(d, j) {
                            select.append('<option value="' + d + '">' + d +
                                '</option>')
                        });
                    });
                }
            });

            // Add Row
            $('#add-row').DataTable({
                "pageLength": 5,
            });

            var action =
                '<td> <div class="form-button-action"> <button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task"> <i class="fa fa-edit"></i> </button> <button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove"> <i class="fa fa-times"></i> </button> </div> </td>';

            $('#addRowButton').click(function() {
                $('#add-row').dataTable().fnAddData([
                    $("#addName").val(),
                    $("#addPosition").val(),
                    $("#addOffice").val(),
                    action
                ]);
                $('#addRowModal').modal('hide');

            });
        });
    </script>
@endsection
