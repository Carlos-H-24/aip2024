@extends('layouts.app')
@section('title', 'Tableau de bord')
@section('content')
    <div class="main-panel">
        <div class="content">
            <div class="page-inner">
                <div class="mt-2 mb-4">
                    <h2 class="text-white pb-2">Content de vous, {{ Auth::user()->prenom }}</h2>
                    <h5 class="text-white op-7 mb-4">Hier, j'étais intelligent, alors je voulais changer le monde.
                        Aujourd'hui je suis
                        sage, alors je me change.</h5>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="card card-dark bg-primary-gradient">
                            <div class="card-body pb-0">
                                <h2 class="mb-2" style="font-size: 50px !important">{{ $abonnements }}</h2>
                                <p>Nombre d'abonnement</p>
                                <div class="pull-in sparkline-fix chart-as-background">
                                    <div id="lineChart"><canvas width="327" height="70"
                                            style="display: inline-block; width: 327px; height: 70px; vertical-align: top;"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card card-dark bg-secondary-gradient">
                            <div class="card-body pb-0">
                                <h2 class="mb-2" style="font-size: 50px !important">{{ $nombreGymnases }}</h2>
                                <p>Nombre de gymnase survi</p>
                                <div class="pull-in sparkline-fix chart-as-background">
                                    <div id="lineChart2"><canvas width="327" height="70"
                                            style="display: inline-block; width: 327px; height: 70px; vertical-align: top;"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card card-dark bg-success2">
                            <div class="card-body pb-0">
                                <h2 class="mb-2" style="font-size: 50px !important">{{ $montantTotalAbonnements }}
                                    <em>FCFA</em></h2>
                                <p>Montant total des abonnements</p>
                                <div class="pull-in sparkline-fix chart-as-background">
                                    <div id="lineChart3"><canvas width="327" height="100"
                                            style="display: inline-block; width: 327px; height: 70px; vertical-align: top;"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-5">
                    <h2 class="text-center"
                        style="font-size: 25px !important; color: white !important; font-weight: bold !important">Liste des
                        abonnements par gymnase</h2>
                </div>
                <div class="row mt-5">
                    <div class="col-12">
                        <table class="table table-striped text-white">
                            <thead>
                                <tr>
                                    <th>Gymnase</th>
                                    <th>Type abonnement</th>
                                    <th>Nombre de mois</th>
                                    <th>Montant</th>
                                    <th>Date de début</th>
                                    <th>Date de fin</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($abonnementsParGymnase as $abonnement)
                                    <tr>
                                        <td>{{ $abonnement['gymnase']->nom }}</td>
                                        <td>{{ $abonnement['type_abonnement'] }}</td>
                                        <td>{{ $abonnement['mois'] }}</td>
                                        <td>{{ $abonnement['montant'] }} <em>FCFA</em></td>
                                        <td>{{ $abonnement['date_debut'] }}</td>
                                        <td>{{ $abonnement['date_fin'] }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
