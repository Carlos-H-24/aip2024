<style>
    .form-label {
        font-size: 18px !important
    }

    .text-muted {
        font-size: 16px !important;
    }

    .text-danger {
        color: rgb(220, 53, 69) !important;
    }

    input {
        color: black !important;
    }
</style>

<div class="modal fade" id="exampleModal3" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content  bg-dark">
            <div class="modal-header">
                <h5 class="modal-title text-white fw-bold" style="font-style: normal !important" id="exampleModalLabel">
                    Veuillez saisir le nombre de mois</h5>
                <button type="button" class="btn-close text-white" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body" style="font-style: normal !important;">
                <form action="{{ route('abonnement.other',['gymnase' => $gym, 'typeAbn' =>  $typeAbn]) }}" method="post" class="form"
                    enctype="multipart/form-data" style="font-size: 18px !important">
                    @csrf
                    <div class="row">
                        <div class="form-group col-12 @error('mois') has-error has-feedback @enderror mt-3">
                            <input type="number" min="1" max="12" class="form-control" id="mois"
                                name="mois" value="{{ old('mois') }}">
                            @error('mois')
                                <span class="form-text text-muted text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="modal-footer" style="font-style: normal !important">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Fermer</button>
                        <button type="submit" class="btn btn-primary">Enrégistrer</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
