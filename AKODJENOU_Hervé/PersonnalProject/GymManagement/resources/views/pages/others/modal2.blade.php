<style>
    .form-label {
        font-size: 18px !important
    }

    .text-muted {
        font-size: 16px !important;
    }

    .text-danger {
        color: rgb(220, 53, 69) !important;
    }

    input {
        color: black !important;
    }
</style>

<div class="modal fade" id="exampleModal2" tabindex="-1" aria-labelledby="exampleModal2Label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content  bg-dark">
            <div class="modal-header">
                <h5 class="modal-title text-white fw-bold" style="font-style: normal !important" id="exampleModal2Label">
                    Veuillez remplir ce formulaire</h5>
                <button type="button" class="btn-close text-white" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body" style="font-style: normal !important;">
                <form action="{{ route('abonnement.store',['gymnase' => $gym, 'typeAbn' =>  $typeAbn]) }}" method="post" class="form"
                    enctype="multipart/form-data" style="font-size: 18px !important">
                    @csrf
                    <div class="row">
                        <div class="form-group col-md-6 @error('nom') has-error has-feedback @enderror mt-3">
                            <label for="nom" class="form-label">Nom </label>
                            <input type="text" class="form-control" id="nom" name="nom"
                                value="{{ old('nom') }}">
                            @error('nom')
                                <span class="form-text text-muted text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6 @error('prenom') has-error has-feedback @enderror mt-3">
                            <label for="prenom" class="form-label">Prénom </label>
                            <input type="text" class="form-control" id="prenom" name="prenom"
                                value="{{ old('prenom') }}">
                            @error('prenom')
                                <span class="form-text text-muted text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-groupl col-md-6 @error('email') has-error has-feedback @enderror mt-3">
                            <label for="email" class="form-label">Email </label>
                            <input type="text" class="form-control" id="email" name="email"
                                value="{{ old('email') }}">
                            @error('email')
                                <span class="form-text text-muted text-danger">{{ $message }}</span>
                            @enderror
                        </div>

                        <div class="form-group col-md-6 @error('adresse') has-error has-feedback @enderror mt-3">
                            <label for="adresse" class="form-label">Adresse </label>
                            <input type="text" class="form-control" id="adresse" name="adresse"
                                value="{{ old('adresse') }}">
                            @error('adresse')
                                <span class="form-text text-muted text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6 @error('telephone') has-error has-feedback @enderror mt-3">
                            <label for="telephone" class="form-label">Télephone </label>
                            <input type="tel" class="form-control" id="telephone" name="telephone"
                                value="{{ old('telephone') }}">
                            @error('telephone')
                                <span class="form-text text-muted text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6 mt-3">

                            <label for="sexe" class="form-label">Sexe</label>
                            <select class="form-control sexe" id="sexe" name="sexe">
                                <option value="Masculin">Masculin</option>
                                <option value="Féminin">Féminin</option>
                                <option value="Autre">Autre</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6 @error('date_naissance') has-error has-feedback @enderror mt-3">
                            <label for="date_naissance" class="form-label">Date de naissance </label>
                            <input type="date" class="form-control" id="date_naissance" name="date_naissance"
                                value="{{ old('date_naissance') }}">
                            @error('date_naissance')
                                <span class="form-text text-muted text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6 @error('mois') has-error has-feedback @enderror mt-3">
                            <label for="mois" class="form-label">Nombre de mois </label>
                            <input type="number" min="1" max="12" class="form-control" id="mois"
                                name="mois" value="{{ old('mois') }}">
                            @error('mois')
                                <span class="form-text text-muted text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="modal-footer" style="font-style: normal !important">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Fermer</button>
                        <button type="submit" class="btn btn-primary">Enrégistrer</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
