<!DOCTYPE html>
<html>

<head>
    <title>FITNESS CENTER</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="format-detection" content="telephone=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="author" content="">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <link rel="stylesheet" type="text/css" href="/assets2/bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="/assets2/style.css">
    <link rel="stylesheet" type="text/css" href="/assets2/fonts/icomoon/icomoon.css">
    <link rel="stylesheet" type="text/css" href="/assets2/slick/slick/slick.css" />
    <link rel="stylesheet" type="text/css" href="/assets2/slick/slick/slick-theme.css" />
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@700&family=Roboto:wght@400;700&display=swap"
        rel="stylesheet">
</head>
<style>
    .text-white {
        color: white !important;

    }

    .text-center {
        text-align: center !important
    }

    .detail {
        color: white !important;

    }

    .label {
        font-weight: bold;
        font-size: 20px
    }

    .services {
        font-size: 18px !important;
    }
</style>

<body class="overflow-x bg-dark">
    <div id="body-wrapper">
        <header id="header" class="bg-black">
            <div class="container d-flex justify-content-start">
                <div class="row">
                    <nav class="navbar navbar-expand-lg col-md-12 padding-small">

                        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse"
                            data-target="#slide-navbar-collapse" aria-controls="slide-navbar-collapse"
                            aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"><i class="icon icon-navicon"></i></span>
                        </button>

                        <div class="navbar-collapse collapse text-hover" id="slide-navbar-collapse">
                            <ul class="navbar-nav list-inline light text-uppercase">
                                <li class="nav-item active"><a href="{{ route('home') }}" class="navlink">Accueil</a>
                                </li>
                                <li class="nav-item"><a href="{{ route('home') }}#about">A propos</a></li>
                                <li class="nav-item"><a href="{{ route('home') }}#salles">Salles</a></li>
                                @guest
                                    <li class="nav-item"><a href="{{ route('login') }}">Connexion</a></li>
                                @endguest
                                @auth
                                    <li class="nav-item"><a href="{{ route('dashboard') }}">Tableau de bord</a></li>
                                @endauth
                            </ul>
                        </div><!--navbar-collapse-->

                    </nav>
                </div>
            </div>
        </header>

        <section class="services" id="salles">
            <div class="row mt-5">
                <div class="workout-box front-dark col-md-6 p-0">
                    <img src="/assets2/images/bodybuilding.jpg" alt="musculation" class="fitnessImg">
                    <div class="text-content light p-4 w-100">
                        <h3 class="colored">{{ $gym->nom }}</h3>
                        <p>{{ $gym->description }}</p>
                    </div><!--text-content-->
                </div>
                <div class=" bg-secondary col-md-6 p-0">
                    <h2 class="text-white text-center mt-3">Informations sur le gymnase</h2>
                    <div class="detail m-5">
                        <div class="row">
                            <div class="col">
                                <p><span class="label">EMAIL : </span><span>{{ $gym->email }}</span></p>
                            </div>
                            <div class="col">
                                <p><span class="label">ADRESSE : </span><span>{{ $gym->adresse }}</span></p>
                            </div>
                        </div>
                        <div class="row pt-4">
                            <div class="col">
                                <p><span class="label">TELEPHONE : </span><span>{{ $gym->telephone }}</span></p>
                            </div>
                            <div class="col">
                                <p><span class="label">DATE DE CREATION :
                                    </span><span>{{ \Carbon\Carbon::parse($gym->created_at)->locale('fr_FR')->isoFormat('DD MMMM, YYYY h:mm:ss A') }}
                                    </span></p>
                            </div>
                        </div>
                        <hr class="bg-white">
                        <div class="row pt-4">
                            <div class="col">
                                <p><span class="label">GESTIONNAIRE : </span><span>{{ $gest->nom }}
                                        {{ $gest->prenom }}</span></p>
                            </div>
                            <div class="col">
                                <p><span class="label">TELEPHONE : </span><span>{{ $gest->telephone }}
                                    </span></p>
                            </div>
                        </div>
                        <div class="row pt-4">
                            <div class="col">
                                <p><span class="label">NOMBRE DE COACHS : </span><span>{{ $nbreCoach }}</span></p>
                            </div>
                            <div class="col">
                                <p><span class="label">NOMBRE DE COURS : </span><span>{{ $nbreCours }}
                                    </span></p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>

        <section class="membership padding-medium light front-dark mt-5">
            <div class="membership-wrapper padding-medium">
                <h2 class="section-title text-center mb-4">Abonnement</h2>
                <div class="container">
                    <div class="row">
                        <div class="flex-container">
                            <div class="membership-box col-md-6 mb-3">
                                <div class="heading-box">
                                    <h3>{{ $typeAbonnements[1]->nom }}</h3>
                                </div>
                                <div class="text-content">
                                    <div class="inclusion">
                                        <ul class="list-unstyled colored-alto">
                                            <li><i class="icon icon-check"></i>Abonnement inclus</li>
                                            <li><i class="icon icon-check"></i>Accès à toutes les installations de la
                                                salle de gym</li>
                                            <li><i class="icon icon-close"></i>Plan diététique inclus</li>
                                            <li><i class="icon icon-check"></i>Conseils santé et fitness</li>
                                            <li><i class="icon icon-close"></i>Accès à la salle de gym du lundi au
                                                vendredi</li>
                                            <li><i class="icon icon-check"></i>Accès complet à tout</li>
                                            <li><i class="icon icon-close"></i>Aucun équipement supplémentaire</li>
                                        </ul>
                                    </div><!--inclusion-->
                                    <div class="meta-price">
                                        <p><strong>{{ $typeAbonnements[1]->tarif }}</strong><em>FCFA/mois</sup></p>
                                    </div>

                                    <div class="pix_btn">
                                        @guest
                                            <button type="button" data-toggle="modal" data-target="#exampleModal"
                                                class="hvr-shutter-in-horizontal mt-3">S'abonner</button>
                                        @endguest
                                        @auth
                                            <button type="button" data-toggle="modal" data-target="#exampleModal3"
                                                class="hvr-shutter-in-horizontal mt-3">S'abonner</button>
                                        @endauth
                                    </div>
                                </div><!--text-content-->
                            </div><!--membership-box-->
                            <div class="membership-box mb-3">
                                <div class="heading-box">
                                    <h3>{{ $typeAbonnements[0]->nom }}</h3>
                                </div>
                                <div class="text-content">
                                    <div class="inclusion">
                                        <ul class="list-unstyled colored-alto">
                                            <li><i class="icon icon-check"></i>Abonnement inclus</li>
                                            <li><i class="icon icon-check"></i>Accès à toutes les installations de la
                                                salle de gym</li>
                                            <li><i class="icon icon-check"></i>Plan diététique inclus</li>
                                            <li><i class="icon icon-check"></i>Conseils santé et fitness</li>
                                            <li><i class="icon icon-check"></i>Accès à la salle de gym du lundi au
                                                vendredi</li>
                                            <li><i class="icon icon-check"></i>Accès complet à tout</li>
                                            <li><i class="icon icon-check"></i>Aucun équipement supplémentaire</li>
                                        </ul>
                                    </div><!--inclusion-->
                                    <div class="meta-price">
                                        <p><strong>{{ $typeAbonnements[0]->tarif }}</strong><em>FCFA/mois</sup></p>
                                    </div>
                                    <div class="pix_btn">
                                        @guest
                                            <button type="button" data-toggle="modal" data-target="#exampleModal2"
                                                class="hvr-shutter-in-horizontal mt-3">S'abonner</button>
                                        @endguest
                                        @auth
                                            <button type="button" data-toggle="modal" data-target="#exampleModal4"
                                                class="hvr-shutter-in-horizontal mt-3">S'abonner</button>
                                        @endauth
                                    </div>
                                </div><!--text-content-->
                            </div><!--membership-box-->


                        </div><!--col-md-12-->
                    </div>
                </div>
            </div><!--membership-wrapper-->
        </section>



        <div class="footer-bottom bg-black padding-small" style="margin-top: 4.4%">
            <div class="container">
                <div class="row">
                    <div class="wrap flex-container">
                        <div class="copyright">
                            <p class="mt-4 mx-5">© Copyright 2024 </p>
                        </div>
                        <div class="social-links mt-3">
                            <ul class="list-unstyled d-flex">
                                <li class="mr-3"><a href="#"><i class="icon icon-facebook m-1"></i></a></li>
                                <li class="mr-3"><a href="#"><i class="icon icon-twitter m-1"></i></a></li>
                                <li><a href="#"><i class="icon icon-youtube m-1"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div><!--body-wrapper-->
    @include('pages.others.modal1', ['gymnase' => $gym, 'typeAbn' => $typeAbonnements[1]])
    @include('pages.others.modal2', ['gymnase' => $gym, 'typeAbn' => $typeAbonnements[0]])
    @include('pages.others.modal3', ['gymnase' => $gym, 'typeAbn' => $typeAbonnements[1]])
    @include('pages.others.modal4', ['gymnase' => $gym, 'typeAbn' => $typeAbonnements[0]])
    <script src="/assets2/js/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="/assets2/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript" src="/assets2/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/assets2/js/hc-sticky.js"></script>

    <script type="text/javascript" src="/assets2/js/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="/assets2/slick/slick/slick.min.js"></script>
    <script type="text/javascript" src="/assets2/js/script.js"></script>
    <script src="https://code.iconify.design/iconify-icon/1.0.7/iconify-icon.min.js"></script>

</body>

</html>
