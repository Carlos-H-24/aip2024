<x-mail::message>
<style>
.btn-primary {
display: inline-block;
font-weight: 700;
text-align: center;
vertical-align: middle;
cursor: pointer;
border: 1px solid transparent;
padding: 10px 20px;
font-size: 16px;
line-height: 1.5;
border-radius: 4px;
color: #fff;
background-color: #007bff;
border-color: #007bff;
text-decoration: none;
}

.text-center{
text-align: center
}

.btn-primary:hover {
background-color: #0056b3;
border-color: #0056b3;
}
</style>
<h2 class="center">Bienvenue chez nous !</h2>

Bonjour @if($user->sexe == "Masculin")M. @else Mme. @endif {{$user->nom}} {{$user->prenom}},

Félicitations pour votre nouvel abonnement ! Nous sommes ravis de vous accueillir dans notre communauté de fitness. 


<b>Détails de votre abonnement :</b>
<x-mail::panel>
  Gymnase : {{$gym->nom}} <br>
  Type d'abonnement : {{$typeAbonnement->nom}} <br>
  Durée : {{$abonnement->mois}} mois <br>
  Début : {{$abonnement->date_debut}} <br>
  Fin : {{$abonnement->date_fin}} <br>
</x-mail::panel>


{{-- <div class="text-center">
  <a href="" class="btn-primary">Définir mon mot de passe</a>
</div><br> --}}


Si vous rencontrez des difficultés ou avez des questions, n'hésitez pas à nous contacter à <a href="mailto:gymcenter.plateform@gmail.com">gymcenter.plateform@gmail.com</a>


Merci et bonne séance d'entraînement !<br>

L'équipe de GymCenter
</x-mail::message>
