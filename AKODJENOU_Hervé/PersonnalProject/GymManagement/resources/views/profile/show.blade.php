@extends('layouts.app')
@section('title', 'Profil')
@section('content')
    <style>
        .empty {
            text-align: center;
            font-weight: bold;
            font-size: 20px;
        }

        .page-title{
            font-size: 21px !important
        }

        .avatar{
            margin-left: 50%
        }

        .aimg{
            border: gray dotted 3px !important;

        }
        .infoS * {
            background-color: rgb(32,41,64) !important;
            color : white !important
        }
    </style>
    <div class="main-panel">
        
        <div class="content">
            <div class="page-inner">
                <div class="mt-2 mb-4 d-flex justify-content-between">
                    <h4 class="page-title">Profil</h4>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="avatar mb-5 avatar-xxl">
                                    <img src="/assets/img/icon2.png" alt="..." class="aimg avatar-img rounded-circle">
                                </div>
                                <div>
                                    <div class="infoS">

                                        @if (Laravel\Fortify\Features::canUpdateProfileInformation())
                                            @livewire('profile.update-profile-information-form')
                            
                                            <x-section-border />
                                        @endif
                            
                                        @if (Laravel\Fortify\Features::enabled(Laravel\Fortify\Features::updatePasswords()))
                                            <div class="mt-10 sm:mt-0">
                                                @livewire('profile.update-password-form')
                                            </div>
                            
                                            <x-section-border />
                                        @endif
                            
                                        @if (Laravel\Fortify\Features::canManageTwoFactorAuthentication())
                                            <div class="mt-10 sm:mt-0">
                                                @livewire('profile.two-factor-authentication-form')
                                            </div>
                            
                                            <x-section-border />
                                        @endif
                            
                                        <div class="mt-10 sm:mt-0">
                                            @livewire('profile.logout-other-browser-sessions-form')
                                        </div>
                            
                                        @if (Laravel\Jetstream\Jetstream::hasAccountDeletionFeatures())
                                            <x-section-border />
                            
                                            <div class="mt-10 sm:mt-0">
                                                @livewire('profile.delete-user-form')
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection
