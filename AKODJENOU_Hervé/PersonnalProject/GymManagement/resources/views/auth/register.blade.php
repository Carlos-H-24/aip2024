<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Inscription</title>
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
    <link rel="icon" href="/assets/img/icon.ico" type="image/x-icon" />
    <script src="/assets/js/plugin/webfont/webfont.min.js"></script>
    <script>
        WebFont.load({
            google: {
                "families": ["Lato:300,400,700,900"]
            },
            custom: {
                "families": ["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands",
                    "simple-line-icons"
                ],
                urls: ['/assets/css/fonts.min.css']
            },
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/atlantis.min.css">
    <link rel="stylesheet" href="/assets/css/demo.css">
</head>

<style>
    .full-screen-image {
        width: 100%;
        height: 100vh;
        object-fit: cover;
    }

    .login-container {
        position: absolute;
        top: 55%;
        left: 50%;
        transform: translate(-50%, -50%);
        text-align: center;
    }

    .form-label {
        display: flex;
        color: white !important;
        font-size: 17px !important
    }

    h1 {
        font-size: 30px
    }

    input {
        font-size: 15px !important
    }

    .remenber {
        font-size: 18px !important;
        color: white !important
    }

    .forgot {
        font-size: 18px !important;
    }

    button {
        font-size: 17px !important;
    }

    .signindiv {
        display: flex;
        justify-content: center
    }

    .signindiv p {
        font-size: 17px !important;
        color: white !important;
        margin-top: 6px
    }

    .signindiv a {
        font-size: 17px !important;
    }

    span {
        text-align: start !important;
        font-size: 15px;
    }

    .alert {
        font-size: 16px
    }
</style>

<body data-background-color="dark">
    <img src="/assets/img/peakpx1.jpg" alt="" class="full-screen-image">
    <div class="container login-container">
        <div class="col-12">
            <div class="card card-stats card-round">
                <div class="card-body">
                    <div class="avatar avatar-xxl">
                        <img src="/assets/img/icon2.png" alt="..." class="avatar-img rounded-circle">
                    </div>
                    <div class="row mt-4 d-flex justify-content-center">
                        <h1 class="text-white">Inscription</h1>
                    </div>
                    @if (session('error'))
                        <div class="alert mt-3 text-white alert-danger bg-danger" id="alertLogin">
                            {{ session('error') }}
                        </div>
                    @endif
                    <div class="row">
                        <div class="col mx-5">
                            <form action="{{ route('register') }}" method="post">
                                @csrf
                                <div class="row">
                                    <div
                                        class="form-group col-md-4 @error('nom') has-error has-feedback @enderror mt-3">
                                        <label for="nom" class="form-label">Nom </label>
                                        <input type="text" class="form-control" id="nom" name="nom"
                                            value="{{ old('nom') }}">
                                        @error('nom')
                                            <span class="form-text text-muted text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div
                                        class="form-group col-md-4 @error('prenom') has-error has-feedback @enderror mt-3">
                                        <label for="prenom" class="form-label">Prénom </label>
                                        <input type="text" class="form-control" id="prenom" name="prenom"
                                            value="{{ old('prenom') }}">
                                        @error('prenom')
                                            <span class="form-text text-muted text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div
                                        class="form-group col-md-4 @error('email') has-error has-feedback @enderror mt-3">
                                        <label for="email" class="form-label">Email </label>
                                        <input type="text" class="form-control" id="email" name="email"
                                            placeholder="ex:johnedoe@gmail.fr" value="{{ old('email') }}">
                                        @error('email')
                                            <span class="form-text text-muted text-danger">{{ $message }}</span>
                                        @enderror

                                    </div>
                                </div>
                                <div class="row">
                                    <div
                                        class="form-group col-md-4 @error('password') has-error has-feedback @enderror  mt-3">
                                        <label for="password" class="form-label">Mot de passe</label>
                                        <input type="password" class="form-control" id="password" name="password">
                                        @error('password')
                                            <span class="form-text text-muted text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div
                                        class="form-group col-4 @error('password_confirmation') has-error has-feedback @enderror  mt-3">
                                        <label for="confirm_password" class="form-label">Confirmation de mot de
                                            passe</label>
                                        <input type="password" class="form-control" id="confirm_password"
                                            name="password_confirmation">
                                        @error('password_confirmation')
                                            <span class="form-text text-muted text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col @error('adresse') has-error has-feedback @enderror mt-3">
                                        <label for="adresse" class="form-label">Adresse </label>
                                        <input type="text" class="form-control" id="adresse" name="adresse"
                                            value="{{ old('adresse') }}">
                                        @error('adresse')
                                            <span class="form-text text-muted text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row">
                                    <div
                                        class="form-group col-md-3 @error('telephone') has-error has-feedback @enderror mt-3">
                                        <label for="telephone" class="form-label">Télephone </label>
                                        <input type="tel" class="form-control" id="telephone" name="telephone"
                                            value="{{ old('telephone') }}">
                                        @error('telephone')
                                            <span class="form-text text-muted text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-3 mt-3">

                                        <label for="sexe" class="form-label">Sexe</label>
                                        <select class="form-control" id="sexe" name="sexe">
                                            <option value="Masculin">Masculin</option>
                                            <option value="Féminin">Féminin</option>
                                            <option value="Autre">Autre</option>
                                        </select>
                                    </div>
                                    <div
                                        class="form-group col-md-3 @error('date_naissance') has-error has-feedback @enderror mt-3">
                                        <label for="date_naissance" class="form-label">Date de naissance </label>
                                        <input type="date" class="form-control" id="date_naissance"
                                            name="date_naissance" value="{{ old('date_naissance') }}">
                                        @error('date_naissance')
                                            <span class="form-text text-muted text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col mt-md-3">
                                        <label for="photo" class="form-label">Photo</label>
                                        <input type="file" class="form-control-file" id="photo"
                                            name="photo">
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-primary  mt-4">S'incrire</button>
                            </form>
                            <div class="mt-4 signindiv">
                                <p>Vous avez déjà un compte ?</p>
                                <a href="{{ route('login') }}" class="nav-link">Se connecter</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--   Core JS Files   -->
    <script src="/assets/js/core/jquery.3.2.1.min.js"></script>
    <script src="/assets/js/core/popper.min.js"></script>
    <script src="/assets/js/core/bootstrap.min.js"></script>

    <!-- jQuery UI -->
    <script src="/assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
    <script src="/assets/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>

    <!-- jQuery Scrollbar -->
    <script src="/assets/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js"></script>


    <!-- Chart JS -->
    <script src="/assets/js/plugin/chart.js/chart.min.js"></script>

    <!-- jQuery Sparkline -->
    <script src="/assets/js/plugin/jquery.sparkline/jquery.sparkline.min.js"></script>

    <!-- Chart Circle -->
    <script src="/assets/js/plugin/chart-circle/circles.min.js"></script>

    <!-- Datatables -->
    <script src="/assets/js/plugin/datatables/datatables.min.js"></script>

    <!-- Bootstrap Notify -->
    <script src="/assets/js/plugin/bootstrap-notify/bootstrap-notify.min.js"></script>

    <!-- jQuery Vector Maps -->
    <script src="/assets/js/plugin/jqvmap/jquery.vmap.min.js"></script>
    <script src="/assets/js/plugin/jqvmap/maps/jquery.vmap.world.js"></script>

    <!-- Sweet Alert -->
    <script src="/assets/js/plugin/sweetalert/sweetalert.min.js"></script>

    <!-- Atlantis JS -->
    <script src="/assets/js/atlantis.min.js"></script>

    <!-- Atlantis DEMO methods, don't include it in your project! -->
    <script src="/assets/js/setting-demo.js"></script>
</body>
<script>
    function closeAlert(alertId) {
        var alertElement = document.getElementById(alertId);
        if (alertElement) {
            alertElement.style.display = 'none';
        }
    }
    setTimeout(function() {
        closeAlert('alertLogin');
    }, 3000);
</script>

</html>
