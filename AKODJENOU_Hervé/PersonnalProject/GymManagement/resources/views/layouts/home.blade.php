<!DOCTYPE html>
<html>

<head>
    <title>FITNESS CENTER</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="format-detection" content="telephone=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="author" content="">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <link rel="stylesheet" type="text/css" href="/assets2/bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="/assets2/style.css">
    <link rel="stylesheet" type="text/css" href="/assets2/fonts/icomoon/icomoon.css">
    <link rel="stylesheet" type="text/css" href="/assets2/slick/slick/slick.css" />
    <link rel="stylesheet" type="text/css" href="/assets2/slick/slick/slick-theme.css" />
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@700&family=Roboto:wght@400;700&display=swap"
        rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"
        integrity="sha512-AA1Bzp5Q0K1KanKKmvN/4d3IRKVlv9PYgwFPvm32nPO6QS8yH1HO7LbgB1pgiOxPtfeg5zEn2ba64MUcqJx6CA=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
</head>

<body class="overflow-x">
    <div id="body-wrapper">
        <header id="header" class="bg-black">
            <div class="container d-flex justify-content-start">
                <div class="row">
                    <nav class="navbar navbar-expand-lg col-md-12 padding-small">

                        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse"
                            data-target="#slide-navbar-collapse" aria-controls="slide-navbar-collapse"
                            aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"><i class="icon icon-navicon"></i></span>
                        </button>

                        <div class="navbar-collapse collapse text-hover" id="slide-navbar-collapse">
                            <ul class="navbar-nav list-inline light text-uppercase">
                                <li class="nav-item active"><a href="{{ route('home') }}" class="navlink">Accueil</a>
                                </li>
                                <li class="nav-item"><a href="#about">A propos</a></li>
                                <li class="nav-item"><a href="#salles">Salles</a></li>
                                @guest
                                    <li class="nav-item"><a href="{{ route('login') }}">Connexion</a></li>
                                @endguest
                                @auth
                                    <li class="nav-item"><a href="{{ route('dashboard') }}">Tableau de bord</a></li>
                                @endauth
                            </ul>
                        </div><!--navbar-collapse-->

                    </nav>
                </div>
            </div>
        </header>
        @if (session('successCreate'))
        <div id="alertS20"></div>
        @endif
        <section id="billboard">
            <div class="button-container">
                <button class="prev slick-arrow"><i class="icon mb-2 mr-1 icon-angle-left"></i></button>
                <button class="next slick-arrow"><i class="icon mb-2 mr-1 icon-angle-right"></i></button>
            </div>
            <div class="slider">
                <div class="slider-item">
                    <div class="container">
                        <div class="row">
                            <div class="text-content light offset-md-5">
                                <h2><strong class="colored">Le Travail Acharné</strong> est la Clé de Tout Succès</h2>
                                <p><em>Commencez par puiser de l'inspiration, continuez à en donner.</em></p>

                            </div><!--text-content-->
                        </div>
                    </div>
                </div><!--slider-item-->

                <div class="slider-item">
                    <div class="container">
                        <div class="row">
                            <div class="text-content light offset-md-5">
                                <h2><strong class="colored">Brisez les Barrières,</strong> Surmontez les Défis</h2>
                                <p><em>L'avenir est forgé par ceux qui persévèrent avec passion.</em></p>
                            </div><!--text-content-->
                        </div>
                    </div>
                </div><!--slider-item-->
            </div>
        </section><!----#billboard----->

        <section class="about-us padding-medium bg-sand" id="about">
            <div class="container">
                <div class="row">
                    <div class="section-header text-center">
                        <h2 class="section-title pb-4">À Propos de Nous</h2>
                        <p>Découvrez une communauté dynamique où chaque salle de gym est une porte ouverte vers une vie
                            plus saine. Notre passion est de faire du fitness une expérience inoubliable.</p>
                    </div>
                    <div class="iconbox-wrapper d-flex pt-4 p-sm-3">
                        <div class="iconbox col-md-3 text-center bg-white p-40 mb-3">
                            <div class="iconbox-icon mb-3">
                                <i class="icon icon-group"></i>
                            </div>
                            <div class="iconbox-content">
                                <h3 class="colored">Coaches d'Exception</h3>
                                <p>Rencontrez nos coaches passionnés, prêts à vous accompagner dans chaque salle de gym
                                    avec expertise et motivation.</p>
                            </div>
                        </div><!--iconbox-->

                        <div class="iconbox col-md-3 text-center bg-white p-40 mrl-3 mb-3">
                            <div class="iconbox-icon mb-3">
                                <i class="icon icon-price-tags"></i>
                            </div>
                            <div class="iconbox-content">
                                <h3 class="colored">Tarifs Accessibles</h3>
                                <p>Nous croyons au fitness pour tous. Profitez de tarifs abordables dans chacune de nos
                                    salles de gym pour rendre le bien-être accessible à tous.</p>
                            </div>
                        </div><!--iconbox-->

                        <div class="iconbox col-md-3 text-center bg-white p-40 mb-3">
                            <div class="iconbox-icon mb-3">
                                <i class="icon icon-dumble"></i>
                            </div>
                            <div class="iconbox-content">
                                <h3 class="colored">Équipements de Pointe</h3>
                                <p>Transformez votre routine avec nos équipements modernes présents dans chaque salle de
                                    gym. Vivez une expérience fitness innovante.</p>
                            </div>
                        </div>
                        <div class="iconbox mx-3 col-md-3 text-center bg-white p-40 mb-3">
                            <div class="iconbox-icon mb-3">
                                <i class="icon icon-dumble"></i>
                            </div>
                            <div class="iconbox-content">
                                <h3 class="colored">Nos Salles de Gym</h3>
                                <p>Découvrez nos salles uniques conçues pour inspirer et défier. Des environnements
                                    adaptés à tous les niveaux d'entraînement, chaque salle ayant son propre caractère
                                    distinctif.</p>
                            </div>
                        </div><!--iconbox-->
                    </div><!--iconbox-wrapper-->
                </div>

            </div>
        </section>

        <h2 class="section-title text-center mb-4">Nos gymnases</h2>

        <section class="services" id="salles">
            @foreach ($gymnases as $gym)
                <div class="workout-box front-dark col-md-6 p-0">
                    <img src="/assets2/images/bodybuilding.jpg" alt="musculation" class="fitnessImg">
                    <div class="text-content light">
                        <h3 class="colored">{{ $gym->nom }}</h3>
                        <p>{{ strlen($gym->description) > 200 ? substr($gym->description, 0, 200) . '...' : $gym->description }}
                        </p>
                        <div class="pix_btn">
                            <a href="{{ route('salle.show', $gym) }}" class="btn btn-effects">
                                <svg>
                                    <rect x="0" y="0" fill="none" width="100%" height="100%"></rect>
                                </svg>
                                En savoir plus
                                <i class="icon icon-arrow-thin-right hvr-icon"></i>
                            </a>
                        </div>
                    </div><!--text-content-->
                </div>
            @endforeach
        </section>

        <section class="nos-realisations padding-medium">
            <div class="section-header text-center">
                <h2 class="section-title pb-4">Nos Réalisations</h2>
            </div>
            <div class="container">
                <div class="row">
                    <div class="flex-container">
                        <div class="achievement-box mb-3">
                            <div class="tracker mb-4">
                                <span class="number">19</span>
                            </div>
                            <div class="iconbox-content text-center">
                                <i class="icon icon-suitcase"></i>
                                <h4>Cours de Formation</h4>
                            </div><!--iconbox-->
                        </div><!--achievement-box-->
                        <div class="achievement-box mb-3">
                            <div class="tracker mb-4">
                                <span class="number">879</span>
                            </div>
                            <div class="iconbox-content text-center">
                                <i class="icon icon-clock2"></i>
                                <h4>Heures de Travail</h4>
                            </div><!--iconbox-->
                        </div><!--achievement-box-->
                        <div class="achievement-box mb-3">
                            <div class="tracker mb-4">
                                <span class="number">150</span>
                            </div>
                            <div class="iconbox-content text-center">
                                <i class="icon icon-users"></i>
                                <h4>Clients Satisfaits</h4>
                            </div><!--iconbox-->
                        </div><!--achievement-box-->
                        <div class="achievement-box mb-3">
                            <div class="tracker mb-4">
                                <span class="number">7</span>
                            </div>
                            <div class="iconbox-content text-center">
                                <i class="icon icon-trophy"></i>
                                <h4>Prix Internationaux pour nos Gymnases</h4>
                            </div><!--iconbox-->
                        </div><!--achievement-box-->
                    </div><!--content-->
                </div>
            </div>
        </section>

        <section class="services">
            <div class="workout-box front-dark col-md-6 p-0">
                <img src="/assets2/images/bodybuilding.jpg" alt="musculation" class="fitnessImg">
                <div class="text-content light">
                    <h3 class="colored">Musculation</h3>
                    <p>Développez votre force et sculptez votre corps avec nos séances de musculation. Nos coaches
                        experts vous guideront pour atteindre vos objectifs de manière efficace.</p>
                </div><!--text-content-->
            </div><!--workout-box-->
            <div class="workout-box front-dark col-md-6 p-0">
                <img src="/assets2/images/cardio.jpg" alt="cardio" class="fitnessImg">
                <div class="text-content light">
                    <h3 class="colored">Cardio</h3>
                    <p>Améliorez votre endurance cardiovasculaire avec nos sessions de cardio. Brûlez des calories,
                        renforcez votre cœur et vivez une expérience énergisante.</p>
                </div><!--text-content-->
            </div><!--workout-box-->
            <div class="workout-box front-dark col-md-6 p-0">
                <img src="/assets2/images/fitness.jpg" alt="fitness" class="fitnessImg">
                <div class="text-content light">
                    <h3 class="colored">Fitness</h3>
                    <p>Adoptez un mode de vie actif avec nos programmes de fitness variés. Que vous soyez débutant ou
                        expert, nos salles offrent des options adaptées à tous les niveaux.</p>
                </div><!--text-content-->
            </div><!--workout-box-->
            <div class="workout-box front-dark col-md-6 p-0">
                <img src="/assets2/images/crossfit.jpg" alt="crossfit" class="fitnessImg">
                <div class="text-content light">
                    <h3 class="colored">Crossfit</h3>
                    <p>Testez vos limites avec nos séances de Crossfit. Renforcez votre corps de manière fonctionnelle
                        et vivez une expérience d'entraînement intense et motivante.</p>
                </div><!--text-content-->
            </div><!--workout-box-->
        </section>

        <section class="nos-entraineurs mt-50 pb-60">
            <h2 class="section-title text-center mb-4">Nos Entraîneurs</h2>
            <div class="container">
                <div class="row">
                    <div class="col-md-3 text-center">
                        <figure>
                            <a href="#"><img src="/assets2/images/david.jpg" alt="poste"
                                    class="teamImg"></a>
                        </figure>
                        <div class="text-content text-center mt-3 mb-3">
                            <h4><a href="#">David Williams</a></h4>
                            <span class="designation colored-grey">Entraîneur de Musculation</span>
                            <p>Expérimentez des entraînements de musculation de haute intensité avec David Williams. Son
                                expertise vous guide vers vos objectifs de manière efficace dans nos différents
                                gymnases.</p>
                            <div class="dark social-links">
                                <ul class="list-unstyled d-flex">
                                    <li class="mr-3"><a href="#"><i class="icon icon-facebook m-1"></i></a>
                                    </li>
                                    <li class="mr-3"><a href="#"><i class="icon icon-twitter m-1"></i></a>
                                    </li>
                                    <li><a href="#"><i class="icon icon-youtube m-1"></i></a></li>
                                </ul>
                            </div>
                        </div><!--text-content-->
                    </div><!--col-md-3-->

                    <div class="col-md-3 text-center">
                        <figure>
                            <a href="#"><img src="/assets2/images/rosy.jpg" alt="poste"
                                    class="teamImg"></a>
                        </figure>
                        <div class="text-content text-center mt-3 mb-3">
                            <h4><a href="#">Rosy Rivera</a></h4>
                            <span class="designation colored-grey">Entraîneur de Musculation</span>
                            <p>Découvrez des séances de musculation innovantes avec Rosy Rivera. Son approche unique
                                vous aide à repousser vos limites dans nos différents gymnases.</p>
                            <div class="dark social-links">
                                <ul class="list-unstyled d-flex">
                                    <li class="mr-3"><a href="#"><i class="icon icon-facebook m-1"></i></a>
                                    </li>
                                    <li class="mr-3"><a href="#"><i class="icon icon-twitter m-1"></i></a>
                                    </li>
                                    <li><a href="#"><i class="icon icon-youtube m-1"></i></a></li>
                                </ul>
                            </div>
                        </div><!--text-content-->
                    </div><!--col-md-3-->

                    <div class="col-md-3 text-center">
                        <figure>
                            <a href="#"><img src="/assets2/images/matt.jpg" alt="poste"
                                    class="teamImg"></a>
                        </figure>
                        <div class="text-content text-center mt-3 mb-3">
                            <h4><a href="#">Matt Stonie</a></h4>
                            <span class="designation colored-grey">Entraîneur de Musculation</span>
                            <p>Relevez le défi avec Matt Stonie et ses séances de musculation uniques. Transformez votre
                                corps avec des entraînements personnalisés dans nos différents gymnases.</p>
                            <div class="dark social-links">
                                <ul class="list-unstyled d-flex">
                                    <li class="mr-3"><a href="#"><i class="icon icon-facebook m-1"></i></a>
                                    </li>
                                    <li class="mr-3"><a href="#"><i class="icon icon-twitter m-1"></i></a>
                                    </li>
                                    <li><a href="#"><i class="icon icon-youtube m-1"></i></a></li>
                                </ul>
                            </div>
                        </div><!--text-content-->
                    </div><!--col-md-3-->

                    <div class="col-md-3 text-center">
                        <figure>
                            <a href="#"><img src="/assets2/images/sofia.jpg" alt="poste"
                                    class="teamImg"></a>
                        </figure>
                        <div class="text-content text-center mt-3 mb-3">
                            <h4><a href="#">Sofia Lauren</a></h4>
                            <span class="designation colored-grey">Entraîneur de Musculation</span>
                            <p>Atteignez vos objectifs avec Sofia Lauren et ses séances de musculation adaptées à tous
                                les niveaux. Découvrez une nouvelle approche du fitness dans nos différents gymnases.
                            </p>
                            <div class="dark social-links">
                                <ul class="list-unstyled d-flex">
                                    <li class="mr-3"><a href="#"><i class="icon icon-facebook m-1"></i></a>
                                    </li>
                                    <li class="mr-3"><a href="#"><i class="icon icon-twitter m-1"></i></a>
                                    </li>
                                    <li><a href="#"><i class="icon icon-youtube m-1"></i></a></li>
                                </ul>
                            </div>
                        </div><!--text-content-->
                    </div><!--col-md-3-->
                </div>

            </div>
        </section>

        <section class="testimonials-wrap mt-3 mb-5">
            <div class="section-header text-center p-30">
                <h2 class="section-title pb-4">Nos TÉMOIGNAGES</h2>
            </div>
            <div class="container">
                <div class="row">
                    <div class="testimonial-slider d-flex mb-6">

                        <div class="col-md-4 testimonials-inner text-center">
                            <figure>
                                <img src="/assets2/images/lucy.jpg" alt="lucy" class="authorImg img-circle">
                            </figure>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.Lorem ipsum dolor sit amet,
                                consectetur adipisicing elit.</p>
                            <div class="testimonial-author">
                                <span class="name colored">Lucy Antony</span>
                            </div>
                        </div>

                        <div class="col-md-4 testimonials-inner text-center">
                            <figure>
                                <img src="/assets2/images/michael.jpg" alt="Michael" class="authorImg img-circle">
                            </figure>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.Lorem ipsum dolor sit amet,
                                consectetur adipisicing elit.</p>
                            <div class="testimonial-author">
                                <span class="name colored">Michael Smith</span>
                            </div>
                        </div>

                        <div class="col-md-4 testimonials-inner text-center">
                            <figure>
                                <img src="/assets2/images/maria.jpg" alt="Maria" class="authorImg img-circle">
                            </figure>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.Lorem ipsum dolor sit amet,
                                consectetur adipisicing elit.</p>
                            <div class="testimonial-author">
                                <span class="name colored">Maria Garcia</span>
                            </div>
                        </div>

                        <div class="col-md-4 testimonials-inner text-center">
                            <figure>
                                <img src="/assets2/images/lucy.jpg" alt="Lucy" class="authorImg img-circle">
                            </figure>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.Lorem ipsum dolor sit amet,
                                consectetur adipisicing elit.</p>
                            <div class="testimonial-author">
                                <span class="name colored">Jennie Rose</span>
                            </div>
                        </div>

                        <div class="col-md-4 testimonials-inner text-center">
                            <figure>
                                <img src="/assets2/images/lucy.jpg" alt="Lucy" class="authorImg img-circle">
                            </figure>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.Lorem ipsum dolor sit amet,
                                consectetur adipisicing elit.</p>
                            <div class="testimonial-author">
                                <span class="name colored">Jennie Rose</span>
                            </div>
                        </div>

                    </div><!--testimonial-slider-->
                </div>
            </div>
        </section>



        <section class="association-with padding-medium bg-sand">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="d-flex list-unstyled justify-content-between flex-wrap">
                            <li><a href="#"><img src="/assets2/images/icon.png" class="brandImg"></a></li>
                            <li><a href="#"><img src="/assets2/images/icon1.png" class="brandImg"></a></li>
                            <li><a href="#"><img src="/assets2/images/icon2.png" class="brandImg"></a></li>
                            <li><a href="#"><img src="/assets2/images/icon3.png" class="brandImg"></a></li>
                            <li><a href="#"><img src="/assets2/images/icon4.png" class="brandImg"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

        <div class="footer-bottom bg-black padding-small">
            <div class="container">
                <div class="row">
                    <div class="wrap flex-container">
                        <div class="copyright">
                            <p class="mt-4 mx-5">© Copyright 2024 </p>
                        </div>
                        <div class="social-links mt-3">
                            <ul class="list-unstyled d-flex">
                                <li class="mr-3"><a href="#"><i class="icon icon-facebook m-1"></i></a></li>
                                <li class="mr-3"><a href="#"><i class="icon icon-twitter m-1"></i></a></li>
                                <li><a href="#"><i class="icon icon-youtube m-1"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div><!--body-wrapper-->


    <script src="/assets2/js/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="/assets2/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript" src="/assets2/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/assets2/js/hc-sticky.js"></script>

    <script type="text/javascript" src="/assets2/js/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="/assets2/slick/slick/slick.min.js"></script>
    <script type="text/javascript" src="/assets2/js/script.js"></script>
    <script src="https://code.iconify.design/iconify-icon/1.0.7/iconify-icon.min.js"></script>
    <script>
        function showAlertSuccess(alertId) {
            var alertElement = document.getElementById(alertId);
            if (alertElement) {
                swal("Abonnement réussie avec succès", "Veuillez consulter votre mail pour plus de détail", {
                    icon: "success",
                    buttons: {
                        confirm: {
                            className: 'btn btn-success'
                        }
                    },
                });

            }
        }
        showAlertSuccess('alertS20')
    </script>


</body>

</html>
