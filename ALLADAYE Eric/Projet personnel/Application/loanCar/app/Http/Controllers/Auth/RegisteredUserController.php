<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Validation\ValidationException;

use App\Http\Controllers\Controller;
use App\Models\Media;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;
use Illuminate\View\View;
use RealRashid\SweetAlert\Facades\Alert;


class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     */
    public function create(): View
    {
        return view('admin.auth.register');
    }

    /**
     * Handle an incoming registration request.
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        // dd($request->hasFile('identity_photo'));  
        try {

            $request->validate([
                'name' => ['required', 'string', 'max:255'],
                'firstname' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'lowercase', 'email', 'max:255', 'unique:' . User::class],
                'sexe' => ['required', 'string', 'in:male,female,other'],
                'country_code' => ['required', 'string', 'max:2'],
                'phone' => ['required', 'string', 'max:20'], // Note: You may need to adjust the max length based on your database field.
                'ifu' => ['required', 'integer', 'unique:' . User::class],
                'profession' => ['required', 'string', 'max:255'],
                // 'identity_photo' => ['nullable', 'image', 'max:10240'], // Maximum file size: 10 MB
                'role' => ['required', 'string', 'in:2,3,les_deux'],
                'password' => ['required', 'confirmed', Rules\Password::defaults()],
            ]);
        
            
            $user = User::create([
                'nom' => $request->name,
                'prenom' => $request->firstname,
                'email' => $request->email,
                'sexe' => $request->sexe,
                'country_code' => $request->country_code,
                'telephone' => $request->phone,
                'ifu' => $request->ifu,
                'profession' => $request->profession,
                // 'role' => $request->role,
                'password' => Hash::make($request->password),
            ]);
        
            $roleId = ($request->role === 'les_deux') ? [2, 3] : [$request->role];

            foreach ($roleId as $roleIdValue) {
                $user->permissions()->create([
                    'role_id' => $roleIdValue,
                ]);
            }
            
            // Check if an identity photo is provided
            if ($request->hasFile('identity_photo')) {
                $path = $request->file('identity_photo')->store('identity_photos');
                $media = Media::create([
                    'type' => 'identity_photo',
                    'users_id' => $user->id,
                    'path' => $path,
                ]);
                $user->media()->save($media);
            }
            
            event(new Registered($user));

            // Alert::success('Inscription réussie', 'Bienvenue, ' . $user->prenom . '!');
        
            // Auth::login($user);

            return view('admin.auth.success', ['name' => $request->firstname]);
        
            // return redirect(RouteServiceProvider::HOME);
    
        
    
        } catch (ValidationException $e) {
            $errors = $e->errors();
            $errorMessage = '';
            foreach ($errors as $error) {
                $errorMessage .= implode('<br>', $error);
            }
            Alert::error('Erreur de validation', $errorMessage);
            return redirect()->back()->withErrors($e->errors())->withInput();
        } catch (\Exception $e) {
            Alert::error('Erreur lors de l\'inscription', $e->getMessage());
            return redirect()->back();
        }
    
    }
    
}
