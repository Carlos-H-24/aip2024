<?php

// app/Http/Controllers/TypeVehiculeController.php
namespace App\Http\Controllers;

use App\Models\TypeVehicule;
use Illuminate\Http\Request;

class TypeVehiculeController extends Controller
{
    public function index()
    {
        $typesVehicules = TypeVehicule::all();
        return view('admin.pages.typesvehicules.index', compact('typesVehicules'));
    }

    public function create()
    {
        return view('admin.pages.typesvehicules.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'libelle' => 'required|string',
            'caracteristiques' => 'required|string',
        ]);

        TypeVehicule::create($request->all());

        return redirect()->route('typesvehicules.index')->with('success', 'Type de véhicule ajouté avec succès.');
    }

    public function edit(TypeVehicule $typeVehicule)
    {
        return view('admin.pages.typesvehicules.edit', compact('typeVehicule'));
    }

    public function update(Request $request, TypeVehicule $typeVehicule)
    {
        $request->validate([
            'libelle' => 'required|string',
            'caracteristiques' => 'required|string',
        ]);

        $typeVehicule->update($request->all());

        return redirect()->route('typesvehicules.index')->with('success', 'Type de véhicule mis à jour avec succès.');
    }

    public function destroy(TypeVehicule $typeVehicule)
    {
        $typeVehicule->delete();

        return redirect()->route('typesvehicules.index')->with('success', 'Type de véhicule supprimé avec succès.');
    }
}
