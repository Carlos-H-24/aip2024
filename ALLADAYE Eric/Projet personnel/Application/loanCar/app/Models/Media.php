<?php
// app/Models/Media.php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    protected $table= 'medias';
    protected $fillable = ['type', 'path', 'vehicule_id', 'users_id'];

    // Relation avec les utilisateurs
    public function user()
    {
        return $this->belongsTo(User::class, 'users_id');
    }

    // Ajoutez la relation avec les véhicules si nécessaire
    public function vehicule()
    {
        return $this->belongsTo(Vehicule::class, 'vehicule_id');
    }
}
