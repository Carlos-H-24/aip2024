<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $libelle
 * @property string $description
 * @property string $typeCarrosserie
 * @property string $autreDetails
 * @property integer $tarifJour
 * @property integer $type_vehicule
 * @property integer $annee_id
 * @property integer $modele
 * @property string $created_at
 * @property string $updated_at
 */
class Vehicule extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['libelle', 'description', 'typeCarrosserie', 'autreDetails', 'tarifJour', 'type_vehicule', 'annee_id', 'modele', 'created_at', 'updated_at'];
}
