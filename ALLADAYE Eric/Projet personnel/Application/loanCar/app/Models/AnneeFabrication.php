<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AnneeFabrication extends Model
{
    protected $fillable = ['annee'];

    // Relation avec les véhicules
    public function vehicules()
    {
        return $this->hasMany(Vehicule::class, 'annee_id');
    }
}
