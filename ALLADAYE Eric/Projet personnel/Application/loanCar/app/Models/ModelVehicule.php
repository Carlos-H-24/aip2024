<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModelVehicule extends Model
{
    protected $fillable = ['libelle', 'marque_id'];

    // Relation avec la marque du véhicule
    public function marque()
    {
        return $this->belongsTo(MarqueVehicule::class, 'marque_id');
    }

    // Relation avec les véhicules
    public function vehicules()
    {
        return $this->hasMany(Vehicule::class, 'modele');
    }
}
