<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $libelle
 * @property string $created_at
 * @property string $updated_at
 */
class Role extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['libelle', 'created_at', 'updated_at'];
}
