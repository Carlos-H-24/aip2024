<?php

// app/Models/MarqueVehicule.php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MarqueVehicule extends Model
{
    protected $fillable = ['libelle', 'pays'];

    // Relation avec les modèles de véhicules
    public function modeles()
    {
        return $this->hasMany(ModelVehicule::class, 'marque_id');
    }
}

