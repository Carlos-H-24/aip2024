<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TypeVehicule extends Model
{

    protected $table='types_vehicules';
    protected $fillable = ['libelle', 'caracteristiques'];

    // Relation avec les véhicules
    public function vehicules()
    {
        return $this->hasMany(Vehicule::class, 'type_vehicule');
    }
}

