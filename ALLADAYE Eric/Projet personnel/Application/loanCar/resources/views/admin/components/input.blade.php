@isset($data)

@switch($data['type'])

@case("text")
<div class="{{$data['class']?? 'col-12'}}">
    <label for="{{$data['id']?? ''}}" class="form-label">{{$data['label']}}</label>
    <input type="text" class="form-control" id="{{$data['id']?? ''}}" name="{{$data['name'] ?? ''}}" @if($data['required']) required @endif value="{{$data['value']?? old($data['name'])}}">
    <div class="valid-feedback">
        Donnée valide
    </div>
    <div class="invalid-feedback">
    Le libellé est requis
    </div>
</div>

@break
@case("textarea")
<div class="{{$data['class']?? 'col-12'}}">
    <label for="{{$data['id']?? ''}}" class="form-label">{{$data['label']}}</label>
    <textarea class="form-control" id="{{$data['id']?? ''}}" name="{{$data['name'] ?? ''}}" @if($data['required']) required @endif >{{$data['value']?? old($data['name'])}}</textarea>
    <div class="valid-feedback">
        Donnée valide
    </div>
    <div class="invalid-feedback">
    Le libellé est requis
    </div>
</div>

@break

@case("file")
<div class="{{$data['class']?? 'col-12'}}">
    <label for="{{$data['id']?? ''}}" class="form-label">{{$data['label']}}</label>
    <input type="file" class="form-control" id="{{$data['id']?? ''}}" name="{{$data['name'] ?? ''}}" @if($data['required']) required @endif>
    @if($data['value'])<span class="help-text"><a href="{{$data['value']}}">consulter le fichier</a></span>@endif
    <div class="valid-feedback">
        Donnée valide
    </div>
    <div class="invalid-feedback">
     La donnée {{$data['label'] ?? ''}} est requis
    </div>
</div>

@break

@case("select")

<div class="{{$data['class']?? 'col-12'}}">
    <label for="{{$data['id']?? ''}}" class="form-label">{{$data['label']}}</label>
    <select class="form-control" id="{{$data['id']?? ''}}" name="{{$data['name'] ?? ''}}" @if($data['required']) required @endif>
        <option selected disabled></option>
        @foreach($items->toArray() as $item)
                <option @if($data['value'] && $data['value']==$item[$opt_value]) selected  @endif value="{{$item[$opt_value]}}">{{$item[$opt_name]}}</option>
        @endforeach
    </select>
    <div class="valid-feedback">
        Donnée valide
    </div>
    <div class="invalid-feedback">
     La donnée {{$data['label'] ?? ''}} est requis
    </div>
</div>

@break


@default
<p></p>
@endswitch

@endisset