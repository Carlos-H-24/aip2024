<div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                                <h4 class="mb-sm-0">{{$title ?? "Titre de la page"}}</h4>

                                {{-- @isset($links)
                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        @foreach($links as $link)
                                        @if($link['has_link'])
                                        <li class="breadcrumb-item"><a href="">Name</a></li>
                                        @else
                                        <li class="breadcrumb-item active">Name</li>
                                        @endif
                                        @endforeach
                                    </ol>
                                </div>
                                @endisset --}}

                            </div>
                        </div>
                    </div>