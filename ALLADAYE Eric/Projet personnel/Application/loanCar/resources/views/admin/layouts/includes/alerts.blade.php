



@if(Session::has('success'))

<!-- Success Alert -->
<div class="alert alert-success alert-dismissible alert-label-icon label-arrow fade show" role="alert">
    <i class="ri-notification-off-line label-icon"></i><strong>Success</strong> - {{Session::get('success')}}
    <button type="button" class="btn-close" data-bs-dismiss=" alert" aria-label="Close"></button>
</div>
@endif

@if(Session::has('error'))

<!-- Danger Alert -->
<div class="alert alert-danger alert-dismissible alert-label-icon label-arrow fade show" role="alert">
    <i class="ri-error-warning-line label-icon"></i><strong>Danger</strong> - {{Session::get('error')}}
    <button type="button" class="btn-close" data-bs-dismiss=" alert" aria-label="Close"></button>
</div>
@endif


@if(Session::has('warning'))

<!-- Warning Alert -->
<div class="alert alert-warning alert-dismissible alert-label-icon label-arrow fade show" role="alert">
    <i class="ri-alert-line label-icon"></i><strong>Attention</strong> - {{Session::get('warning')}}
    <button type="button" class="btn-close" data-bs-dismiss=" alert" aria-label="Close"></button>
</div>
@endif


@if(Session::has('info'))

<!-- Info Alert -->
<div class="alert alert-info alert-dismissible alert-label-icon label-arrow fade show" role="alert">
    <i class="ri-airplay-line label-icon"></i><strong>Info</strong> - {{Session::get('info')}}
    <button type="button" class="btn-close" data-bs-dismiss=" alert" aria-label="Close"></button>
</div>

@endif


@if($errors->any())
    {!! implode('', $errors->all('<div class="alert alert-danger alert-dismissible alert-label-icon label-arrow fade show" role="alert">
    <i class="ri-error-warning-line label-icon"></i><strong>Danger</strong> - :message
    <button type="button" class="btn-close" data-bs-dismiss=" alert" aria-label="Close"></button>
</div>')) !!}
@endif