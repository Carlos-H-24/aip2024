

<!-- Primary Alert -->
<div class="alert alert-primary alert-dismissible alert-label-icon label-arrow fade show" role="alert">
    <i class="ri-user-smile-line label-icon"></i><strong>Primary</strong> - Label icon arrow  alert
    <button type="button" class="btn-close" data-bs-dismiss=" alert" aria-label="Close"></button>
</div>

<!-- Secondary Alert -->
<div class="alert alert-secondary alert-dismissible alert-label-icon label-arrow fade show" role="alert">
    <i class="ri-check-double-line label-icon"></i><strong>Secondary</strong> - Label icon arrow  alert
    <button type="button" class="btn-close" data-bs-dismiss=" alert" aria-label="Close"></button>
</div>

<!-- Success Alert -->
<div class="alert alert-success alert-dismissible alert-label-icon label-arrow fade show" role="alert">
    <i class="ri-notification-off-line label-icon"></i><strong>Success</strong> - Label icon arrow  alert
    <button type="button" class="btn-close" data-bs-dismiss=" alert" aria-label="Close"></button>
</div>

<!-- Danger Alert -->
<div class="alert alert-danger alert-dismissible alert-label-icon label-arrow fade show" role="alert">
    <i class="ri-error-warning-line label-icon"></i><strong>Danger</strong> - Label icon arrow  alert
    <button type="button" class="btn-close" data-bs-dismiss=" alert" aria-label="Close"></button>
</div>

<!-- Warning Alert -->
<div class="alert alert-warning alert-dismissible alert-label-icon label-arrow fade show" role="alert">
    <i class="ri-alert-line label-icon"></i><strong>Warning</strong> - Label icon arrow  alert
    <button type="button" class="btn-close" data-bs-dismiss=" alert" aria-label="Close"></button>
</div>

<!-- Info Alert -->
<div class="alert alert-info alert-dismissible alert-label-icon label-arrow fade show" role="alert">
    <i class="ri-airplay-line label-icon"></i><strong>Info</strong> - Label icon arrow  alert
    <button type="button" class="btn-close" data-bs-dismiss=" alert" aria-label="Close"></button>
</div>

<!-- Light Alert -->
<div class="alert alert-light alert-dismissible alert-label-icon label-arrow fade show" role="alert">
    <i class="ri-mail-line label-icon"></i><strong>Light</strong> - Label icon arrow  alert
    <button type="button" class="btn-close" data-bs-dismiss=" alert" aria-label="Close"></button>
</div>
    
<!-- Dark Alert -->
<div class="alert alert-dark alert-dismissible alert-label-icon label-arrow fade show mb-0" role="alert">
    <i class="ri-refresh-line label-icon"></i><strong>Dark</strong> - Label icon arrow  alert
    <button type="button" class="btn-close" data-bs-dismiss=" alert" aria-label="Close"></button>
</div>