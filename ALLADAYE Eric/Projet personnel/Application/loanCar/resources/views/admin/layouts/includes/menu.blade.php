<div class="app-menu navbar-menu">
    <!-- LOGO -->
    <div class="navbar-brand-box">
        <!-- Dark Logo-->
        <a href="index.html" class="logo logo-dark">
            <span class="logo-sm">
                <img src="assets-admin/image/logo-goDriveHub1.png" style="width: 3cm;height: 3cm; border-radius: 50%" alt="" height="22">
            </span>
            <span class="logo-lg">
                <img src="assets-admin/images/logo-goDriveHub1.png" style="width: 3cm;height: 3cm; border-radius: 50%" alt="" height="17">
            </span>
        </a>
        <!-- Light Logo-->
        <a href="index.html" class="logo logo-light">
            <span class="logo-sm">
                <img src="assets-admin/images/logo-goDriveHub1.png" style="width: 3cm;height: 3cm; border-radius: 50%" alt="" height="22">
            </span>
            <span class="logo-lg">
                <img src="assets-admin/images/logo-goDriveHub1.png" style="width: 3cm;height: 3cm; border-radius: 50%" alt="" height="17">
            </span>
        </a>
        <button type="button" class="btn btn-sm p-0 fs-20 header-item float-end btn-vertical-sm-hover" id="vertical-hover">
            <i class="ri-record-circle-line"></i>
        </button>
    </div>

    
    <div id="scrollbar" data-simplebar="init" class="h-100 simplebar-scrollable-y"><div class="simplebar-wrapper" style="margin: 0px;"><div class="simplebar-height-auto-observer-wrapper"><div class="simplebar-height-auto-observer"></div></div><div class="simplebar-mask"><div class="simplebar-offset" style="right: 0px; bottom: 0px;"><div class="simplebar-content-wrapper" tabindex="0" role="region" aria-label="scrollable content" style="height: 100%; overflow: hidden scroll;"><div class="simplebar-content" style="padding: 0px;">
        <div class="container-fluid">


            <div id="two-column-menu">
            </div>
            <ul class="navbar-nav" id="navbar-nav" data-simplebar="init"><div class="simplebar-wrapper" style="margin: 0px;"><div class="simplebar-height-auto-observer-wrapper"><div class="simplebar-height-auto-observer"></div></div><div class="simplebar-mask"><div class="simplebar-offset" style="right: 0px; bottom: 0px;"><div class="simplebar-content-wrapper" tabindex="0" role="region" aria-label="scrollable content" style="height: auto; overflow: hidden;"><div class="simplebar-content" style="padding: 0px;">
                <li class="menu-title"><span data-key="t-menu">Menu</span></li>
                <li class="nav-item">
                    <a class="nav-link menu-link active" href="widgets.html" aria-expanded="false">
                        <i class="ri-honour-line"></i> <span data-key="t-widgets">Tableau de bord</span>
                    </a>
                </li><!-- end Dashboard Menu -->
                <li class="nav-item">
                    <a class="nav-link menu-link active" href="widgets.html" aria-expanded="false">
                        <i class="ri-honour-line"></i> <span data-key="t-widgets">Locations</span>
                    </a>
                </li><!-- end Dashboard Menu -->
                <li class="nav-item">
                    <a class="nav-link menu-link active" href="widgets.html" aria-expanded="false">
                        <i class="ri-honour-line"></i> <span data-key="t-widgets">Historiques</span>
                    </a>
                </li><!-- end Dashboard Menu -->
                <li class="nav-item">
                    <a class="nav-link menu-link collapsed" href="#sidebarApps1" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarApps1">
                        <i class="ri-apps-2-line"></i> <span data-key="t-apps">Réservations</span>
                    </a>
                    <div class="collapse menu-dropdown" id="sidebarApps1">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a href="#sidebarCalendar" class="nav-link menu-link active">
                                    En cours
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#sidebarCalendar" class="nav-link menu-link active">
                                    Terminées
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#sidebarCalendar" class="nav-link menu-link active">
                                    Programmées
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link menu-link collapsed" href="#sidebarApps" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarApps">
                        <i class="ri-apps-2-line"></i> <span data-key="t-apps">Paramètres</span>
                    </a>
                    <div class="collapse menu-dropdown" id="sidebarApps">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a href="#sidebarCalendar" class="nav-link menu-link active">
                                    Types Vehicules
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#sidebarCalendar" class="nav-link menu-link active">
                                    Modèles Vehicules
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#sidebarCalendar" class="nav-link menu-link active">
                                    Marques Vehicules
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#sidebarCalendar" class="nav-link menu-link active">
                                    Annee de Fabricacion
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#sidebarCalendar" class="nav-link menu-link active">
                                    Moyens de Paiements
                                </a>
                            </li>
                            
                        </ul>
                    </div>
                </li>
            </div></div></div></div><div class="simplebar-placeholder" style="width: 249px; height: 1086px;"></div></div><div class="simplebar-track simplebar-horizontal" style="visibility: hidden;"><div class="simplebar-scrollbar" style="width: 0px; display: none; transform: translate3d(0px, 0px, 0px);"></div></div><div class="simplebar-track simplebar-vertical" style="visibility: hidden;"><div class="simplebar-scrollbar" style="height: 0px; display: none;"></div></div><div class="simplebar-track simplebar-horizontal"><div class="simplebar-scrollbar"></div></div><div class="simplebar-track simplebar-vertical"><div class="simplebar-scrollbar"></div></div><div class="simplebar-track simplebar-horizontal"><div class="simplebar-scrollbar"></div></div><div class="simplebar-track simplebar-vertical"><div class="simplebar-scrollbar"></div></div><div class="simplebar-track simplebar-horizontal"><div class="simplebar-scrollbar"></div></div><div class="simplebar-track simplebar-vertical"><div class="simplebar-scrollbar"></div></div><div class="simplebar-track simplebar-horizontal"><div class="simplebar-scrollbar"></div></div><div class="simplebar-track simplebar-vertical"><div class="simplebar-scrollbar"></div></div></ul>
        </div>
        <!-- Sidebar -->
    </div></div></div></div><div class="simplebar-placeholder" style="width: 249px; height: 1086px;"></div></div><div class="simplebar-track simplebar-horizontal" style="visibility: hidden;"><div class="simplebar-scrollbar" style="width: 0px; display: none; transform: translate3d(0px, 0px, 0px);"></div></div><div class="simplebar-track simplebar-vertical" style="visibility: visible;"><div class="simplebar-scrollbar" style="height: 234px; transform: translate3d(0px, 0px, 0px); display: block;"></div></div></div>

    <div class="sidebar-background"></div>
</div>