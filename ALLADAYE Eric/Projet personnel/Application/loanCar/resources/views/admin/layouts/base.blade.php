<!doctype html>
<html lang="en" data-layout="vertical" data-topbar="light" data-sidebar="dark" data-sidebar-size="lg" data-sidebar-image="none" data-preloader="disable">


<!-- Mirrored from themesbrand.com/velzon/html/default/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 09 Jan 2023 16:23:00 GMT -->
<head>

    <meta charset="utf-8" />
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Location vehicule" name="description" />
    <meta content="Themesbrand" name="author" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{asset('assets/images/gennius.jpg')}}">
    <link rel="stylesheet" href="{{asset('assets-admin/libs/sweetalert2/sweetalert2.min.css')}}">

    @stack('css')

    <!-- Layout config Js -->
    <script src="{{asset('assets-admin/js/layout.js')}}"></script>
    <!-- Bootstrap Css -->
    <link href="{{asset('assets-admin/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- Icons Css -->
    <link href="{{asset('assets-admin/css/icons.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- App Css-->
    <link href="{{asset('assets-admin/css/app.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- custom Css-->
    <link href="{{asset('assets-admin/css/custom.min.css')}}" rel="stylesheet" type="text/css" />

</head>

<body>

    <!-- Begin page -->
    <div id="layout-wrapper">

        @include('admin.layouts.includes.header')
        @include('admin.layouts.includes.other')

<!-- /.modal -->
        <!-- ========== App Menu ========== -->
        @include('admin.layouts.includes.menu')

        <!-- Left Sidebar End -->
        <!-- Vertical Overlay-->
        <div class="vertical-overlay"></div>

        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="main-content">

            <div class="page-content">
                <div class="container-fluid">

                    <!-- start page title -->
                    {{-- @include('admin.layouts.includes.breadcrumb',["title"=>$title, "links"=>$links]) --}}
                    @include('admin.layouts.includes.alerts')

                    @yield('body-content')
                    <!-- end page title -->

                </div>
                <!-- container-fluid -->
            </div>
            <!-- End Page-content -->
            @include('admin.layouts.includes.footer')

        </div>
        <!-- end main content-->

    </div>
    <!-- END layout-wrapper -->



    <!--start back-to-top-->
    <button onclick="topFunction()" class="btn btn-danger btn-icon" id="back-to-top">
        <i class="ri-arrow-up-line"></i>
    </button>
    <!--end back-to-top-->

    <!--preloader-->
    <div id="preloader">
        <div id="status">
            <div class="spinner-border text-primary avatar-sm" role="status">
                <span class="visually-hidden">Chargement...</span>
            </div>
        </div>
    </div>



    <!-- JAVASCRIPT -->
    <script src="{{asset('assets-admin/libs/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('assets-admin/libs/simplebar/simplebar.min.js')}}"></script>
    <script src="{{asset('assets-admin/libs/node-waves/waves.min.js')}}"></script>
    <script src="{{asset('assets-admin/libs/feather-icons/feather.min.js')}}"></script>
    <script src="{{asset('assets-admin/js/pages/plugins/lord-icon-2.1.0.js')}}"></script>
    <script src="{{asset('assets-admin/js/plugins.js')}}"></script>*

    <script src="{{asset('assets-admin/libs/sweetalert2/sweetalert2.min.js')}}"></script>

    @stack('js')

    <!-- App js -->
    <script src="{{asset('assets-admin/js/app.js')}}"></script>
    <script>
        function sendDeleteForm(e,id) {
        e.preventDefault();
        Swal.fire({
            html:'<div class="mt-3"><lord-icon src="https://cdn.lordicon.com/gsqxdxog.json" trigger="loop" colors="primary:#f7b84b,secondary:#f06548" style="width:100px;height:100px"></lord-icon><div class="mt-4 pt-2 fs-15 mx-5"><h4>Suppression ?</h4><p class="text-muted mx-4 mb-0">Voulez vous vraiment supprimer cet élément ?</p></div></div>',
            showCancelButton:!0,
            confirmButtonClass:"btn btn-primary w-xs me-2 mb-1",
            confirmButtonText:"Oui, supprimer",
            cancelButtonClass:"btn btn-danger w-xs mb-1",
            cancelButtonText:"Annuler",
            buttonsStyling:!1,
            showCloseButton:!0} ).then(function(result) {
                    if (result.isConfirmed) {
                        document.getElementById(id).submit();
                     }
                });

        
        }
        </script>
</body>


<!-- Mirrored from themesbrand.com/velzon/html/default/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 09 Jan 2023 16:24:32 GMT -->
</html>