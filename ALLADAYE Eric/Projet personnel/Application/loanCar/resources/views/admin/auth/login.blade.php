@extends('admin.layouts.auth-base')
@section('title')
Authentification | GoDriveHub
@endsection
@section('content')

<div class="row justify-content-center">
    <!-- Session Status -->
                    @if(session('status'))
                    <div class="mb-4">{{ session('status') }}</div>
                    @endif

                    <div class="col-md-8 col-lg-6 col-xl-5">
                        <div class="card mt-4">

                            <div class="card-body p-4">
                                <div class="text-center mt-2">
                                    <h5 class="text-primary">Bienvenue</h5>
                                    <p class="text-muted">Connectez vous pour continuer</p>
                                </div>
                                <div class="p-2 mt-4">
                                    <form method="POST" action="{{ route('login') }}">
                                    @csrf
                                        <div class="mb-3">
                                            <label for="username" class="form-label">Email</label>
                                            <input type="text" class="form-control" id="username" type="email" name="email" value="{{ old('email') }}" required autofocus autocomplete="username" />
                                                @if($errors->has('email'))
                                                        <div class="mt-2 text-danger">
                                                        {{ $errors->first('email') }}
                                                            {{-- *Veillez entrez un email vailide --}}
                                                    </div>
                                                @endif
                                        </div>

                                        <div class="mb-3">
                                            <div class="float-end">
                                                <a href="auth-pass-reset-basic.html" class="text-muted">Mot de passe oublié ?</a>
                                            </div>
                                            <label class="form-label" for="password-input">Mot de passe</label>
                                            <div class="position-relative auth-pass-inputgroup mb-3">
                                                <input type="password" class="form-control pe-5 password-input" placeholder="Enter password" id="password-input" type="password" name="password" required autocomplete="current-password" />
                                                    @if($errors->has('password'))
                                                        <div class="mt-2 text-danger">
                                                            {{ $errors->first('password') }}
                                                        </div>
                                                    @endif
                                                <button class="btn btn-link position-absolute end-0 top-0 text-decoration-none text-muted password-addon" type="button" id="password-addon"><i class="ri-eye-fill align-middle"></i></button>
                                            </div>
                                        </div>

                     
                                        <div class="mt-4">
                                            <button class="btn btn-success w-100" type="submit">Se connecter</button>
                                        </div>

                                      
                                    </form>
                                    <div class="mt-4 text-center">
                                        <p class="mb-0">Pas encore un compte ? <a href="/register" class="fw-bold text-primary text-decoration-underline"> S'inscrire ! </a> </p>
                                    </div>
                                </div>
                            </div>
                            <!-- end card body -->
                        </div>
                        <!-- end card -->

                    </div>
                </div>
@endsection
