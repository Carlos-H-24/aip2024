@extends('admin.layouts.auth-base')

@section('title')
Inscription | GoDriveHub
@endsection
@section('content')
<style>
    .iti {
    width: 100%!important;
}
</style>
    <div class="row justify-content-center">
        <!-- Session Status -->
                        @if(session('status'))
                        <div class="mb-4">{{ session('status') }}</div>
                        @endif
                        <div class="row justify-content-center">
                            <div class="col-md-8 col-lg-8 col-xl-8">
                                <div class="card mt-4">
                        
                                    <div class="card-body p-4">
                                        @if (session('success'))
                                        <div class="alert alert-success">{{session('success')}}</div>
                                        @elseif(session('error'))
                                            <div class="alert alert-danger">{{session('error')}}</div>
                                        @endif
                                        <div class="text-center mt-2">
                                            <h5 class="text-primary">Creer un nouveau compte</h5>
                                            <p class="text-muted">Explorer toute la potentialité de GoDriveHub maintenant !</p>
                                        </div>
                                        <div class="p-2 mt-4">
                                            <form class="needs-validation" method="POST" action="/register" enctype="multipart/form-data">
                                            @csrf
                                            <div class="row">
                                                <div class="mb-3 col-6">
                                                    <label for="username" class="form-label">Nom <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control" placeholder="Entrez votre nom " id="username"type="text" name="name" value="" required >
                                                    @if ($errors->has('name'))
                                                        <span class="text-red-600">{{ $errors->first('name') }}</span>
                                                    @endif
                                                    <div class="invalid-feedback">
                                                        Veillez entrez votre nom!
                                                    </div>
                                                </div>
                                                <div class="mb-3 col-6">
                                                    <label for="username" class="form-label">Prénom(s) <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control" placeholder="Entrez votre prenom " id="username"type="text" name="firstname" value="" required >
                                                    @if ($errors->has('firstname'))
                                                        <span class="text-red-600">{{ $errors->first('firstname') }}</span>
                                                    @endif
                                                    <div class="invalid-feedback">
                                                        Veillez entrez vos prenoms!
                                                    </div>
                                                </div>
                                                
                                            </div>
                                          
                                            <div class="row">
                                                <div class="mb-3 col-6">
                                                    <label for="useremail" class="form-label">Email <span class="text-danger">*</span></label>
                                                    <input type="email" class="form-control" placeholder="Entrer votre mail" id="useremail" type="email" name="email" value="" required >
                                                        @if ($errors->has('email'))
                                                            <span class="text-red-600">{{ $errors->first('email') }}</span>
                                                        @endif
                                                    <div class="invalid-feedback">
                                                        Veillez entrez votre mail !
                                                    </div>
                                                </div>
                                                <div class="mb-3 col-6">
                                                    <label for="sexe" class="form-label">Sexe <span class="text-danger">*</span></label>
                                                    <select class="form-select" id="usergender" name="sexe" required>
                                                        <option value="" selected disabled>Choisissez le sexe</option>
                                                        <option value="male" {{ old('gender') == 'male' ? 'selected' : '' }}>Homme</option>
                                                        <option value="female" {{ old('gender') == 'female' ? 'selected' : '' }}>Femme</option>
                                                        <option value="other" {{ old('gender') == 'other' ? 'selected' : '' }}>Autre</option>
                                                    </select>
                                                    @if ($errors->has('sexe'))
                                                        <span class="text-red-600">{{ $errors->first('gender') }}</span>
                                                    @endif
                                                    <div class="invalid-feedback">
                                                        Veuillez sélectionner votre sexe !
                                                    </div>
                                                </div>
                                              
                                            </div>
                                            <div class="row">
                                                <div class="mb-3 col-12">

                                                    <label for="useremail" class="form-label">Téléphone <span class="text-danger">*</span></label><br>
                                                    <input type="hidden" id="country_code" name="country_code" value="">

                                                    <input type="phone" id="phone" class="form-control" placeholder="Entrer votre numéro téléphonique" type="phone" name="phone" value="+" required >
                                                       
                                                    @if ($errors->has('telephone'))
                                                            <span class="text-red-600">{{ $errors->first('telephone') }}</span>
                                                        @endif
                                                    <div class="invalid-feedback">
                                                        Veillez entrez votre numéro de téléphone !
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="mb-3 col-6">
                                                    <label for="useremail" class="form-label">Numéro IFU <span class="text-danger">*</span></label>
                                                    <input type="number" class="form-control" placeholder="Entrer votre numéro IFU" id="useremail"  name="ifu" value="" required >
                                                        @if ($errors->has('ifu'))
                                                            <span class="text-red-600">{{ $errors->first('ifu') }}</span>
                                                        @endif
                                                    <div class="invalid-feedback">
                                                        Veillez entrez votre mail !
                                                    </div>
                                                </div>
                                                <div class="mb-3 col-6">
                                                    <label for="useremail" class="form-label">Profession <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control" placeholder="Entrer votre profession" id="useremail" name="profession" value="" required  >
                                                        @if ($errors->has('profession'))
                                                            <span class="text-red-600">{{ $errors->first('profession') }}</span>
                                                        @endif
                                                    <div class="invalid-feedback">
                                                        Veillez entrez votre profession !
                                                    </div>
                                                </div>
                                               
                                            </div>
                                            {{-- <div class="row">
                                                <div class="mb-3 col">
                                                    <label for="usergender" class="form-label">Sexe <span class="text-danger">*</span></label>
                                                    <select class="form-select" id="usergender" name="gender" required>
                                                        <option value="" selected disabled>Choisissez le sexe</option>
                                                        <option value="male" {{ old('gender') == 'male' ? 'selected' : '' }}>Homme</option>
                                                        <option value="female" {{ old('gender') == 'female' ? 'selected' : '' }}>Femme</option>
                                                        <option value="other" {{ old('gender') == 'other' ? 'selected' : '' }}>Autre</option>
                                                    </select>
                                                    @if ($errors->has('gender'))
                                                        <span class="text-red-600">{{ $errors->first('gender') }}</span>
                                                    @endif
                                                    <div class="invalid-feedback">
                                                        Veuillez sélectionner votre sexe !
                                                    </div>
                                                </div>
                                                
                                               
                                            </div> --}}
                                            <div class="row">
                                                <div class="mb-3 col">
                                                    <label for="identity_photo" class="form-label">Photo de votre pièce d'identité <span class="text-danger">*</span></label>
                                                    <input type="file" class="form-control" name="identity_photo" accept="image/*" >
                                                    @if ($errors->has('identity_photo'))
                                                        <span class="text-red-600">{{ $errors->first('identity_photo') }}</span>
                                                    @endif
                                                    <div class="invalid-feedback">
                                                        Veuillez founir une photo de votre pièce d'identité !
                                                    </div>
                                                </div>
                                                
                                               
                                            </div>

                                            {{-- role --}}
                                            <div class="row">
                                                <div class="my-4 col">
                                                    <label class="form-label">Qui êtes-vous ? <span class="text-danger">*</span></label>
                                                    <div class="form-check my-2">
                                                        <input class="form-check-input" type="radio" name="role" id="locateur" value="2" required>
                                                        <label class="form-check-label" for="locateur">
                                                            Locateur (Je propose des véhicules à la location)
                                                        </label>
                                                    </div>
                                                    <div class="form-check my-3">
                                                        <input class="form-check-input" type="radio" name="role" id="locataire" value="3" required>
                                                        <label class="form-check-label" for="locataire">
                                                            Locataire (Je souhaite louer des véhicules)
                                                        </label>
                                                    </div>
                                                    <div class="form-check my-3">
                                                        <input class="form-check-input" type="radio" name="role" id="les_deux" value="les_deux" required>
                                                        <label class="form-check-label" for="les_deux">
                                                            Les deux (Je suis à la fois locateur et locataire)
                                                        </label>
                                                    </div>
                                                    @if ($errors->has('role'))
                                                        <span class="text-red-600">{{ $errors->first('role') }}</span>
                                                    @endif
                                                    <div class="invalid-feedback">
                                                        Veuillez sélectionner votre rôle !
                                                    </div>
                                                </div>
                                            </div>                                            

                                            {{-- mot de passe --}}
                                            <div class="row">
                                                <div class="mb-3 col-6">
                                                    <label class="form-label" for="password-input">Mot de passe</label>
                                                    <div class="position-relative auth-pass-inputgroup">
                                                        <input type="password" class="form-control pe-5 password-input" name="password"  placeholder="Enter password" id="password-input" aria-describedby="passwordInput" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" required>
                                                            @if ($errors->has('password'))
                                                                <span class="text-red-600">{{ $errors->first('password') }}</span>
                                                            @endif
                                                        <button class="btn btn-link position-absolute end-0 top-0 text-decoration-none text-muted password-addon" type="button" id="password-addon"><i class="ri-eye-fill align-middle"></i></button>
                                                        <div class="invalid-feedback">
                                                            Please enter password
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="mb-3 col-6">
                                                    <label class="form-label" for="password-input">Confirme Mot de passe</label>
                                                    <div class="position-relative auth-pass-inputgroup">
                                                        <input type="password" class="form-control pe-5 password-input" name="password_confirmation" placeholder="Enter password" id="password-input" aria-describedby="passwordInput" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" required>

                                                            @if ($errors->has('password'))
                                                                <span class="text-red-600">{{ $errors->first('password') }}</span>
                                                            @endif
                                                        <button class="btn btn-link position-absolute end-0 top-0 text-decoration-none text-muted password-addon" type="button" id="password-addon"><i class="ri-eye-fill align-middle"></i></button>
                                                        <div class="invalid-feedback">
                                                            Please enter password
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="password-contain1" class="p-3 bg-warning mb-2 rounded">
                                                <h5 class="fs-13">Le mot de passe doit contenir:</h5>
                                                <div class="mx-3">
                                                    <p id="pass-length" class="invalid fs-13 mb-2"><li>Minimum <b>8 Caractères</b></p>
                                                <p id="pass-lower" class="invalid fs-13 mb-2"><li>Au moins 1 <b>miniscule</b> (a-z)</p>
                                                <p id="pass-upper" class="invalid fs-13 mb-2"><li>Au moins 1 <b>majuscule</b> (A-Z)</p>
                                                <p id="pass-number" class="invalid fs-13 mb-0">
                                                    <li>Au moins 1 <b>nombre</b>(0-9)</p>
                                                </div>
                                                
                                            </div>
                        
                                                <div class="mb-4">
                                                    <p class="mb-0 ml-4 fs-13 text-muted fst-italic">En cliquant sur s'inscrire vous accepter <a href="#" class="text-primary text-decoration-underline fst-normal fw-semibold">nos conditions d'utilisations.</a></p>
                                                </div>
                        
                                              

                                                
                                                <div class="row d-flex justify-content-center">
                                                    <div class="mt-4 col-xl-4 col-lg-4 col-sm-8">
                                                    <button class="btn btn-success w-100" type="submit">S'inscrire </button>
                                                  </div>
                                                </div>
                                                
                        
                                                {{-- <div class="mt-4 text-center">
                                                    <div class="signin-other-title">
                                                        <h5 class="fs-14 mb-4 title text-muted">Create account with</h5>
                                                    </div>
                        
                                                    <div>
                                                        <button type="button" class="btn btn-primary btn-icon waves-effect waves-light"><i class="ri-facebook-fill fs-16"></i></button>
                                                        <button type="button" class="btn btn-danger btn-icon waves-effect waves-light"><i class="ri-google-fill fs-16"></i></button>
                                                        <button type="button" class="btn btn-dark btn-icon waves-effect waves-light"><i class="ri-github-fill fs-16"></i></button>
                                                        <button type="button" class="btn btn-info btn-icon waves-effect waves-light"><i class="ri-twitter-fill fs-16"></i></button>
                                                    </div>
                                                </div> --}}
                                            </form>
                        
                                        </div>
                                    </div>
                                    <!-- end card body -->
                                </div>
                                <!-- end card -->
                        
                                <div class="mt-4 text-center">
                                    <p class="mb-0">Déjà un compte ? <a href="/login" class="fw-bold text-primary text-decoration-underline"> Se connecter pluôt ! </a> </p>
                                </div>
                        
                            </div>

                            
                        </div>
                    
    </div>
@endsection








