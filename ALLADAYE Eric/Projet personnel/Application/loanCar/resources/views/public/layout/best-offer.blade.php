  <section>
          <div class="r-best-vehicles">
            <div class="r-sec-head r-accent-color r-sec-head-v">
              <span>FEATURED CARS</span>
              <h2>Our <b>Best Offers.</b></h2>
            </div>
            <div class="container">
              <div class="row clearfix r-best-offer-list">
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-xs-12">
                  <div class="r-best-offer-single">
                    <div class="r-best-offer-in">
                      <div class="r-offer-img">
                        <a class="d-inline-block" href="#"><img src="assets/images/best-01.jpg" class="img-fluid d-block m-auto" alt=""></a>
                        <div class="r-offer-img-over">
                          <i class="fa fa-search"></i>
                        </div>
                      </div>
                      <div class="r-best-offer-content">
                        <a href="#"><b>Volk</b> GTR</a>
                        <p>Start at <b>45.00 USD</b> per day</p>
                        <ul class="pl-0 mb-0">
                          <li><i class="fa fa-car"></i><span>2017</span></li>
                          <li><i class="fa fa-cogs"></i><span>MANUAL</span></li>
                          <li><i class="fa fa-beer"></i><span>PETROL</span></li>
                          <li><i class="fa fa-road"></i><span>2.3k CC</span></li>
                        </ul>
                      </div>
                      <div class="r-offer-rewst-this">
                        <span class="text-uppercase">Rent this car</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-xs-12">
                  <div class="r-best-offer-single">
                    <div class="r-best-offer-in">
                      <div class="r-offer-img">
                        <a class="d-inline-block" href="#"><img src="assets/images/best-02.jpg" class="img-fluid d-block m-auto" alt=""></a>
                        <div class="r-offer-img-over">
                          <i class="fa fa-search"></i>
                        </div>
                      </div>
                      <div class="r-best-offer-content">
                        <a href="#"><b>Hyundai</b> Z.E</a>
                        <p>Start at <b>45.00 USD</b> per day</p>
                        <ul class="pl-0 mb-0">
                          <li><i class="fa fa-car"></i><span>2017</span></li>
                          <li><i class="fa fa-cogs"></i><span>MANUAL</span></li>
                          <li><i class="fa fa-beer"></i><span>PETROL</span></li>
                          <li><i class="fa fa-road"></i><span>2.3k CC</span></li>
                        </ul>
                      </div>
                      <div class="r-offer-rewst-this">
                        <span class="text-uppercase">Rent this car</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-xs-12">
                  <div class="r-best-offer-single">
                    <div class="r-best-offer-in">
                      <div class="r-offer-img">
                        <a class="d-inline-block" href="#"><img src="assets/images/best-03.jpg" class="img-fluid d-block m-auto" alt=""></a>
                        <div class="r-offer-img-over">
                          <i class="fa fa-search"></i>
                        </div>
                      </div>
                      <div class="r-best-offer-content">
                        <a href="#"><b>Audi</b> R8</a>
                        <p>Start at <b>45.00 USD</b> per day</p>
                        <ul class="pl-0 mb-0">
                          <li><i class="fa fa-car"></i><span>2017</span></li>
                          <li><i class="fa fa-cogs"></i><span>MANUAL</span></li>
                          <li><i class="fa fa-beer"></i><span>PETROL</span></li>
                          <li><i class="fa fa-road"></i><span>2.3k CC</span></li>
                        </ul>
                      </div>
                      <div class="r-offer-rewst-this">
                        <span class="text-uppercase">Rent this car</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>