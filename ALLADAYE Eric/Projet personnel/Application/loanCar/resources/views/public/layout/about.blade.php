 <section id="r-about-info">
          <div class="r-about-info">
            <div class="container">
              <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12">
                  <div class="r-about-info-img">
                    <img src="assets/images/about-img-01.png" class="img-fluid d-block m-auto" alt="">
                  </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12">
                  <div class="r-about-info-content">
                    <div class="r-sec-head r-sec-head-left r-sec-head-line r-sec-head-w pt-0">
                      <span>KNOW MORE ABOUT US</span>
                      <h2>Who <b>Royal Cars</b> Are.</h2>
                    </div>
                    <p>
                      We know the difference is in the details and that’s why our car rental services, in the tourism and business industry, stand out for their quality and good taste.
                    </p>
                    <ul class="mb-0 pl-0">
                      <li><i class="fa fa-check-circle"></i> We working since 1980 with 4.000 customers</li>
                      <li><i class="fa fa-check-circle"></i> All brand & type cars in our garage</li>
                      <li><i class="fa fa-check-circle"></i> 1.000+ picking locations around the world</li>
                    </ul>
                    <img src="assets/images/about-sign.png" class="img-fluid d-inline-block" alt="">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>