 <section id="r-advantages-part" class="r-advantages-part">
          <div class="r-advantage-main-part">
            <div class="container clearfix">
              <div class="advantage-head">
                <span>120+ CARS TYPE &amp; BRANDS</span>
                <h2>Royal Cars <b>Advantages.</b></h2>
              </div>
              <div class="row clearfix">
                 <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-xs-12">
                   <div class="r-advantages-box">
                     <div class="icon"> <img src="assets/images/advantage-icon-1.png" alt=""> </div>
                     <div class="head animated pulse">24/7 Customer Online Support</div>
                     <div class="sub-text">Call us Anywhere Anytime</div>
                   </div>
                 </div>
                 <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-xs-12">
                   <div class="r-advantages-box">
                     <div class="icon"> <img src="assets/images/advantage-icon-2.png" alt=""> </div>
                     <div class="head animated pulse">Reservation Anytime You Wants</div>
                     <div class="sub-text">24/7 Online Reservation</div>
                   </div>
                 </div>
                 <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-xs-12">
                   <div class="r-advantages-box">
                     <div class="icon"> <img src="assets/images/advantage-icon-3.png" alt=""> </div>
                     <div class="head animated pulse">Lots of Picking Locations</div>
                     <div class="sub-text">250+ Locations</div>
                   </div>
                 </div>
              </div>
            </div>
          </div>
        </section>