                    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <div class="r-header-action float-left">
                            @guest
                                <a href="{{ route('login') }}">
                                    <img src="assets/images/icon-lock.png" alt='' />
                                    <span>Se connecter</span>
                                </a>
                             @endguest          
                         </div>
                        <div class="r-nav-section float-right">
                                <nav>
                                    <ul>
                                    <li class="">
                                        <a href="index-2.html">ACCUEIL</a>
                                    </li>
                                    <li class="">
                                        <a href="about.html">A PROPOS</a>
                                    
                                    </li>
                                    <li class="">
                                        <a href="">VEHICLES</a>
                                    </li>
                                    <li><a href="">FAQ</a></li>
                                    <li><a href="">CONTACTEZ-NOUS</a></li>
                                    @auth
                                    <li style="background-color: #FFC107;">
                                        <a class="px-2" href="{{ route('dashboard') }}">TABLEAU DE BORD</a>
                                    </li>
                                    @endauth                                    </ul>
                                </nav>
                        </div>
                    </div>
                  