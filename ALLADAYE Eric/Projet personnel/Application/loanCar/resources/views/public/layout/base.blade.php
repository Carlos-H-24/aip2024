
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
  
<!-- Mirrored from jthemes.net/themes/html/royalcars/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 14 Feb 2024 10:25:00 GMT -->
<head>
      <!-- PAGE TITLE -->
      <title>Home - Royal Cars  :: Car, Bike Any Vehicle Rental HTML Template</title>

      <!-- META-DATA -->
      <meta http-equiv="content-type" content="text/html; charset=utf-8" >
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <meta name="description" content="" >
      <meta name="keywords" content="" >

      <!-- FAVICON -->
      <link rel="shortcut icon" href="assets/images/favicon.png">

      <!-- CSS:: FONTS -->
      <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">

      <!-- CSS:: ANIMATE -->
      <link rel="stylesheet" type="text/css" href="assets/css/plugins/animate/animate.css">

      <!-- CSS:: MAIN -->
      <link rel="stylesheet" type="text/css" href="assets/css/main.css">
      <link rel="stylesheet" type="text/css" id="r-color-roller" href="assets/color-files/color-08.css">

  </head>
  <body>
      <section id="r-customizer" class="r-customizer">
            <div class="r-selector"> 
                  <span class="d-block text-center">Color Options</span>
                  <div class="r-color_section r-color_block">
                        <ul class="r-color_selector" id="r-color_selector">
                            <li class="r-color_1" data-attr="color-01"></li>
                            <li class="r-color_6" data-attr="color-06"></li>
                            <li class="r-color_2" data-attr="color-02"></li>
                            <li class="r-color_3" data-attr="color-03"></li>
                            <li class="r-color_4" data-attr="color-04"></li>
                            <li class="r-color_5" data-attr="color-05"></li>
                            <li class="r-color_7" data-attr="color-07"></li>
                            <li class="r-color_8" data-attr="color-08"></li>
                        </ul>
                  </div>  
            </div>
            <i id="r-selector_icon" class="fa fa-cog"></i>
      </section>
      <div class="r-wrapper">
        {{-- header --}}
        <header>
            <div class="r-header r-header-inner r-header-strip-01">
              <div class="r-header-strip r-header-strip-01">
                <div class="container">
                  <div class="row clearfix">
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12">
                      <div class="r-logo">
                        <a href="#" class="d-inline-block"><img src="assets/images/logo-white.png" class="img-fluid d-block" alt=""></a>
                      </div>
                      <a href="javaScript:void(0)" class="menu-icon"> <i class="fa fa-bars"></i> </a>
                    </div>
                    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
                      <div class="r-header-action float-left">
                        <a href="login-register.html"> <img src="assets/images/icon-lock.png" alt='' /> <span>Login</span></a>
                        <a href="#" class="r-search"> <img src="assets/images/icon-search.png" alt='' /> <span>Search</span></a>
  
                        <div class="r-search-wrapper">
                          <div class="r-search-inner">
                            <form>
                                <input type="text" class="r-search-field" placeholder="Search"/>
                                <button type="submit" class="r-search-btn">
                                  <i class="fa fa-search"></i>
                                </button>
                            </form>
                          </div>
                        </div> <!-- /r-search-wrapper -->
                      </div>
  
                      {{-- nav bar section --}}
                      <div class="r-nav-section float-right">
                        <nav>
                          <ul>
                            <li class="r-has-child">
                              <a href="index-2.html">HOME</a>
                              <ul class="pl-0 ml-0">
                                <li><a href="index-2.html">Home 01</a></li>
                                <li><a href="index-02.html">Home 02</a></li>
                                <li><a href="index-03.html">Home 03</a></li>
                                <li><a href="index-04.html">Home 04</a></li>
                              </ul>
                            </li>
                            <li class="r-has-child">
                              <a href="about.html">ABOUT US</a>
                              <ul class="pl-0 ml-0">
                                <li><a href="faq.html">Faq</a></li>
                              </ul>
                            </li>
                            <li class="r-has-child">
                              <a href="car-listing.html">VEHICLES</a>
                              <ul class="pl-0 ml-0">
                                <li><a href="car-list-map.html">Car List Map</a></li>
                                <li><a href="car-booking.html">Car Booking</a></li>
                              </ul>
                            </li>
                            <li><a href="gallery.html">GALLERY</a></li>
                            <li><a href="drivers.html">DRIVERS</a></li>
                            <li><a href="contact.html">CONTACT US</a></li>
                          </ul>
                        </nav>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              {{-- slider carousel --}}
              <div class="r-slider owl-carousel" id="defaultHomeSlider">
                <div class="r-slider-item">
                  <img src="assets/images/main-slider-01.jpg" class="img-fluid d-block m-auto" alt="">
                  <div class="container">
                    <div class="r-slider-top-content">
                      <h1 class="animated fadeInDown">Kia Rio <span>z</span></h1>
                      <h4 class="animated fadeInLeft">FOR RENT <strong>$50</strong> PER DAY</h4>
                      <a href="#" class="btn btn-outlined animated fadeInUp"> Reserve Now </a>
                    </div>
                  </div> 
                </div>
                <div class="r-slider-item">
                  <img src="assets/images/main-slider-04.jpg" class="img-fluid d-block m-auto" alt="">
                  <div class="container">
                    <div class="r-slider-top-content">
                      <h1>BMW <span>3</span></h1>
                      <h4>FOR RENT <strong>$150</strong> PER DAY</h4>
                      <a href="#" class="btn btn-outlined"> Reserve Now </a>
                    </div>
                  </div>
                </div>
                <div class="r-slider-item">
                  <img src="assets/images/main-slider-05.jpg" class="img-fluid d-block m-auto" alt="">
                  <div class="container">
                    <div class="r-slider-top-content">
                      <h1>Audi <span>A4</span></h1>
                      <h4>FOR RENT <strong>$100</strong> PER DAY</h4>
                      <a href="#" class="btn btn-outlined"> Reserve Now </a>
                    </div>
                  </div>
                </div>
              </div>
              {{-- search section --}}
  
              <div class="r-car-search">
                <div class="container">
                  <div class="r-top-form-title animated fadeInUp">
                    <span>120+ CARS TYPE & BRANDS</span>
                    <h3>Search Your <b>Best Cars.</b></h3>
                  </div>
                  <form>
                    <div class="row">
                      <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                          <label> Car Brand </label>
                          <select class="form-control">
                            <option>Any Brands</option>
                            <option>Any Brands</option>
                            <option>Any Brands</option>
                            <option>Any Brands</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                          <label> Car Type </label>
                          <select class="form-control">
                            <option>Any Type</option>
                            <option>Any Type</option>
                            <option>Any Type</option>
                            <option>Any Type</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                          <label> Car Price </label>
                          <select class="form-control">
                            <option>Price Low to High</option>
                            <option>Price High to Low</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                          <input type="submit" class="form-control btn-primary" value="Search Car Now">
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </header>        {{-- advantage section --}}
             @include('public.layout.advantage')
    
            {{-- about section --}}
                  @include('public.layout.about')
    
            {{-- Best offer  --}}
                  @include('public.layout.best-offer')
            {{-- testimonial --}}
        
                  @include('public.layout.testimonial')
        
                {{-- best-vehicule --}}
                  @include('public.layout.best-vehicule')
                {{-- faq --}}
                  @include('public.layout.faq')
            {{-- counter --}}
                  @include('public.layout.counter')
      
            {{-- footer --}}
                  @include('public.layout.footer')
        </div>
        <div id="r-to-top" class="r-to-top"><i class="fa fa-angle-up"></i></div>
      
        <!-- JQUERY:: JQUERY.JS -->
        <script src="assets/js/jquery.min.js"></script>

        <!-- JQUERY:: APPEAR.JS -->
        <script src="assets/js/plugins/appear/appear.js"></script>

        <!-- JQUERY:: COUNTER.JS -->
        <script src="assets/js/plugins/counter/jquery.easing.min.js"></script>
        <script src="assets/js/plugins/counter/counter.min.js"></script>

        <!-- JQUERY:: BOOTSTRAP.JS -->
        <script src="assets/js/tether.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>

        <!-- JQUERY:: PLUGINS -->
        <script src="assets/js/plugins/owl/owl.carousel.min.js"></script>
        <script src="assets/js/plugins/lightcase/lightcase.js"></script>


        <!-- JQUERY:: MAP -->
        <script src="assets/js/map.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBK7lXLHQgaGdP3IvMPi1ej0B9JHUbcqB0&amp;callback=initMap">></script>

        <!-- JQUERY:: CUSTOM.JS -->
        <script src="assets/js/custom.js"></script>

  </body>

<!-- Mirrored from jthemes.net/themes/html/royalcars/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 14 Feb 2024 10:25:00 GMT -->
</html>
