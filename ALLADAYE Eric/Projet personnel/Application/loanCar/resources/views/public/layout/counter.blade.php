<div class="r-counter-section r-counter-with-bg m-0">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-4 col-sm-6">
                        <div class="r-counter-box">
                            <div class="r-counter-icon">
                                <img src="assets/images/icon-happy-customer-white.png" alt="" class="img-fluid">
                            </div>
                            <div class="r-counts" data-count="172700">
                                <!-- 1.172.700 -->
                                <span class="r-count">0</span>
                            </div>
                            <span class="r-count-title"> HAPPY CUSTOMERS </span>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-4 col-sm-6">
                        <div class="r-counter-box">
                            <div class="r-counter-icon">
                                <img src="assets/images/icon-cars-count-white.png" alt="" class="img-fluid">
                            </div>
                            <div class="r-counts" data-count="2450">
                                <!-- 2.450 -->
                                <span class="r-count">0</span>

                            </div>
                            <span class="r-count-title"> CARS IN GARAGE </span>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-4 col-sm-6">
                        <div class="r-counter-box">
                            <div class="r-counter-icon">
                                <img src="assets/images/icon-total-km-white.png" alt="" class="img-fluid">
                            </div>
                            <div class="r-counts" data-count="1211100">
                                <!-- 1.211.100 -->
                                <span class="r-count">0</span>
                            </div>
                            <span class="r-count-title"> TOTAL KILOMETER/MIL </span>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-4 col-sm-6">
                        <div class="r-counter-box">
                            <div class="r-counter-icon">
                                <img src="assets/images/icon-car-center-white.png" alt="" class="img-fluid">
                            </div>
                            <div class="r-counts" data-count="47250">
                                <!-- 47.250 -->
                                <span class="r-count">0</span>
                            </div>
                            <span class="r-count-title"> CAR CENTER SOLUTIONS </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>