<header>
          <div class="r-header r-header-inner r-header-strip-01">
            <div class="r-header-strip r-header-strip-01">
              <div class="container">
                <div class="row clearfix">
                  <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <div class="r-logo">
                      <a href="#" class="d-inline-block"><img src="assets/images/logo-white.png" class="img-fluid d-block" alt=""></a>
                    </div>
                    <a href="javaScript:void(0)" class="menu-icon"> <i class="fa fa-bars"></i> </a>
                  </div>   
                                   {{-- nav bar section --}}
                  @include('public.layout.menu')

                </div>
              </div>
            </div>
            {{-- slider carousel --}}
            <div class="r-slider owl-carousel" id="defaultHomeSlider">
              <div class="r-slider-item">
                <img src="assets/images/main-slider-01.jpg" class="img-fluid d-block m-auto" alt="">
                <div class="container">
                  <div class="r-slider-top-content">
                    <h1 class="animated fadeInDown">Kia Rio <span>z</span></h1>
                    <h4 class="animated fadeInLeft">EN LOCATION POUR <strong>$50</strong>/ JOUR</h4>
                    <a href="{{route('register')}}" class="btn btn-outlined animated fadeInUp"> Reserver maintenant </a>
                  </div>
                </div> 
              </div>
              <div class="r-slider-item">
                <img src="assets/images/main-slider-04.jpg" class="img-fluid d-block m-auto" alt="">
                <div class="container">
                  <div class="r-slider-top-content">
                    <h1>BMW <span>3</span></h1>
                    <h4>EN LOCATION POUR  <strong>$150</strong> / JOUR</h4>
                    <a href="{{route('register')}}" class="btn btn-outlined"> Reserver maintenant </a>
                  </div>
                </div>
              </div>
              <div class="r-slider-item">
                <img src="assets/images/main-slider-05.jpg" class="img-fluid d-block m-auto" alt="">
                <div class="container">
                  <div class="r-slider-top-content">
                    <h1>Audi <span>A4</span></h1>
                    <h4>EN LOCATION POUR  <strong>$100</strong> / JOUR</h4>
                    <a href="{{route('register')}}" class="btn btn-outlined"> Reserver maintenant </a>
                  </div>
                </div>
              </div>
            </div>
            {{-- search section --}}

            <div class="r-car-search">
              <div class="container">
                <div class="r-top-form-title animated fadeInUp">
                  <span>PLUS DE 120 TYPES ET MARQUES DE VOITURES</span>
                  <h3>RECHERCHER VOS <b>MEILLEURS VOITURE.</b></h3>
                </div>
                <form>
                  <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                      <div class="form-group">
                        <label> Car Brand </label>
                        <select class="form-control">
                          <option>Any Brands</option>
                          <option>Any Brands</option>
                          <option>Any Brands</option>
                          <option>Any Brands</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                      <div class="form-group">
                        <label> Car Type </label>
                        <select class="form-control">
                          <option>Any Type</option>
                          <option>Any Type</option>
                          <option>Any Type</option>
                          <option>Any Type</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                      <div class="form-group">
                        <label> Car Price </label>
                        <select class="form-control">
                          <option>Price Low to High</option>
                          <option>Price High to Low</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                      <div class="form-group">
                        <input type="submit" class="form-control btn-primary" value="Search Car Now">
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </header>