
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
  
<!-- Mirrored from jthemes.net/themes/html/royalcars/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 14 Feb 2024 10:25:00 GMT -->
<head>
      <!-- PAGE TITLE -->
      <title>Accueil - GoDriveHub</title>

      <!-- META-DATA -->
      <meta http-equiv="content-type" content="text/html; charset=utf-8" >
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <meta name="description" content="" >
      <meta name="keywords" content="" >

      <!-- FAVICON -->
      <link rel="shortcut icon" href="assets/images/favicon.png">

      <!-- CSS:: FONTS -->
      <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">

        <!-- CSS:: ANIMATE -->
        <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/animate/animate.css') }}">

        <!-- CSS:: MAIN -->
        <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/main.css') }}">
        <link rel="stylesheet" type="text/css" id="r-color-roller" href="{{ asset('assets/color-files/color-08.css') }}">

  </head>
  <body>

      <div class="r-wrapper">
                    {{-- header --}}
                  @include('public.layout.header')
                     {{-- advantage section --}}
                  @include('public.layout.advantage')
    
                  {{-- about section --}}
                  @include('public.layout.about')
    
                    {{-- Best offer  --}}
                  @include('public.layout.best-offer')
                     {{-- testimonial --}}
        
                  @include('public.layout.testimonial')
        
                      {{-- best-vehicule --}}
                  @include('public.layout.best-vehicule')
                     {{-- faq --}}
                  @include('public.layout.faq')
                    {{-- counter --}}
                  @include('public.layout.counter')
                     {{-- footer --}}
                  @include('public.layout.footer')
        </div>
        
        <div id="r-to-top" class="r-to-top"><i class="fa fa-angle-up"></i></div>
      
            <!-- JQUERY:: JQUERY.JS -->
            <script src="{{ asset('assets/js/jquery.min.js') }}"></script>

            <!-- JQUERY:: APPEAR.JS -->
            <script src="{{ asset('assets/js/plugins/appear/appear.js') }}"></script>

            <!-- JQUERY:: COUNTER.JS -->
            <script src="{{ asset('assets/js/plugins/counter/jquery.easing.min.js') }}"></script>
            <script src="{{ asset('assets/js/plugins/counter/counter.min.js') }}"></script>

            <!-- JQUERY:: BOOTSTRAP.JS -->
            <script src="{{ asset('assets/js/tether.min.js') }}"></script>
            <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>

            <!-- JQUERY:: PLUGINS -->
            <script src="{{ asset('assets/js/plugins/owl/owl.carousel.min.js') }}"></script>
            <script src="{{ asset('assets/js/plugins/lightcase/lightcase.js') }}"></script>

            <!-- JQUERY:: MAP -->
            <script src="{{ asset('assets/js/map.js') }}"></script>
            <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_GOOGLE_MAPS_API_KEY&amp;callback=initMap"></script>

            <!-- JQUERY:: CUSTOM.JS -->
            <script src="{{ asset('assets/js/custom.js') }}"></script>

  </body>

<!-- Mirrored from jthemes.net/themes/html/royalcars/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 14 Feb 2024 10:25:00 GMT -->
</html>
