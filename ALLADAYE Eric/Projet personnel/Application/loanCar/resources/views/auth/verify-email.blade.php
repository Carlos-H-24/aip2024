{{-- <x-guest-layout>
    <div class="mb-4 text-sm text-gray-600">
        {{ __('Thanks for signing up! Before getting started, could you verify your email address by clicking on the link we just emailed to you? If you didn\'t receive the email, we will gladly send you another.') }}
    </div>

    @if (session('status') == 'verification-link-sent')
        <div class="mb-4 font-medium text-sm text-green-600">
            {{ __('A new verification link has been sent to the email address you provided during registration.') }}
        </div>
    @endif

    <div class="mt-4 flex items-center justify-between">
        <form method="POST" action="{{ route('verification.send') }}">
            @csrf

            <div>
                <x-primary-button>
                    {{ __('Resend Verification Email') }}
                </x-primary-button>
            </div>
        </form>

        <form method="POST" action="{{ route('logout') }}">
            @csrf

            <button type="submit" class="underline text-sm text-gray-600 hover:text-gray-900 rounded-md focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                {{ __('Log Out') }}
            </button>
        </form>
    </div>
</x-guest-layout> --}}

@extends('admin.layouts.auth-base')
@section('title')
Verification par Mail | GoDriveHub
@endsection
@section('content')

<div class="row justify-content-center">

                    <div class="col-md-8 col-lg-6 col-xl-5">
                        <div class="card mt-4">

                            <div class="card-body p-4">
                                <div class="text-center mt-2">
                                    <h5 class="text-primary">Bienvenue</h5>
                                    <p class="text-muted">Verifiez d'abord confirmer votre mail !</p>
                                </div>
                                @if (session('status') !== 'verification-link-sent')
                                    <div class="p-2 mt-4">
                                        {{ __("Avant de continuer, Veillez cliquer sur le lien que nous venons de vous envoyer par e-mail . Si vous n'avez pas reçu l'e-mail, nous vous en enverrons volontiers un autre.") }}
                                        
                                    </div>
                                @endif
                                
                                        @if (session('status') == 'verification-link-sent')
                                                    <div class="p-2 mt-4">
                                                        {{ __("Un nouveau lien de vérification a été envoyé à l'adresse e-mail que vous avez fournie lors de l'inscription. N'oubliez pas de fouiller dans vos spams .") }}
                                                    </div>
                                        @endif

                                        <div class="mt-4 d-flex items-center justify-between">
                                            <form method="POST" class="mx-2" action="{{ route('verification.send') }}">
                                                @csrf
                                    
                                                <div>
                                                    <x-primary-button>
                                                        {{ __('Renvoyer le mail de verification') }}
                                                    </x-primary-button>
                                                </div>
                                            </form>
                                    
                                            <form method="POST" action="{{ route('logout') }}">
                                                @csrf
                                    
                                                <button type="submit" class="btn btn-danger">
                                                    {{ __('Se Déconnecter') }} <i class="fas fa-sign-out"></i>
                                                </button>
                                            </form>
                                        </div>
                            </div>
                            <!-- end card body -->
                        </div>
                        <!-- end card -->

                    </div>
                </div>
@endsection
