<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        // Schema::table('users', function (Blueprint $table) {
        //     $table->foreign('role_id')
        //     ->references('id')
        //     ->on('role')
        //     ->onDelete('restrict')
        //     ->onUpdate('restrict');
        // });


        Schema::table('vehicules', function (Blueprint $table) {
            $table->foreign('annee_id')
            ->references('id')
            ->on('annee_fabrications')
            ->onDelete('restrict')
            ->onUpdate('restrict');
            $table->foreign('type_vehicule')
            ->references('id')
            ->on('type_vehicules')
            ->onDelete('restrict')
            ->onUpdate('restrict');
            $table->foreign('modele')
            ->references('id')
            ->on('model_vehicules')
            ->onDelete('restrict')
            ->onUpdate('restrict');
           
        });


        Schema::table('locations', function (Blueprint $table) {
            $table->foreign('users_id')
            ->references('id')
            ->on('users')
            ->onDelete('restrict')
            ->onUpdate('restrict');

            $table->foreign('vehicule')
            ->references('id')
            ->on('vehicules')
            ->onDelete('restrict')
            ->onUpdate('restrict');
        });


        Schema::table('paiements', function (Blueprint $table) {
            $table->foreign('location_id')
            ->references('id')
            ->on('location')
            ->onDelete('restrict')
            ->onUpdate('restrict');

            $table->foreign('paiment_means_id')
            ->references('id')
            ->on('paiement_means')
            ->onDelete('restrict')
            ->onUpdate('restrict');
        });

        Schema::table('medias', function (Blueprint $table) {
            $table->foreign('vehicule_id')
            ->references('id')
            ->on('vehicules')
            ->onDelete('restrict')
            ->onUpdate('restrict');
            $table->foreign('users_id')
            ->references('id')
            ->on('users')
            ->onDelete('restrict')
            ->onUpdate('restrict');
        });

        Schema::table('model_vehicules', function (Blueprint $table) {
            $table->foreign('marque_id')
            ->references('id')
            ->on('marque_vehicules')
            ->onDelete('restrict')
            ->onUpdate('restrict');
        });
        
        Schema::table('evaluations', function (Blueprint $table) {
            $table->foreign('location_id')
            ->references('id')
            ->on('locations')
            ->onDelete('restrict')
            ->onUpdate('restrict');
        });
        Schema::table('permissions', function (Blueprint $table) {
            $table->foreign('role_id')
            ->references('id')
            ->on('roles')
            ->onDelete('restrict')
            ->onUpdate('restrict');
            $table->foreign('user_id')
            ->references('id')
            ->on('users')
            ->onDelete('restrict')
            ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('table', function (Blueprint $table) {
            //
        });
    }
};
