Departement(id, nom, longitude, lattitude),

Commune(id, nom, longitude, lattitude, departement_id),

Arrondissement(id, nom, longitude, lattitude, commune_id),

Quartier(id, nom, longitude, lattitude, arrondissement_id),

Hotel(id, nom, longitude, lattitude, adresse_postale, email, telephone, directeur, quartier_id),

TypeChambre(id, type, prix),

Chambre(id, numero, libelle, etage, description, capacite, statut, type_Chambre_id, hotel_id),

Role(id, role),

Permission(id, permission),

Utilisateur(id, nom, prenoms, date_naissance, sexe, nationnalite, email, telephone, hotel_id),


RoleUtilisateur(role_id, utilisateur_id),

PermissionUtilisateur(permission_id, utilisateur_id),

PermissionRole(permission_id, role_id),


Reservation(id, chambre_id, utilisateur_id, statut, date_reservation),

TypeService(id, type, prix),

Service(id, description, utilisateur_id, type_service_id),

Paiement(id, utilisateur_id, moyen_paiement, montant, date_paiement),

Facture(id, type, montant_total, paiement_id)

